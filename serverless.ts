import type { AWS } from '@serverless/typescript';


import createOrder from '@functions/createOrder';
import updateOrder from '@functions/updateOrder';
import getOrderById from '@functions/getOrderById';
import addproduct from '@functions/createProduct';
import addErrorDetails  from '@functions/createErrorDetails';
import addRecomendation  from '@functions/createRecomendations';
import getAllProducts from '@functions/getAllProducts';
import getAllOrder from '@functions/getAllorders';
import deleteOrder from '@functions/deleteOrder';
import deleteProduct from '@functions/deleteProduct';
import updateErrorDetails from '@functions/updateErrorDetails';
import updateRecomendationDetails from '@functions/updateRecomendationDetails';
import updateProductDetails from '@functions/updateProductDetails';
import addTechnicianAvailability  from '@functions/scheduler/createTechnicianAvailability';
import getRecomendationDetails from '@functions/getRecomendationDetails';
import assignOrderToTechnician  from '@functions/scheduler/assignOrderTechnician';
// import getBookingTypeById from '@functions/getBookingTypeById'
import getProductById from '@functions/getProductById';
import getErrorDetailsById from '@functions/getErrorDetailsById';
import getTechnicianAvailability  from '@functions/scheduler/getTechnicianAvailabilty';
import getTechnicianAssignOrders from '@functions/scheduler/getTechnicianAssignOrders';
import updateTechnicianAvailability from '@functions/scheduler/updateTechnicianAvailability';
import updateAssignOrder from '@functions/scheduler/updateAssignOrder';
// import cancelOrderAssignment from '@functions/scheduler/cancelOrderAssignment';
import deleteTechnicianAvailability from '@functions/scheduler/deleteTechnicianAvailability';
import getAllBookingTypes from '@functions/getAllBookingTypes';
import getOrderStatus from '@functions/scheduler/getOrderStatus';
import addOrderSpareParts  from '@functions/orderSpareParts/addOrderSparePart';
import getAllSparePartsByOrderId from '@functions/orderSpareParts/getAllSparePartsByOrderId';
import updateOrderSpareParts from '@functions/orderSpareParts/updateOrderSpareParts';
import deleteOrderSpareParts from '@functions/orderSpareParts/deleteOrderSpareParts';
import getOrderSparePartStatus from '@functions/orderSpareParts/getOrderSparePartStatus';
import  addProblemSolution  from '@functions/problem&solutions/addProblemSolution';
import updateProblemSolution from '@functions/problem&solutions/updateProblemSolution';
import getProblemSolution from '@functions/problem&solutions/getProblemSolution';
import deleteProblemSolution from '@functions/problem&solutions/deleteProblemSolution';
import addSignature from '@functions/signature/addSignature'
import addProofOfWork from '@functions/proofOfWork/addProofOfWork';
import updateProofOfWork from '@functions/proofOfWork/updateProofOfWork';
import getProofOfWork from '@functions/proofOfWork/getProofOfWork';
import deleteProofOfWork from '@functions/proofOfWork/deleteProofOfWork';
import updateSignature from '@functions/signature/updateSignature'
import getSignature from '@functions/signature/getSignature';
import getAllProductBrands from '@functions/getAllProductBrands';
import getAllProductTypes from '@functions/getAllProductTypes';
import getStatusActivityByOrderId from '@functions/getStatusActivityByOrderId';
import getordersByCustomerId from '@functions/getordersByCustomerId';
import getDashBoardAnalytics from '@functions/getDashBoardAnalytics';
// import getOrderNotifications from '@functions/getOrderNotifications';
// import addProductComments from '@functions/productComments/addProductComments';
// import updateProductComments from '@functions/productComments/updateProductComments';
// import getProductComments from '@functions/productComments/getProductComments';
// import deleteProductComments from '@functions/productComments/deleteProductComments';
import getAllOrderBySparePartId from '@functions/orderSpareParts/getAllOrderBySparePartId';
import getAllCustomerOrders from '@functions/getAllCustomerOrders';
import getRecommendationChecks from '@functions/getRecommendationChecks';
import getErrorDetialsChecks from '@functions/getErrorDetialsChecks';
import addSignatureApprovel from '@functions/approvelSignature/addSignatureApprovel';
import getSignatureApprovel from '@functions/approvelSignature/getSignatureApprovel';
import updateSignatureApprovel from '@functions/approvelSignature/updateSignatureApprovel';
import deleteSignatureApprovel from '@functions/approvelSignature/deleteSignatureApprovel';
import getPartnersTechnicianAvailability from '@functions/scheduler/getPartnersTechnicianAvailability';
import orderFdToPartner from '@functions/scheduler/orderFdToPartner';
import getForwardedOrderByOrderId from '@functions/scheduler/getForwardedOrderByOrderId';
import getAllAvailableTechnicians from '@functions/scheduler/getAllAvailableTechnicians';
import getAllForwardedOrder from '@functions/getAllForwardedOrder';
import updateForwardedOrderAssignment from '@functions/scheduler/updateForwardedOrderAssignment';
import updateOrderStatus from '@functions/updateOrderStatus';
import getStatusReason from '@functions/getStatusReason';
import getStateReason from '@functions/getStateReason';


// import addTechniciansTimings from '@functions/addTechniciansTimings';
// import updateTechniciansTimings from '@functions/updateTechniciansTimings';
// import getTechiniciansTimings from '@functions/getTechniciansTimings';
// import deleteTechniciansTimings from '@functions/deleteTechniciansTimings';











const serverlessConfiguration: AWS = {
  service: 'APIOrderManagement',
  frameworkVersion: '3',
  plugins: ['serverless-esbuild','serverless-offline','serverless-dotenv-plugin','serverless-sequelize-migrations'],
  provider: {
    name: 'aws',
    runtime: 'nodejs14.x',
    region:'eu-central-1',
    memorySize:256,
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: {
      dialect:'mysql',
      DATABASE_NAME:process.env.DATABASE_NAME,
      DATABASE_USERNAME:process.env.READ_DATABASE_USERNAME,
      DATABASE_PASSWORD:process.env.DATABASE_PASSWORD,
      DATABASE_HOST:process.env.DATABASE_HOST,
      DATABASE_PORT:process.env.DATABASE_PORT,
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      NODE_OPTIONS: '--enable-source-maps --stack-trace-limit=1000',
    },
  },
  // import the function via paths
  functions: { 
              createOrder  ,updateOrder ,getOrderById ,addproduct ,addErrorDetails ,
              addRecomendation ,getAllProducts, getAllOrder,deleteOrder,deleteProduct,updateErrorDetails,
              updateRecomendationDetails ,getRecomendationDetails, updateProductDetails ,addTechnicianAvailability,
              assignOrderToTechnician, getProductById , getErrorDetailsById,getTechnicianAvailability,
              getTechnicianAssignOrders,updateTechnicianAvailability,updateAssignOrder ,
              deleteTechnicianAvailability,getAllBookingTypes,getOrderStatus,addOrderSpareParts, getAllSparePartsByOrderId,
              updateOrderSpareParts,deleteOrderSpareParts,getOrderSparePartStatus,
              addProblemSolution,updateProblemSolution,getProblemSolution,deleteProblemSolution,
              addProofOfWork,updateProofOfWork,deleteProofOfWork,getProofOfWork,addSignature,updateSignature,getSignature,
              getAllProductBrands,getAllProductTypes,getordersByCustomerId,getDashBoardAnalytics,
              // addProductComments,updateProductComments,getProductComments,deleteProductComments,getOrderNotifications,getBookingTypeById,cancelOrderAssignment,
              getAllOrderBySparePartId,getStatusActivityByOrderId,
              getAllCustomerOrders,getRecommendationChecks,getErrorDetialsChecks, addSignatureApprovel, deleteSignatureApprovel, updateSignatureApprovel, getSignatureApprovel,
              getPartnersTechnicianAvailability,orderFdToPartner,getForwardedOrderByOrderId,getAllAvailableTechnicians,getAllForwardedOrder,updateForwardedOrderAssignment,
              updateOrderStatus,getStatusReason,
              getStateReason
              // hello,addTechniciansTimings ,updateTechniciansTimings ,getTechiniciansTimings ,deleteTechniciansTimings
            },
  package: { individually: true , exclude:['pg-hstore'] },
  custom: {
    esbuild: {
      bundle: true,
      minify: false,
      sourcemap: true,
      exclude: ['aws-sdk'],
      target: 'node14',
      define: { 'require.resolve': undefined },
      platform: 'node',
      concurrency: 1000,
    },
  },
};
// console.log("DATABASE_NAME",DATABASE_NAME)
module.exports = serverlessConfiguration;

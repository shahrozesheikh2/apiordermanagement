'use strict'

import { Sequelize } from 'sequelize-typescript';
import * as dotenv from 'dotenv';

import { models } from '../models';

// import { models } from '../models';

dotenv.config({ path: '.env' });

export const sequelize: Sequelize = new Sequelize(process.env.DATABASE_NAME || '', process.env.DATABASE_USERNAME || '', process.env.DATABASE_PASSWORD, {
    dialect: 'mysql',
    dialectModule: require('mysql2'),
    host: process.env.DATABASE_HOST,
    logging: process.env.NODE_ENVR === 'local' ? true : false,
    models: [...models],
    port: +(process.env.DATABASE_PORT || ''),
    replication: {
        read: [
          {
            database: process.env.READ_DATABASE_NAME,
            host: process.env.READ_DATABASE_HOST,
            password: process.env.READ_DATABASE_PASSWORD,
            username: process.env.READ_DATABASE_USERNAME,
          }
        ],
        write: {
            database: process.env.WRITE_DATABASE_NAME,
            host: process.env.WRITE_DATABASE_HOST,
            password: process.env.WRITE_DATABASE_PASSWORD,
            username: process.env.WRITE_DATABASE_USERNAME,
        }
    },
    pool: {
      max: 65,
      min: 0,
      idle: 0,
      acquire: 6000,
    }
});

sequelize.authenticate()

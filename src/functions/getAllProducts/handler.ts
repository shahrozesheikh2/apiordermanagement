import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { productType, productBrand, order, product } from '../../models/index';
import schema, { getAllProductSchema } from './schema';
import * as typings from '../../shared/common';

import { Op } from 'sequelize';


const getAllProducts:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const data :typings.ANY   = await getAllProductSchema.validateAsync(event.body)

    const{
      // userId,
      limit,
      offset,
      filters
    } = data

    let  whereClause: { [key: string]: typings.ANY }  = {}

    let  productTypeWhereClause: { [key: string]: typings.ANY }  = {}

    let  brandWhereClause: { [key: string]: typings.ANY }  = {}
  
    if (filters?.productType) {
      productTypeWhereClause.productType =  {  [Op.startsWith]: filters.productType } ;
    }
   
    console.log("productTypeWhereClause",productTypeWhereClause)

    
    if (filters?.productTypeId) {
      whereClause.productTypeId =  filters.productTypeId;
    }
   
    if (filters?.brandId) {
      whereClause.brandId =  filters.brandId ;
    } 

    if (filters?.brandName) {
      brandWhereClause.brandName =   {  [Op.startsWith]: filters.brandName } ;
    } 
        console.log("brandWhereClause",brandWhereClause)

    if (filters?.modelNumber) {
      whereClause.modelNumber = {  [Op.startsWith]: filters.modelNumber };
    }

    if (filters?.fixFirstId) {
      whereClause.fixFirstId = {  [Op.startsWith]: filters.fixFirstId };
    }
   
    const response : typings.ANY = await product.findAndCountAll({
      where: { 

        // createdBy:userId
        deletedAt:null,
        ...whereClause
      },
      include:[
      {
        as:'productType',
        model: productType,
        where:{...productTypeWhereClause},
        attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
      },
      {
        as:'brands',
        model: productBrand,
        where:{...brandWhereClause},
        attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
      },
      // {
      //   as:'order',
      //   model: order,
      //   attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
      // }
    ],
      limit:limit,
      offset: offset * limit
    })

    // const count = await product.count(
    //   {
    //     where:{
    //       deletedAt:null,
    //       ...whereClause
    //     }
    // });
  
      if(!response){
        throw Error("Empty")
      }
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,
       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getAllProducts);
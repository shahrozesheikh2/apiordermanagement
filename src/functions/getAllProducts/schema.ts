import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;


export const getAllProductSchema =  Joi.object({
  userId: Joi.number().integer(),
  offset: Joi.number().min(0).default(0),
  limit: Joi.number().max(999).default(10),
  filters:Joi.object({
    productTypeId:Joi.number().integer(),
    brandId: Joi.number().integer(),
    productType:Joi.string(),
    brandName: Joi.string(),
    modelNumber:Joi.string(),
    fixFirstId:Joi.string()
  })
})


import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;

export const updateErrorSchema =  Joi.object({
  userId: Joi.number().integer(),
  orderId: Joi.number().integer().required(),
  errorDetails:Joi.object({
    description: Joi.string(),
    problem: Joi.string(),
    errorCode: Joi.string(),
    veryUrgent:Joi.boolean(),
    parkingHint: Joi.boolean(),
    noNoise:Joi.boolean(),
    badLightning:Joi.boolean(),
    poorlyAccessible:Joi.boolean(),
    slowInternet:Joi.boolean(),
    smartphone:Joi.boolean(),
    tablet:Joi.boolean(),
    laptop:Joi.boolean(),
    webcam:Joi.boolean(),
    fragile:Joi.boolean(),
    resources: Joi.array().items(Joi.object({
      awsKey: Joi.string(),
      awsUrl: Joi.string().uri(),
      description:Joi.string()
  }))
  })
})

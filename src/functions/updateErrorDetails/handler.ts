import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { orderErrorDetails, orderErrorDetailsI } from '../../models/index';
import schema, { updateErrorSchema } from './schema';
import * as typings from '../../shared/common';


const updateErrorDetails:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'Successfully Updated'
      if(lang =='de')
      {
         message = 'Erfolgreich geupdated'
      }
    const Data :typings.ANY   = await updateErrorSchema.validateAsync(event.body)

    const {
      orderId,
      userId,
      errorDetails
    } = Data

    const errorObj = { ...errorDetails, updatedBy:userId}

    const Exists = await orderErrorDetails.findOne({where:{orderId}});

    if(!Exists) throw new Error("Invalid Order Id");

     await orderErrorDetails.update({
          ...errorObj
        }, { 
            where: {
              orderId
            }
        }); 

      const response : orderErrorDetailsI = await orderErrorDetails.findOne({
          where: {
            orderId
          }
      });
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
       message: message,
      //  message: `Successfully Updated`,

       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(updateErrorDetails);
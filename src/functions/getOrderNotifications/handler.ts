import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import {  } from 'src/models/subBookingType';
import { ANY } from 'src/shared/common';
import { orderStatus, roles, statusActivity, statusNotifications, users } from '../../models/index';
import schema, { getOrderNotificationsSchema } from './schema';
import * as typings from '../../shared/common';



const getOrderNotifications:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const data :typings.ANY  = await getOrderNotificationsSchema.validateAsync(event.body)

    const notificationsArr : ANY = await statusNotifications.findAll({
            where: {
                userId: data.userId,
                deletedAt:null
            },
            include:[
              {
                as:'statusActivity',
                model:statusActivity,
                include:[ {
                  as:'orderStatus',
                  model:orderStatus,
                  where:{deletedAt:null},
                  attributes:['id','name','key'],
                },{
                  as:'users',
                  model:users,
                  where:{deletedAt:null},
                  attributes:['id','firstName','lastName'],
                  include:[{
                    as:'roles',
                    model:roles,
                    attributes:['id','name'],
                    where:{deletedAt:null},
                  }]
                  }],
                where:{deletedAt:null},
                attributes:{exclude:['createdBy','updatedAt','createdAt','deletedAt']},
              }
              
            ],
            attributes:{exclude:['createdBy','updatedAt','createdAt','deletedAt']}
        }); 
      let notifications = notificationsArr.map(arr =>({
        id:arr.id,
        userId:arr.userId,
        orderId:arr?.statusActivity?.orderId,
        status:arr?.statusActivity?.orderStatus?.name,
        statusKey:arr?.statusActivity?.orderStatus?.Key,
        changedUserId:arr?.statusActivity?.users?.id,
        changedUserFirstName:arr?.statusActivity?.users?.firstName,
        changedUserlastName:arr?.statusActivity?.users?.lastName,
        userRole:arr?.statusActivity?.users?.roles?.name

      }))
     await sequelize.connectionManager.close();
     return formatJSONResponse({
       statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

       notifications
     });
     
   } catch(error) {
     await sequelize.connectionManager.close();
     console.error(error);
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getOrderNotifications);
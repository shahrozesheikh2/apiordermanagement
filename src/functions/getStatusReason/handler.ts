import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import {  } from 'src/models/subBookingType';
import { ANY } from 'src/shared/common';
import { statusReason } from '../../models/index';
import schema from './schema';
// import * as typings from '../../shared/common';



const getStatusReason:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    // const data :typings.ANY   = await getReasonSchema.validateAsync(event.body)

    console.log("event.pathParameters",event)


    const statusId = event.queryStringParameters.statusId


    console.log("statusId",statusId)
    // const {
    //   statusId
    // }= data

    const response : ANY = await statusReason.findAll({
            where: {
               statusId
            },
            attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}, 
        }); 
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 200,
      //  message: `Erfolgreich erhalten`,
       message: `Successfully Recieved`,
       response
     });
     
   } catch(error) {
     await sequelize.connectionManager.close();
     console.error(error);
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getStatusReason);
export { default as hello } from './hello';
export { default as createOrder } from './createOrder';
export { default as updateOrder } from './updateOrder';
export { default as getOrderById } from './getOrderById';
export { default as addProduct } from './createProduct';
export { default as addErrorDetails } from './createErrorDetails';
export { default as addRecomendation } from './createRecomendations';
export { default as getAllProducts } from './getAllProducts';
export { default as getAllOrder } from './getAllorders';
export { default as deleteOrder } from './deleteOrder';
export { default as deleteProduct } from './deleteProduct';
export { default as updateErrorDetails } from './updateErrorDetails';
export { default as updateRecomendationDetails } from './updateRecomendationDetails';
export { default as updateProductDetails } from './updateProductDetails';
export { default as getRecomendationDetails } from './getRecomendationDetails'
export { default as addTechniciansTimings } from './addTechniciansTimings';
export { default as updateTechniciansTimings } from './updateTechniciansTimings';
export { default as getTechniciansTimings } from './getTechniciansTimings';
export { default as deleteTechniciansTimings } from './deleteTechniciansTimings';
export { default as addTechnicianAvailability } from './scheduler/createTechnicianAvailability';
export { default as assignOrderToTechnician } from './scheduler/assignOrderTechnician';
export { default as getBookingTypeById } from './getBookingTypeById'
export { default as getProductById } from './getProductById'
export { default as getErrorDetailsById} from './getErrorDetailsById'
export { default as getTechnicianAvailability } from './scheduler/getTechnicianAvailabilty'
export { default as getTechnicianAssignOrders } from './scheduler/getTechnicianAssignOrders'
export { default as updateTechnicianAvailability } from './scheduler/updateTechnicianAvailability'
export { default as updateAssignOrder } from './scheduler/updateAssignOrder'
export { default as cancelOrderAssignment } from './scheduler/cancelOrderAssignment'
export { default as deleteTechnicianAvailability } from './scheduler/deleteTechnicianAvailability'
export { default as getAllBookingTypes } from './getAllBookingTypes'
export { default as getOrderStatus } from './scheduler/getOrderStatus'
export { default as getOrderSparePartStatus } from './orderSpareParts/getOrderSparePartStatus'
export { default as addOrderSpareParts } from './orderSpareParts/addOrderSparePart'
export { default as getAllSparePartsByOrderId } from './orderSpareParts/getAllSparePartsByOrderId'
export { default as updateOrderSpareParts } from './orderSpareParts/updateOrderSpareParts'
export { default as addProblemSolution } from './problem&solutions/addProblemSolution'
export { default as updateProblemSolution } from './problem&solutions/updateProblemSolution'
export { default as getProblemSolution } from './problem&solutions/getProblemSolution'
export { default as deleteProblemSolution } from './problem&solutions/deleteProblemSolution'
export { default as addSignature } from './signature/addSignature'
export { default as addProofOfWork } from './proofOfWork/addProofOfWork'
export { default as updateProofOfWork } from './proofOfWork/updateProofOfWork'
export { default as deleteProofOfWork } from './proofOfWork/deleteProofOfWork'
export { default as getProofOfWork } from './proofOfWork/getProofOfWork'
export { default as updateSignature } from './signature/updateSignature'
export { default as getSignature } from './signature/getSignature'
export { default as getAllProductBrands } from './getAllProductBrands'
export { default as getAllProductTypes } from './getAllProductTypes'
export { default as getStatusActivityByOrderId } from './getStatusActivityByOrderId'
export { default as getordersByCustomerId } from './getordersByCustomerId'
export { default as getOrderNotifications } from './getOrderNotifications'
export { default as getDashBoardAnalytics } from './getDashBoardAnalytics'
export { default as addProductComments } from './productComments/addProductComments'
export { default as updateProductComments } from './productComments/updateProductComments'
export { default as getProductComments } from './productComments/getProductComments'
export { default as deleteProductComments } from './productComments/deleteProductComments'
export { default as getAllOrderBySparePartId } from './orderSpareParts/getAllOrderBySparePartId'
export { default as getAllCustomerOrders } from './getAllCustomerOrders'
export { default as getRecommendationChecks } from './getRecommendationChecks'
export { default as getErrorDetialsChecks } from './getErrorDetialsChecks'
export { default as getSignatureApprovel} from './approvelSignature/getSignatureApprovel'
export { default as updateSignatureApprovel} from './approvelSignature/updateSignatureApprovel'
export { default as addSignatureApprovel} from './approvelSignature/addSignatureApprovel'
export { default as deleteSignatureApprovel} from './approvelSignature/deleteSignatureApprovel'
export { default as getPartnersTechnicianAvailability} from './scheduler/getPartnersTechnicianAvailability'
export { default as orderFdToPartner} from './scheduler/orderFdToPartner'
export { default as getForwardedOrderByOrderId} from './scheduler/getForwardedOrderByOrderId'
export { default as getAllAvailableTechnicians} from './scheduler/getAllAvailableTechnicians'
export { default as getAllForwardedOrder} from './getAllForwardedOrder'
export { default as updateForwardedOrderAssignment} from './scheduler/updateForwardedOrderAssignment'
export { default as updateOrderStatus} from './updateOrderStatus'
export { default as getStatusReason} from './getStatusReason'
export { default as getStateReason} from './getStateReason'

































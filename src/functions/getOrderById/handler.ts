import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { subBookingType } from 'src/models/subBookingType';
import { ANY } from 'src/shared/common';
import { productType, productBrand, order ,product,orderErrorDetails,recomendations, orderAssignment, schReccurenceDateList, orderStatus, bookingType, schAvalabilityTechnicians, users} from '../../models/index';
import schema, { getSingleOrderSchema } from './schema';
import * as typings from '../../shared/common';
import axios from 'axios';


const getOrderById :ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{    
      sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
      sequelize.authenticate()    
      
      const orderData :typings.ANY   = await getSingleOrderSchema.validateAsync(event.body)    
      console.log(event.body)
      const{
      id
    } = orderData

        let orderDetails : ANY = await order.findOne({
            where: {
                id
            },
            attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
            include:[
              {
                as: 'subBookingType',
                model: subBookingType,
                attributes: ['name'],
                include: [{
                  as: 'bookingType',
                  model: bookingType,
                  attributes: ['typeName'],
                }]
              },
            {
              as: 'products',
              model: product,
              include:[{
                as:'productType',
                model: productType,
                attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
              },{
                as:'brands',
                model: productBrand,
                attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
              }],
              attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
            },
            {
              as: 'errorDetails',
              model: orderErrorDetails,
              attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
            },
            {
              as: 'recomendations',
              model: recomendations,
              attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
            },
            {
              as: 'orderStatus',
              model: orderStatus,
              attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
            },
            {
              as: 'orderAssignment',
              model:orderAssignment ,
              include: [{
                as: 'availableTechnicians',
                model: schAvalabilityTechnicians,
                include:[{
                  as: 'dateList',
                  model: schReccurenceDateList,
                  where: {
                    deletedAt: null,
                  }
                },{
                  as: 'users',
                  model: users,
                  attributes: ['id','firstName','lastName'],
                  where: {
                    deletedAt: null,
                  }
                }],
                where: {
                  deletedAt: null,
                }
              }],
              attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
            }]
        });        
        
        if(!orderDetails){
          await sequelize.connectionManager.close();
          return formatJSONResponse({
            statusCode: 403,
            message: 'order Id not Exist'
          });
        }

        // const data =  JSON.stringify(response)        
        
        console.log("response.customerId",orderDetails.customerId)

      let customer: any
      try{          
          
        const custResponse: ANY = await axios.post(`${process.env.CUSTOMER_MANAGEMENT}getCustomerById`, {id:orderDetails.customerId});          
          
        console.log("custResponse",custResponse)          
          
        if (custResponse.status !== 200) {
          throw Error('customer hit');
          
        }
        if(custResponse.data == null){
          throw Error('customer Id Invalid');
        }

        customer = custResponse?.data.response

      } catch(error) {
          await sequelize.connectionManager.close();
          return formatJSONResponse({
          statusCode: 403,
          message: error.message
        });
      }        
        
      const response = {orderDetails,customer}
      console.log(response)
      await sequelize.connectionManager.close();  
      return formatJSONResponse({
        statusCode: 200,
        message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

        response
      });   
      
    } 
      
  catch(error) {
    console.error(error);
    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });   
  }
}
 export const main = middyfy(getOrderById);
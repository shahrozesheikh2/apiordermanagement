import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;


export const getSingleOrderSchema =  Joi.object({
  id: Joi.number().integer().required()
})

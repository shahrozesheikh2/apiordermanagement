import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { approvelSignatureI, approvelSignature } from 'src/models';
// import addSignatureApprovel from '.';
import schema, { addSignatureApprovelSchema }  from './schema';



const addSignatureApprovel:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const data = await addSignatureApprovelSchema.validateAsync(event.body)

    const response: approvelSignatureI = await approvelSignature.create(data)
    await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
      message: `Unterschrift erfolgreich hinzugefügt genehmigen`,
      //  message: `Successfully added signature approvel`,
       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(addSignatureApprovel);
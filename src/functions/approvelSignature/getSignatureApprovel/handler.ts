import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { approvelSignature } from 'src/models';
import { ANY } from 'src/shared/common';

import schema, { getSignatureApprovelSchema } from './schema';


const getSignatureApprovel:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const data : ANY   = await getSignatureApprovelSchema.validateAsync(event.body)

    const{
      orderId
    } = data

    const response : ANY = await approvelSignature.findOne({
      where: { 
        // createdBy:userId
        deletedAt:null,
        orderId
      },
      attributes: {exclude: ['updatedAt','updatedBy','deletedBy','createdBy','deletedAt']},
    })
    
      await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode:200,
       message: `Unterschriftsgenehmigung erfolgreich erhalten`,
      //  message: `Successfully Recieved sigature approvel`,

       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode:403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getSignatureApprovel);
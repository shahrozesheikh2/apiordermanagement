import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;


export const getSignatureApprovelSchema =  Joi.object({
  orderId: Joi.number().integer().required()
})

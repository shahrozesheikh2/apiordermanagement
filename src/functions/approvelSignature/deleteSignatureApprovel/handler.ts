import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { approvelSignature } from 'src/models';
import schema, { deleteSignatureApprovelSchema }  from './schema';



const deleteSignatureApprovel:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const data = await deleteSignatureApprovelSchema.validateAsync(event.body)

    const {
        id
    } = data

    const Exists = await approvelSignature.findOne({where:{id: id, deletedAt:null}, attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}});

    // console.log("Exists",Exists)

    if(!Exists){
      await sequelize.connectionManager.close();

      return formatJSONResponse({
         statusCode: 403,
         message: `no id found as added`,
       });
    } 

   await approvelSignature.update({ deletedAt:new Date },{where:{ id: id } });
    // const response : approvelSignatureI = await approvelSignature.findOne({
    //       where: {
    //         id
    //       }
    //   });
      await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
       message: `Successfully Deleted Siganture Approvel`,
       data
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(deleteSignatureApprovel);
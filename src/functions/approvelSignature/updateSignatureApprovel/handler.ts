import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { approvelSignature, approvelSignatureI } from 'src/models';
import schema, { updateSignatureApprovelSchema }  from './schema';



const updateSignatureApprovel:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const data = await updateSignatureApprovelSchema.validateAsync(event.body)

    const {
        technicianId,
        orderId,
      ...other
    } = data

    const Exists = await approvelSignature.findOne({where:{technicianId, orderId}, attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}});

    // console.log("Exists",Exists)

    if(!Exists){
      await sequelize.connectionManager.close();

      return formatJSONResponse({
         statusCode: 403,
         message: `no data against order`,
       });
    } 

   await approvelSignature.update({
        ...other
        }, {
            where: {
                technicianId:technicianId,
                orderId:orderId
            }
        });
    const response : approvelSignatureI = await approvelSignature.findOne({
          where: {
            technicianId,
            orderId
          }
      });
      await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
       message: `Successfully Updated Siganture Approvel`,
       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(updateSignatureApprovel);
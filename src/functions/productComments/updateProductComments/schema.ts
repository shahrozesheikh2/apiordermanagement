import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  },
} as const;

export const updateProductCommentsSchema =  Joi.object({
  id: Joi.number().integer().required(),
  comments: Joi.string().required()
})
import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { problemSolutions, problemSolutionsI, productComments, productCommentsI } from 'src/models';
import schema, { updateProductCommentsSchema }  from './schema';



const updateProductComments:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    var lang = event.headers['Accept-Language'];
    var message = 'Updated Successfully'
    if(lang =='de')
    {
       message = 'Erfolgreich geupdated'
    }
    const data = await updateProductCommentsSchema.validateAsync(event.body)

    const {
      id,
      ...other
    } = data

    const Exists = await productComments.findOne({where:{id}, attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}});

    // console.log("Exists",Exists)

    if(!Exists){
       await sequelize.connectionManager.close();

      return formatJSONResponse({
         statusCode: 403,
         message: `Id Invalid`,
       });
    } 

   await productComments.update({
        ...other
        }, {
            where: {
                id:id,
            }
        });
    const response : productCommentsI = await productComments.findOne({
          where: {
            id
          }
      });
      await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
       message: message,
       response
     });
     
   } catch(error) {
     console.error(error);
      await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(updateProductComments);
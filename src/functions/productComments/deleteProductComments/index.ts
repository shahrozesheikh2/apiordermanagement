import schema from './schema';
import { handlerPath } from '@libs/handler-resolver';


export default {
  handler: `${handlerPath(__dirname)}/handler.main`,
  events: [
    {
      http: {
        method: 'post',
        path: 'deleteProductComments',
        // authorizer: 'arn:aws:lambda:eu-central-1:576223876742:function:auth-service-dev-auth',
        request: {
          schemas: {
            'application/json': schema,
          },
        },
        cors: true,
      },
    },
  ],
};

import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;


export const getProductCommentsSchema =  Joi.object({
  productId: Joi.number().integer().required()
})

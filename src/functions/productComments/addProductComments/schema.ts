import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  },
} as const;

export const addProductCommentsSchema =  Joi.object({
  productId: Joi.number().integer().required(),
  userId: Joi.number().integer().required(),
  comments: Joi.string().required()
})
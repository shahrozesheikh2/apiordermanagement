import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { productComments, productCommentsI } from 'src/models';
import schema, { addProductCommentsSchema }  from './schema';



const addProductComments:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    var lang = event.headers['Accept-Language'];
    var message = 'Successfully Added'
    if(lang =='de')
    {
       message = 'erfolgreich hinzugefügt'
    }
    const data = await addProductCommentsSchema.validateAsync(event.body)

    const {
      userId,
      ...other
    } = data

    const response: productCommentsI = await productComments.create({...other,createdBy:userId})
    await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
      message: message,
      //  message: `Successfully added`,
       response
     });
     
   } catch(error) {
     console.error(error);
      await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(addProductComments);

function createdBY(other: any, createdBY: any, userId: any): any {
  throw new Error('Function not implemented.');
}

import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { recomendations, recomendationsI } from '../../models/index';
import schema, { updateRecomendationSchema } from './schema';
import * as typings from '../../shared/common';


const updateRecomendationDetails:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'Updated Successfully'
    if(lang =='de')
    {
       message = 'Erfolgreich geupdated'
    }
    const Data :typings.ANY   = await updateRecomendationSchema.validateAsync(event.body)

    const {
      orderId,
      userId,
      // recomendationDetails
      ...otherAttributes
    } = Data

    const recomendationObj = { ...otherAttributes, updatedBy:userId}

    const Exists = await recomendations.findOne({where:{orderId}});

    if(!Exists) throw new Error("Invalid Invalid Order Id");

     await recomendations.update({
          ...recomendationObj
        }, { 
            where: {
              orderId
            }
        }); 

      const response : recomendationsI = await recomendations.findOne({
          where: {
            orderId
          }
      });
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
      message: message,
      //  message: `Successfully Updated`,
       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(updateRecomendationDetails);
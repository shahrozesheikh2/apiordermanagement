
import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;

export const updateRecomendationSchema =  Joi.object({
  userId: Joi.number().integer(),
  orderId: Joi.number().integer().required(),
  // recomendationDetails:Joi.object({
  diagnosisSystemsNeeded: Joi.boolean(),
  specialToolsNeeded:Joi.boolean(),
  customerPreparation:Joi.boolean(),
  recomendations:Joi.string(),
  resources: Joi.array().items(Joi.object({
      awsKey: Joi.string(),
      awsUrl: Joi.string().uri(),
      description:Joi.string()
  }))
  // })
})

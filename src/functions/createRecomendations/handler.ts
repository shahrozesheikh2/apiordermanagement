import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
// import * as Joi from "joi";
import { sequelize } from 'src/config/database';
import { recomendations, recomendationsI   } from '../../models/index';
import schema, { recomendationSchema }  from './schema';



const addRecomendation:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'Recommendation Added Successfully'
    if(lang =='de')
    {
       message = 'Empfehlung erfolgreich hinzugefügt'
    }
    const recomendationData = await recomendationSchema.validateAsync(event.body) //recomendationSchema.validate(event.body) 


    const response: recomendationsI  = await recomendations.create(recomendationData)
    await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 200,
       message: message,
      //  Recommendation Added Successfully
       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403, 
      message: error.message
    });
     
   }
 }
 export const main = middyfy(addRecomendation);
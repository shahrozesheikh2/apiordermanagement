import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  },
} as const;

export const recomendationSchema =  Joi.object().keys({
  orderId: Joi.number().integer().required(),
  diagnosisSystemsNeeded: Joi.boolean(),
  specialToolsNeeded:Joi.boolean(),
  customerPreparation:Joi.boolean(),
  recomendations:Joi.string().required(),
  resources: Joi.array().items(Joi.object({
    awsKey: Joi.string(),
    awsUrl: Joi.string().uri(),
    description:Joi.string()
}))
})

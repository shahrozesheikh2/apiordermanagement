import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import {  } from 'src/models/subBookingType';
import { ANY } from 'src/shared/common';
import { errorDetailsChecks} from '../../models/index';
import schema, { getErrorDetialsChecksSchema }  from './schema';



const getErrorDetialsChecks:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const data = await getErrorDetialsChecksSchema.validateAsync(event.body)

    const response : ANY = await errorDetailsChecks.findAll({
          where: {
            bookingTypeId: data.bookingTypeId
          },
          attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}
            
        }); 
        await sequelize.connectionManager.close();
     return formatJSONResponse({
       statusCode: 200,
      //  message: `Erfolgreich erhalten`,
       message: `Successfully Recieved`,
       response
     });
     
   } catch(error) {
     console.error(error);
      await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getErrorDetialsChecks);
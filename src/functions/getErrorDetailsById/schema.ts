import * as Joi from "joi";

export default {
  type: "object",
  // properties: {
  //   id:{ type :'number'},
  // }
} as const;


export const getErrorDetailsByIdSchema =  Joi.object({
  id: Joi.number().integer(),
  orderId:Joi.number().integer()
})
import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { orderSpareParts, orderSparePartsStatus, spareParts } from '../../../models/index';
import schema, { getAllByIdSchema } from './schema';
import * as typings from '../../../shared/common';


const getAllSparePartsByOrderId:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const data :typings.ANY   = await getAllByIdSchema.validateAsync(event.body)

    const{
      orderId,
      technicianId,
      orderSparePartsStatusId
    } = data

    let  whereClause: { [key: string]: typings.ANY }  = {}
  
    if (technicianId) {
      whereClause.updatedBy =  technicianId;
    }

    if(orderSparePartsStatusId){
      whereClause.orderSparePartsStatusId = orderSparePartsStatusId
    }

    // const dataExist = await orderSpareParts.findAll({where:{orderId,...whereClause}})

    // // if(!dataExist.length){
    // //   return formatJSONResponse({
    // //     statusCode: 403,
    // //     message: `invalid ID`,
    // //   });
    // // }
    
    // // const Response: typings.ANY = await orderSpareParts.findAll( { where:{ orderId }, limit:limit,
    // //   offset: offset * limit, attributes: {exclude: ['createdBy', 'updatedBy', 'deletedBy']}
    // // })

    const Response: typings.ANY = await orderSpareParts.findAll( 
      { 
        where:{ orderId ,...whereClause,deletedAt:null },
        attributes: {exclude: ['createdBy','deletedBy'],
      },
      include:[{
        as:'spareParts',
        model: spareParts,
        attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
      },{
        as:'orderSparePartsStatus',
        model: orderSparePartsStatus,
        attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
      }]
    })
    await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

       Response
     });
  } 
  catch(error) {
    await sequelize.connectionManager.close();
    console.error(error);
    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(getAllSparePartsByOrderId);
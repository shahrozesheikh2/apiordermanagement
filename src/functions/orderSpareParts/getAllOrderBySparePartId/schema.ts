import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;


export const getAllOrderBySparePartIdSchema =  Joi.object({
    sparePartId: Joi.number().integer().required(),
    offset: Joi.number().min(0).default(0),
    limit: Joi.number().max(999).default(10)
  })

import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { bookingType, customer, order, orderSpareParts, orderStatus, product, productType, subBookingType } from '../../../models/index';
import schema, { getAllOrderBySparePartIdSchema } from './schema';
import * as typings from '../../../shared/common';


const getAllOrderBySparePartId:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const data :typings.ANY   = await getAllOrderBySparePartIdSchema.validateAsync(event.body)

    const{
      sparePartId,
      limit,
      offset
    } = data


    const Response: typings.ANY = await orderSpareParts.findAndCountAll( 
      { 
        where:{ sparePartId ,deletedAt:null },
        order: [['createdAt', 'desc']],
        attributes: {exclude: ['createdBy','deletedBy'],
      },
      include:[
      {
        as:'order',
        model: order,
        include:[
          {
            as: 'subBookingType',
            model: subBookingType,
            attributes: ['id', 'name'],
            include: [
            {
              as: 'bookingType',
              model: bookingType,
              attributes: ['id', 'typeName'],
            }
          ]
          },
          { 
            as: 'products',
            model: product,
            include:[{
              as:'productType',
              model: productType,
              attributes: ['productType'],
            },
            // {
            //   as:'brands',
            //   model: productBrand,
            //   attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
            // }
          ],
            attributes: ['productTypeId'],
          }, 
          {
            as: 'orderStatus',
            model: orderStatus,
            attributes: {exclude: ['updatedAt','updatedBy','deletedBy','createdBy','deletedAt']},
          },
          {
            as: 'customer',
            model: customer,
            attributes:  ['id', 'customerAddress', 'mobileNumberExtension', 'mobileNumber', 'notes'],
          }
        ],
        attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
       
      },
      // {
      //   as:'orderSparePartsStatus',
      //   model: orderSparePartsStatus,
      //   attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
      // }
    ], 
    limit:limit,
    offset: offset * limit
    })
    await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

       Response
     });
  } 
  catch(error) {
    await sequelize.connectionManager.close();
    console.error(error);
    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(getAllOrderBySparePartId);
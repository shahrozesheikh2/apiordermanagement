import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  },
} as const;

export const orderSparePartsSchema =  Joi.object({

  orderId: Joi.number().integer().required(),
  sparePartId: Joi.number().integer(),
  reqQuantity: Joi.number().integer().required(),
  comments:Joi.string(),
  itemName: Joi.string(),
  code: Joi.string(),
  pricePerPiece: Joi.number(),
  // price: Joi.string().required(), "price": "120,20",
  // orderSparePartsStatusId: Joi.number().integer().required(),"orderSparePartsStatusId": 1S
  userId:Joi.number().integer(),
  companyId:Joi.number().integer()

})

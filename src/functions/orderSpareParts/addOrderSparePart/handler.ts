import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { company, orderSpareParts, orderSparePartsI, spareParts } from 'src/models';
import { ANY } from 'src/shared/common';
import schema, { orderSparePartsSchema }  from './schema';
import { Op } from 'sequelize';



const addOrderSpareParts:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{

    sequelize.connectionManager.initPools();
  
    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }

    sequelize.authenticate()

    const transaction = await sequelize.transaction();

    try{

      const data :ANY   = await orderSparePartsSchema.validateAsync(event.body)
  
      const {
        reqQuantity,
        orderId,
        comments,
        userId,
        itemName,
        code,
        pricePerPiece,
        companyId
      }=data
  
      let {
        sparePartId
      }=data
  
      let sparePart
      let price
      // let createSparePart;
  
  
      const amountConversion = (Value: number) => {
        return  Value.toString().replace('.', ',')
      }
  
      if(!data.sparePartId){
  
        const uniqueValue = await spareParts.findOne({
          where:{ 
              code:{ 
                  [Op.like]: code
              } 
          }
        })
  
        if(uniqueValue){
          throw new Error('this item is already in the inventory against current code');
        }
  
        const obj2 = {
          itemName,
          price: pricePerPiece,
          code,
          currentStateId:1,
          companyId,
          quantity:reqQuantity
        }
        console.log("obj2",obj2)
        sparePart = await spareParts.create(obj2,{transaction}) 
  
        sparePartId = sparePart.id
      }
  
      if(data.sparePartId){
        sparePart =  await spareParts.findOne({
          where:{ 
            id:data.sparePartId
          }
        })
        let remaningQuantity
        if (reqQuantity > sparePart.quantity ){
          return formatJSONResponse({
            statusCode: 200,
            message: `Out Of stock`
          });
        }
        remaningQuantity = sparePart.quantity - reqQuantity
        console.log("remaningQuantity",remaningQuantity)
  
      let currentStateId = sparePart.currentStateId // available
      console.log("currentStateId",currentStateId)
  
      if(remaningQuantity <= 5 && remaningQuantity > 0){
          currentStateId = 2 // low on stock
      }else if(remaningQuantity > 5){
          currentStateId = 1
      }else{
        currentStateId = 3
      }
      console.log("currentStateId After",currentStateId)
      
      await spareParts.update({quantity:remaningQuantity,currentStateId},{where:{ id:data.sparePartId },transaction})

      }
  
      const copmanyDetails = await company.findOne({
         where:{
           id:companyId
        },
        attributes:['vat']
      })
      // console.log("copmanyDetails",copmanyDetails)

      let value = reqQuantity * sparePart.price

      console.log("value",value)

      const tax = (value * copmanyDetails.vat)/100

      console.log("tax",tax)

      // value = value + tax

      price = amountConversion(value)
       
      const obj =  {orderId, sparePartId, comments, userId,price,quantity:reqQuantity,tax,type:'Unit'}
  
      const response: orderSparePartsI = await orderSpareParts.create( obj ,{transaction} )

      await transaction.commit()
  
      await sequelize.connectionManager.close();
  
       return formatJSONResponse({
         statusCode: 200,
         message: `erfolgreich hinzugefügt`,
        //  message: `Successfully added`,
         response
       });
    } 
    catch(error) {
      if (transaction) {transaction.rollback(); }
      await sequelize.connectionManager.close();
      console.error(error);
      return formatJSONResponse({
        statusCode: 403,
        message: error.message
      });
    }
  }catch(error){
    await sequelize.connectionManager.close();
    console.error(error);
    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
 }
 export const main = middyfy(addOrderSpareParts);
import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  },
} as const;

export const updateOrderPartSchema =  Joi.object({
  id: Joi.number().integer().required(),
  sparePartId: Joi.number().integer(),
  reqQuantity: Joi.number().integer(),
  description:Joi.string(),
  comments:Joi.string(),
  // price: Joi.string(),
  orderSparePartsStatusId: Joi.number().integer(),
  userId:Joi.number().integer().required(),
  companyId:Joi.number().integer()
})

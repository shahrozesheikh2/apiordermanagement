import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { company, orderSpareParts, spareParts} from 'src/models';
import { ANY } from 'src/shared/common';
import schema, { updateOrderPartSchema }  from './schema';



const updateOrderSpareParts:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    const data :ANY = await updateOrderPartSchema.validateAsync(event.body)

    const{
      id,
      userId,
      reqQuantity,
      companyId,
      ...otherAtt
    } = data

    const dataExist = await orderSpareParts.findOne({where:id})
    if(!dataExist){
      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 403,
        message: `invalid ID`,
      });
    }

    let sparePart

    const amountConversion = (Value: number) => {
      return  Value.toString().replace('.', ',')
    }

    sparePart = await spareParts.findOne({
      where:{ 
        id:dataExist.sparePartId
      }
    })

    let remaningQuantity
        if (reqQuantity > sparePart.quantity ){
          return formatJSONResponse({
            statusCode: 200,
            message: `Out Of stock`
          });
        }
        remaningQuantity = sparePart.quantity - reqQuantity
        console.log("remaningQuantity",remaningQuantity)
  
      let currentStateId = sparePart.currentStateId // available
      console.log("currentStateId",currentStateId)
  
      if(remaningQuantity <= 5 && remaningQuantity > 0){
          currentStateId = 2 // low on stock
      }else if(remaningQuantity > 5){
          currentStateId = 1
      }else{
        currentStateId = 3
      }
      console.log("currentStateId After",currentStateId)
      
      await spareParts.update({quantity:remaningQuantity,currentStateId},{where:{ id:dataExist.sparePartId }})

      const copmanyDetails = await company.findOne({
        where:{
          id:companyId
       },
       attributes:['vat']
     })

      let price
    
      const value = reqQuantity * sparePart.price

      const tax = (value * copmanyDetails.vat)/100

      price = amountConversion(value)
    
      const obj =  {...otherAtt,price,quantity:reqQuantity}
     
      const Response: ANY = await orderSpareParts.update({ ...obj,updatedAt:new Date ,updatedBy:userId,tax }, {
      where:{
        id
      }
    })
    await sequelize.connectionManager.close();
     return formatJSONResponse({
       statusCode: 200,
       message: `Successfully updated`,
       Response
     });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(updateOrderSpareParts);
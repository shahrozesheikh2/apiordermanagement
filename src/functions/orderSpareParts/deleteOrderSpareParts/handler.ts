import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { orderSpareParts } from '../../../models/index';
import schema, { deleteSchema } from './schema';
import * as typings from '../../../shared/common';


const deleteOrderSpareParts:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const data :typings.ANY   = await deleteSchema.validateAsync(event.body)

    const{
      id
    } = data

    const dataExist = await orderSpareParts.findOne({where:{id}})

    if(!dataExist){
      return formatJSONResponse({
        statusCode: 403,
        message: `invalid ID`,
      });
    }
    
    const Response: typings.ANY = await orderSpareParts.update({ deletedAt: new Date }, {
      where:{
        id
      }
    })
    await sequelize.connectionManager.close();
     return formatJSONResponse({
       statusCode: 200,
       message: `Erfolgreich gelöscht`,
      //  message: `Successfully deleted`,
       Response
     });
  } 
  catch(error) {
    await sequelize.connectionManager.close();
    console.error(error);
    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(deleteOrderSpareParts);
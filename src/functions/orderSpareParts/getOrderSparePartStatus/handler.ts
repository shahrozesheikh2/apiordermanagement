import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { ANY } from 'src/shared/common';
import schema, {  } from './schema';
import { orderSparePartsStatus } from 'src/models';



const getOrderSparePartStatus:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async()=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    // const BookingTypeData :typings.ANY   = await getSingleBookingTypeSchema.validateAsync(event.body)

    const response : ANY = await orderSparePartsStatus.findAll({
            attributes: ['id','name','key']
        }); 

      await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

       response
     });
     
   } catch(error) {
    await sequelize.connectionManager.close();
     console.error(error);
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getOrderSparePartStatus);
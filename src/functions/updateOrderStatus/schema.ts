
import * as Joi from "joi";

export default {
  type: "object",
  properties: {

  },
} as const;


export const updateOrderStatusSchema =  Joi.object({
  
  statusId:Joi.number().integer().required(),
  userId:Joi.number().integer().required(),
  orderId:Joi.number().integer().required(),
  statusReason:Joi.string()
})
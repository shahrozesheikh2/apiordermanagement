import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { order, orderI, statusActivity, statusActivityI, statusNotifications } from '../../models/index';
import schema, { updateOrderStatusSchema } from './schema';


const updateOrderStatus:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
     
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
      sequelize.authenticate()
      var lang = event.headers['accept-language']
      var message = 'Updated Order Status Successfully'
      if(lang =='de')
      {
         message = 'Erfolgreich geupdated'
      }
      const transaction = await sequelize.transaction();
      try{

        const Data  = await updateOrderStatusSchema.validateAsync(event.body)

        const {
          statusId,
          statusReason,
          userId,
          orderId
        } = Data

        const Exists = await order.findOne({where:{id:orderId}});

        console.log("Exists",Exists)

        if(!Exists){
          await sequelize.connectionManager.close();

          return formatJSONResponse({
            statusCode: 403,
            message: `Id Invalid`,
          });
        }

        const obj = { statusId, updatedBy:userId}
        
        await order.update({
                ...obj
            }, 
            {
              where: {
                    id:orderId, 
                },transaction
        });

            if(statusId){

              const statusObj = {
                statusId,
                orderId,
                userId,
                activityDateTime:new Date(),
                statusReason
              }
      
              const statusResponse: statusActivityI = await statusActivity.create(statusObj,{transaction})
              
              const statusActivityExist = await statusActivity.findAll({where:{orderId,deletedAt:null}})

              const notiArray = statusActivityExist.map(s => ({
                    userId:s.userId,
                    statusActivityId:statusResponse.id
              }))?.filter(t =>{
                if(userId != t.userId ){
                  return t
                }
              });

              let newArr = notiArray.filter((value, index, self) =>
              index === self.findIndex((t) => (
                t.userId === value.userId 
              ))
            )

              await statusNotifications.bulkCreate(newArr,{transaction})
            }
        
           
    
        await transaction.commit()
            
        const response : orderI = await order.findOne({
              where: {
                id:orderId
              }
          });
        await sequelize.connectionManager.close();

        return formatJSONResponse({
          statusCode: 200,
          message: message,
          //  message: `Successfully Updated`,
          response
        });
        
      } catch(error) {
        if (transaction) { transaction.rollback(); }
        console.error(error);
        await sequelize.connectionManager.close();

        return formatJSONResponse({
          statusCode: 403,
          message: error.message
        });
        
      }
  }catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
    
  }  
 }
 export const main = middyfy(updateOrderStatus);
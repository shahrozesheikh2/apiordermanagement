import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import {  } from 'src/models/subBookingType';
import { ANY } from 'src/shared/common';
import { recomendationsChecks } from '../../models/index';
import schema from './schema';
// import translate from "translate";
// import { Json } from 'sequelize/types/utils';


const getRecommendationChecks:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    // const data :typings.ANY  = await getRecommendationChecksSchema.validateAsync(event.body)

    const response : ANY = await recomendationsChecks.findAll({
            attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}
            
        }); 
        await sequelize.connectionManager.close();
        // let bar = await translate(JSON.stringify(response), { to: "eu" });
        //     bar       = JSON.parse(bar)
     return formatJSONResponse({
       statusCode: 200,
      //  message: `Erfolgreich erhalten`,
       message: `Successfully Recieved`,
       response
     });
     
   } catch(error) {
     console.error(error);
      await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getRecommendationChecks);
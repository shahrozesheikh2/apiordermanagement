import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;


export const deleteTechTimingsSchema =  Joi.object({
    dayIds: Joi.array().required(),
    technicianId: Joi.number().integer().required()
})



import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { techniciansTimings } from '../../models/index';
import schema, { deleteTechTimingsSchema }  from './schema';

const deleteTechniciansTimings:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
          sequelize.authenticate()
          
          const timingsdata = await deleteTechTimingsSchema.validateAsync(event.body)

          const{
            dayIds,
            technicianId
          } = timingsdata

          const exist = await techniciansTimings.findOne({
            where: {
              technicianId,
              deletedAt:null
            }
          })

        if(!exist){
          throw new Error('Technician Id not Exist')
        }

        for (const dayId of dayIds ) {

          await techniciansTimings.update({deletedAt:new Date()},{ where : {technicianId , dayId } } )
          
        }
        await sequelize.connectionManager.close();
        return formatJSONResponse({
          statusCode:200,
            message: `Successfully deteled`
        });
   
   } catch(error) {
      await sequelize.connectionManager.close();
      return formatJSONResponse({
        statusCode:403,
        message: error.message
    });
     
   }
 }
 export const main = middyfy(deleteTechniciansTimings);
import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import {  } from 'src/models/subBookingType';
import { ANY } from 'src/shared/common';
import {  invoice, order, orderStatus, orderStatusI, statusActivity } from '../../models/index';
import schema, {  } from './schema';
// import * as typings from '../../shared/common';



const getDashBoardAnalytics:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()
    // const data :ANY   = await getReachSchema.validateAsync(event.body)
    const data :ANY   = event.body

    const {
      companyId
    } = data

    // const BookingTypeData :typings.ANY   = await getSingleBookingTypeSchema.validateAsync(event.body)

    const statusType = await orderStatus.findAll(
      {
        where:{
          deletedAt:null,
        }
    });

    let orderArray : ANY = await order.findAll({
            where:{
              companyId,
              // statusId:statusType.id
            },
            attributes: ['id','createdAt'], 
            include:[
              { 
                as: 'statusActivity',
                model: statusActivity,
                include:[{
                  as: 'orderStatus',
                  model: orderStatus,
                  attributes: ['id','name','key'], 
                }],
                // where:{
                //   statusId:1
                // },
                attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}
              },{
                as: 'invoice',
                model: invoice,
              }
            ]
           
    }); 
    orderArray = JSON.parse(JSON.stringify(orderArray))
  //  console.log("ordersss",JSON.parse(JSON.stringify(orderArray)))
    let reactionTimeArr = [];
    let completedTimeArr = [];
    let InvoiceRev = 0

    for (let a of orderArray) {
      let newDateTime;
      let schDateTime;
      let completedTime
      for(let s of a.statusActivity){
        if (s.statusId == 6) // status type Id --->new
        {
           newDateTime = s.activityDateTime
        }
        if (s.statusId == 1) // status type Id --->scheduled
        {
          schDateTime = s.activityDateTime
        }
        if (s.statusId == 4)// status type Id --->comp,eted
        {
          completedTime = s.activityDateTime
        }
      }
      // console.log("invice",a.invoice)
      if(a.invoice){
        InvoiceRev = InvoiceRev + a.invoice.grandTotal
      }
      if(newDateTime && schDateTime){
        var diff = new Date (schDateTime).getTime() - new Date (newDateTime).getTime()
        console.log("newDateTime",newDateTime)
        console.log("schDateTime",schDateTime)
        console.log("diff",diff)
        reactionTimeArr.push(diff)
      }

      if(newDateTime && completedTime){
        var orderCompleteDiff = new Date (completedTime).getTime() - new Date (newDateTime).getTime()
        console.log("newDateTime",newDateTime)
        console.log("completedTime",completedTime)
        console.log("orderCompleteDiff",orderCompleteDiff)
        completedTimeArr.push(orderCompleteDiff)
      }
     
    }
    // console.log("arr",arr)
    // console.log(
    //   arr.reduce((a, b) => a + b, 0)
    // )
    let reactionAvgTime = '0:0'
    console.log("reactionTimeArr",reactionTimeArr)
    if(reactionTimeArr.length != 0){
      let avg = reactionTimeArr.reduce((a, b) => a + b, 0)/reactionTimeArr.length
      console.log("avg",avg)
      var msec = avg;
      var d = Math.floor(msec / (24*60*60*1000))
      console.log("day",d)
      const daysms = msec % (24*60*60*1000);
      var hh = Math.floor(daysms / (60*60*1000));
      const hoursms = msec % (60*60*1000);
      var mm = Math.floor(hoursms / (60*1000));
      const minutesms = msec % (60*1000);
      var ss = Math.floor(minutesms / 1000);
      console.log("average reaction Time",d+":"+hh + ":" + mm + ":" + ss)
      reactionAvgTime = `${d}:${hh}`
    }
    

    console.log("completedTimeArr",completedTimeArr)

    let fixAvgTime = "0:0"

    if(completedTimeArr.length != 0){
      let AvgTimeMs = completedTimeArr.reduce((a, b) => a + b, 0)/completedTimeArr.length
      let cDay = Math.floor(AvgTimeMs / (24*60*60*1000))
      console.log("day",cDay)
      const dms = AvgTimeMs % (24*60*60*1000);
      var cHour = Math.floor(dms / (60*60*1000));
      const hourms = AvgTimeMs % (60*60*1000);
      var cMin = Math.floor(hourms / (60*1000));
      const minms = AvgTimeMs % (60*1000);
      var cSec = Math.floor(minms / 1000);
      console.log("average Fix Time",cDay+":"+cHour + ":" + cMin + ":" + cSec)
      fixAvgTime = `${cDay}:${cHour}`  
    }
    

    const response = {
      reactionAvgTime,
      fixAvgTime,
      fixFirstRatio:0,
      InvoiceRev,
    }


     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getDashBoardAnalytics);
import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import {  } from 'src/models/subBookingType';
import { ANY } from 'src/shared/common';
import { orderStatus, roles, statusActivity, users } from '../../models/index';
import schema, { getStatusActivityByOrderIdSchema } from './schema';
import * as typings from '../../shared/common';



const getStatusActivityByOrderId:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const data :typings.ANY  = await getStatusActivityByOrderIdSchema.validateAsync(event.body)

    const orderStatusActivities : ANY = await statusActivity.findAll({
            where: {
                orderId: data.orderId,
                deletedAt:null
            },
            include:[
              {
                as:'orderStatus',
                model:orderStatus,
                where:{deletedAt:null},
                attributes:['id','name','key'],
              },
              {
              as:'users',
              model:users,
              where:{deletedAt:null},
              attributes:['id','firstName','lastName'],
              include:[{
                as:'roles',
                model:roles,
                attributes:['id','name'],
                where:{deletedAt:null},
              }]
              }
          ],
            attributes:{exclude:['createdBy','updatedAt']}
        }); 
        await sequelize.connectionManager.close();
     return formatJSONResponse({
       statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

       orderStatusActivities
     });
     
   } catch(error) {
     console.error(error);
      await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getStatusActivityByOrderId);
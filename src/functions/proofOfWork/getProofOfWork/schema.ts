import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;


export const getProofOfWorkSchema =  Joi.object({
  orderId: Joi.number().integer().required()
})

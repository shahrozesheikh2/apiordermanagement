import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  },
} as const;

export const addProofOfWorkSchema =  Joi.object({
  orderId: Joi.number().integer().required(),
  technicianId: Joi.number().integer().required(),
  title: Joi.string().required(),
  resources: Joi.object({
    awsKey: Joi.string(),
    awsUrl: Joi.string().uri(),
    description:Joi.string()
})

})
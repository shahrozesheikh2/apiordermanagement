import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import {proofOfWork, proofOfWorkI } from 'src/models';
import schema, { addProofOfWorkSchema }  from './schema';



const addProofOfWork:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()
    var lang = event.headers['Accept-Language'];
    var message = 'Created Successfully'
    if(lang =='de')
    {
       message = 'erfolgreich hinzugefügt'
    }
    const data = await addProofOfWorkSchema.validateAsync(event.body)

    const response: proofOfWorkI = await proofOfWork.create(data)
    await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
      message: message,
      //  message: `Successfully added`,
       response
     });
     
   } catch(error) {
     await sequelize.connectionManager.close();

     console.error(error);
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(addProofOfWork);
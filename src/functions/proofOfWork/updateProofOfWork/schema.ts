import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  },
} as const;

export const updateProofOfWorkSchema =  Joi.object({

  id: Joi.number().integer().required(),
  userId: Joi.number().integer(),
  title: Joi.string(),
  resources: Joi.object({
    awsKey: Joi.string(),
    awsUrl: Joi.string().uri(),
    description:Joi.string()
})
})
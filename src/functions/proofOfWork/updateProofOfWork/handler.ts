import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { proofOfWork, proofOfWorkI } from 'src/models';
import schema, { updateProofOfWorkSchema }  from './schema';



const updateProofOfWork:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'Updated Successfully'
    if(lang =='de')
    {
       message = 'Erfolgreich geupdated'
    }
    const data = await updateProofOfWorkSchema.validateAsync(event.body)

    const {
      id,
      ...other
    } = data

    const Exists = await proofOfWork.findOne({where:{id}, attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}});

    // console.log("Exists",Exists)

    if(!Exists){
       await sequelize.connectionManager.close();

      return formatJSONResponse({
         statusCode: 403,
         message: `Id Invalid`,
       });
    } 

   await proofOfWork.update({
        ...other
        }, {
            where: {
                id:id,
            }
        });
    const response : proofOfWorkI = await proofOfWork.findOne({
          where: {
            id
          }
      });
      await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
       message:message,
       response
     });
     
   } catch(error) {
     console.error(error);
    await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(updateProofOfWork);
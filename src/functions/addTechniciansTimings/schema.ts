import * as Joi from "joi";

export default {
  type: "object",
  properties: {

  },
} as const;

export const addTechTimingsSchema =  Joi.object({

 
  techniciansTiming:Joi.array().items({
    dayId: Joi.number().integer().required(),
    startTime: Joi.string().required(),
    endTime:Joi.string().required(),
    breakStartTime:Joi.string().required(),
    breakEndTime:Joi.string().required()
  }),
  technicianId: Joi.number().integer(),
  timeZone: Joi.number().integer().required(),
  userId:Joi.number().integer().required()
})

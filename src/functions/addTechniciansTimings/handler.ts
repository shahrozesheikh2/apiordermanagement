import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { techniciansTimings } from '../../models/index';
import schema, { addTechTimingsSchema }  from './schema';


const addTechniciansTimings:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const techData = await addTechTimingsSchema.validateAsync(event.body)

    const {
      techniciansTiming,
      technicianId,
      timeZone,
      userId
    } = techData
    
    const techTimingData = techniciansTiming.map((t: any) =>({
      ...t,
      technicianId,
      timeZone,
      createdBy:userId
    }))

    const response = await techniciansTimings.bulkCreate(techTimingData)
    await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode:200,
       message: `Successfully Added`,
       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode:403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(addTechniciansTimings);
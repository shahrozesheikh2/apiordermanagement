import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { techniciansTimings } from '../../models/index';
import schema, { updateTechTimingsSchema }  from './schema';



const updateTechniciansTimings:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{

    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'Updated Successfully'
    if(lang =='de')
    {
       message = 'Erfolgreich geupdated'
    }
    const techData = await updateTechTimingsSchema.validateAsync(event.body)

    const {
      techniciansTiming,
      technicianId,
      timeZone,
      userId
    } = techData
    
    const techTimingData = techniciansTiming.map(t =>({
      ...t,
      timeZone,
      createdBy:userId
    }))


    for (const timing of techTimingData ) {

      await techniciansTimings.update(timing,{where:{technicianId,dayId:timing.dayId}})

    }
    
    const response = await techniciansTimings.findAll({where:{technicianId}})
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode:200,
       message: message,
       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode:403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(updateTechniciansTimings);
import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;




export const productSchema =  Joi.object({
    orderId: Joi.number().integer(),
    // noOfProduct : Joi.number().integer().required(),
    productTypeId:Joi.number().integer().required(),
    brandId: Joi.number().integer().required(),
    modelNumber:Joi.string().required(),
    serialNumber:Joi.string().required(),
    indexNumber:Joi.number().integer().required(),
    modelYear: Joi.number().integer().required(),
    contract:Joi.string().allow(null).allow(''),
    IsWarrenty:Joi.boolean().allow(null).allow(''),
    warrantyContractNum:Joi.when('IsWarrenty',{is: true, then: Joi.number().integer().allow(null).allow('')}),
    warrentyDetails:Joi.when('IsWarrenty',{is: true, then: Joi.string().allow(null).allow('')}),
    warrentyNotes:Joi.when('IsWarrenty',{is: true, then: Joi.string().allow(null).allow('')}),
    installation:Joi.string().allow(null).allow(''),
    levelUsage:Joi.number().integer().valid(1,2,3).allow(null).allow(''),
    purchaseDate:Joi.date().allow(null).allow(''),
    priceNew:Joi.number().integer().allow(null).allow(''),
    problem:Joi.string().allow(null).allow(''),
    errorCode:Joi.string().allow(null).allow(''),
    detail:Joi.string().allow(null).allow(''),
    notes:Joi.string().allow(null).allow(''),
    recallInfo:Joi.string().allow(null).allow(''),
    other:Joi.string().allow(null).allow(''),
    resetLink:Joi.string().allow(null).allow(''),
    cleaningLink:Joi.string().allow(null).allow(''),
    resources: Joi.array().items(Joi.object({
      awsKey: Joi.string(),
      awsUrl: Joi.string().uri(),
      description:Joi.string()
  }))
  // .max(3).required(),
    // typeName:Joi.string().required()
})



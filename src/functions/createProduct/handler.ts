import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { order, product } from '../../models/index';
import schema, { productSchema }  from './schema';

const addProduct:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{

  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'Product Created Successfully'
    if(lang =='de')
    {
       message = 'Produkt erfolgreich erstellt'
    }
    const transaction = await sequelize.transaction();

    try{

    const applianceData = await productSchema.validateAsync(event.body)

    const {
      orderId
    } = applianceData

    let response = await product.findOne({where:{modelNumber:applianceData.modelNumber}})

    const number  = parseInt(`${Math.random() * 1000000}`)

    const fixFirstId = `FF-${number}`

    const productObj = {
      fixFirstId,
      ...applianceData 
  }

  if(!response){

    response  = await product.create(productObj ,{ transaction } )  

  }
 
  if(orderId){
       await order.update({productId:response.id}, { where: { id: orderId},transaction});
    }

    await transaction.commit()
    await sequelize.connectionManager.close();
     return formatJSONResponse({ 
      statusCode: 200,
       message: message,
      //  Appliance Added Successfully
       response
     });
     
   } catch(error) {
    if (transaction) {transaction.rollback(); }
    await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    }); 
   }
  }catch(error) {
    await sequelize.connectionManager.close();
    return formatJSONResponse({
      statusCode: 403,
     message: error.message
   }); 
  }
 }
 export const main = middyfy(addProduct);
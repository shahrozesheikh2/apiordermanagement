import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  },
} as const;

export const orderSchema =  Joi.object({
  orderDetails:Joi.object({
    subBookingTypeId: Joi.number().integer().required(),
    serviceProvideId: Joi.number().integer(),
    customerId: Joi.number().integer().required(),
    customTag:Joi.string()
  }).required(),
  userId:Joi.number().integer().required(),
  companyId:Joi.number().integer()
})

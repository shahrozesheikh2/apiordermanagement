import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { customer, order, orderI, orderStatus, orderStatusI,statusActivity,statusActivityI } from '../../models/index';
import schema, { orderSchema }  from './schema';

import fetch from 'node-fetch'



const createOrder:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
      sequelize.authenticate()
      const transaction = await sequelize.transaction();
      var lang = event.headers['accept-language']
      var message = 'Order Created Successfully'
      if(lang =='de')
      {
         message = 'Bestellung erfolgreich erstellt'
      }

      try{

        // sequelize.authenticate()

        const orderData = await orderSchema.validateAsync(event.body)

        const {
          orderDetails,
          userId,
          companyId
        } = orderData

        const {
          customerId
        } = orderDetails

        let count = 0;

        const orderNumber = await order.count(
          {
            where:{
              companyId,
              deletedAt:null,
            }
        });
        console.log("orderNumber",orderNumber)
        count = orderNumber + 1;
        console.log("count",count)

        const statusType: orderStatusI = await orderStatus.findOne(
          {
            where:{
              key:'new',
              deletedAt:null,
            }
        });

        const number  = parseInt(`${Math.random() * 1000000}`)

        const fixFirstId = `FF-${number}`

        const obj = {...orderDetails , createdBy:userId ,companyId, partialFilled:true,statusId:statusType.id, fixFirstId: fixFirstId,orderNum:count}

        console.log("obj",obj)
        // throw new Error
        const response: orderI = await order.create(obj,{transaction})

        const statusObj = {
          statusId:statusType.id,
          orderId:response.id,
          userId,
          activityDateTime:new Date(),
        }
        console.log("statusObj",statusObj)

        const statusResponse: statusActivityI = await statusActivity.create(statusObj,{transaction})
        
        console.log("statusResponse",statusResponse)

        const customerData = await customer.findOne({where:{id:customerId}})

        var params = {
          subject: 'Order Created',
          body: `A new order #${response.id} was successfully Created. 
          Once we have all details confirmed, we will notify you.`,
          recipient: customerData.email
        }

        const emailServiceResponse = await fetch(`${process.env.SEND_EMAIL_SES}/sendMail`, {
          method: 'POST',
          body: JSON.stringify(params),
          headers: { 'Content-Type': 'application/json' }
        })
        .then(res => res.json())
        .then(json => json)
        .catch(err => err);

        await transaction.commit()

        await sequelize.connectionManager.close();

          return formatJSONResponse({
          statusCode: 200,
            // message: `Order Created Successfully`,
            message:message,
            response,
            emailServiceResponse
          });
          
        } catch(error) {
          if (transaction) {transaction.rollback(); }
          await sequelize.connectionManager.close();
          console.error(error);
          return formatJSONResponse({
          statusCode: 403,
          message: error.message
        });
          
        }
    }catch(error) {
      console.error(error);
      await sequelize.connectionManager.close();
      return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
      
    }  
 }
 export const main = middyfy(createOrder);
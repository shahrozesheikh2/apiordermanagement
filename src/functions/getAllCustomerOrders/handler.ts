import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { order,  orderStatus, subBookingType, bookingType, customer, product, productType } from '../../models/index';
// import { productType, productBrand, product, orderErrorDetails, recomendations, orderAssignment, } from '../../models/index';
import schema, { getAllCustomerOrdersSchema } from './schema';
import * as typings from '../../shared/common';
import { Op } from 'sequelize';



const getAllCustomerOrders:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const data :typings.ANY   = await getAllCustomerOrdersSchema.validateAsync(event.body)
    const{
      // userId,
      customerId,
      limit,
      offset,
      // technicianId,
      filters
    } = data

    let  whereClause: { [key: string]: typings.ANY }  = {}


    if (filters?.bookingTypeIds && filters?.bookingTypeIds.length) {
      whereClause.bookingTypeIds = {  [Op.in]: filters.bookingTypeIds };
    }
    if ((filters?.fromDate && filters?.fromDate.length) &(filters?.toDate && filters?.toDate.length)) {
      whereClause.createdAt = {   [Op.between]:[filters.fromDate,filters.toDate] };
    }

    if (filters?.statusId) {
      whereClause.statusId =  filters.statusId;
    }


    let  response  = await order.findAndCountAll({
        where: { 
          // createdBy:userId , 
          customerId,
          deletedAt:null,
          ...whereClause
          },
          order: [['createdAt', 'desc']],
          attributes: {exclude: ['updatedAt','updatedBy','deletedBy','createdBy','deletedAt']},
        include:[
          {
            as: 'subBookingType',
            model: subBookingType,
            attributes: ['id', 'name'],
            include: [{
              as: 'bookingType',
              model: bookingType,
              attributes: ['id', 'typeName'],
            }]
          },
          { 
            as: 'products',
            model: product,
            include:[{
              as:'productType',
              model: productType,
              attributes: ['productType'],
            },
          ],
            attributes: ['productTypeId'],
          },
          {
            as: 'orderStatus',
            model: orderStatus,
            attributes: {exclude: ['updatedAt','updatedBy','deletedBy','createdBy','deletedAt']},
          },
          {
            as: 'customer',
            model: customer,
            attributes:  ['id', 'customerAddress', 'mobileNumberExtension', 'mobileNumber', 'notes'],
          },
        ],
        limit:limit,
        offset: offset * limit
      })
    
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,
       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getAllCustomerOrders);
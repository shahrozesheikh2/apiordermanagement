import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { orderErrorDetails, orderErrorDetailsI } from '../../models/index';
import schema, { erroDetailSchema }  from './schema';



const addErrorDetails:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'Error Details Added Successfully'
    if(lang =='de')
    {
       message = 'Fehlerdetails erfolgreich hinzugefügt'
    }

    const errorData = await erroDetailSchema.validateAsync(event.body)

    const {
      orderId,
      errorDetails
    } = errorData

    const data = {
      orderId,
      ...errorDetails
    }

    const response: orderErrorDetailsI = await orderErrorDetails.create(data)
    await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 200,
       message: message,
      //  message: `Error Details Added Successfully`,
       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(addErrorDetails);
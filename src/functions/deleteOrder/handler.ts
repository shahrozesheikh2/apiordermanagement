import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { order, orderErrorDetails, orderStatus, orderStatusI, recomendations } from '../../models/index';
import schema, { deleteOrderSchema }  from './schema';

const deleteOrder:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
        sequelize.authenticate()
        var lang = event.headers['Accept-Language'];
        var message = 'Successfully deleted'
        var errormessage = 'This Order Assign cannot Delete'

        if(lang =='de')
        {
           message = 'Erfolgreich gelöscht'
           errormessage = 'Diese Auftragszuweisung kann nicht gelöscht werden'
        }
        const transaction = await sequelize.transaction();

        try {
          
          const data = await deleteOrderSchema.validateAsync(event.body)

          const{
            id
          } = data

        const orderExit = await order.findOne({where:{id , deletedAt:null}})

        const statusType: orderStatusI = await orderStatus.findOne(
          {
            where:{
              key:'new',
              deletedAt:null,
            }
        });

        if(statusType.id != orderExit.statusId){
          throw new Error(errormessage)
          // throw new Error("This Order Assign cannot Delete")
        }

          await order.update({ deletedAt: new Date }, { where:{id}, transaction})

          await orderErrorDetails.update({ deletedAt:new Date },{where:{ orderId: id },transaction })

          await recomendations.update({ deletedAt:new Date },{where:{ orderId: id },transaction })

          await transaction.commit();
          await sequelize.connectionManager.close();
          return formatJSONResponse({
            statusCode: 200,
            message: message,
            // message: `Successfully deteled`,
            // response
            });
     
          
        } 
          catch(error){
                if (transaction) {transaction.rollback(); }
                console.error(error);
                await sequelize.connectionManager.close();
                return formatJSONResponse({
                  statusCode: 403,
                  message: error.message 
                })
              }

   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(deleteOrder);
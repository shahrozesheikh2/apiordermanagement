import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { order } from '../../models/index';

import { sequelize } from '../../config/database';

import schema from './schema';
// import { orderSchema } from 'src/shared/common/validators/order.validator';

const hello: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {

  try{
    console.log('HIT 1');
    sequelize.authenticate()
    console.log('HIT 2');
    const check = await order.findAll()
    console.log('connct',check)
    return formatJSONResponse({
      message: `Hello ${event.body.name}, welcomrprpe to the exciting Serverless world!`,
      check
    });
    
  } catch(error) {
    console.error(error);
    
  }
};
//  const createOrder:ValidatedEventAPIGatewayProxyEvent<typeof orderSchema> = async(event)=>{
//    try{
//     console.log('HIT 1');
//     sequelize.authenticate()
//     console.log('HIT 2');
//     // const check = await order.findAll()
//     // console.log('connct',check)
//     return formatJSONResponse({
//       message: `Hello ${event.body}, welcomrprpe to the exciting Serverless world!`,
//       // check
//     });
    
//   } catch(error) {
//     console.error(error);
    
//   }
// }
export const main = middyfy(hello);
// export const addOrder = middyfy(createOrder);


import schema from './schema';
import { handlerPath } from '@libs/handler-resolver';

// import { orderSchema } from 'src/shared/common/validators/order.validator';

export default {
  handler: `${handlerPath(__dirname)}/handler.main`,
  events: [
    {
      http: {
        method: 'post',
        path: 'hello',
        request: {
          schemas: {
            'application/json': schema,
          },
        },
      },
    },
  ],
};

import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { product, productI } from '../../models/index';
import schema, { updateProductSchema } from './schema';
import * as typings from '../../shared/common';


const updateProductDetails:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'Updated Successfully'
    if(lang =='de')
    {
       message = 'Erfolgreich geupdated'
    }
    const Data :typings.ANY   = await updateProductSchema.validateAsync(event.body)

    const {
      id,
      userId,
      productDetails
    } = Data

    const obj = { ...productDetails, updatedBy:userId}

    const Exists = await product.findOne({where:{id}});

    console.log("Exists",Exists)

    if(!Exists) throw new Error("Invalid Id");

     await product.update({
          ...obj
        }, { 
            where: {
              id
            }
        }); 

      const response : productI = await product.findOne({
          where: {
            id
          }
      });
      await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
      message: message,
      //  message: `Successfully Updated`,
       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(updateProductDetails);
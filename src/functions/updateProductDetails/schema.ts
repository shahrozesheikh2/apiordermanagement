
import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;

export const updateProductSchema =  Joi.object({
  userId: Joi.number().integer(),
  id: Joi.number().integer().required(),
  orderId:Joi.number().integer(),/////front end use less requirement remove when 
  productDetails:Joi.object({
    // noOfProduct: Joi.number().integer(), 
    productTypeId:Joi.number().integer(),
    brandId: Joi.number().integer(),
    modelNumber:Joi.string().allow(null).allow(''),
    serialNumber:Joi.string().allow(null).allow(''),
    indexNumber:Joi.number().integer().allow(null).allow(''),
    modelYear: Joi.number().integer().allow(null).allow(''),
    contract:Joi.string().allow(null).allow(''),
    IsWarrenty:Joi.boolean().allow(null).allow(''),
    warrantyContractNum:Joi.number().integer().allow(null).allow(''),
    warrentyDetails:Joi.string().allow(null).allow(''),
    warrentyNotes:Joi.string().allow(null).allow(''),
    installation:Joi.string().allow(null).allow(''),
    levelUsage:Joi.number().integer().allow(null).allow(''),
    purchaseDate:Joi.date().allow(null).allow(''),
    priceNew:Joi.number().integer().allow(null).allow(''),
    problem:Joi.string().allow(null).allow(''),
    errorCode:Joi.string().allow(null).allow(''),
    detail:Joi.string().allow(null).allow(''),
    notes:Joi.string().allow(null).allow(''),
    recallInfo:Joi.string().allow(null).allow(''),
    other:Joi.string().allow(null).allow(''),
    resetLink:Joi.string().allow(null).allow(''),
    cleaningLink:Joi.string().allow(null).allow(''),
    resources: Joi.array().items(Joi.object({
      awsKey: Joi.string().allow(null).allow(''),
      awsUrl: Joi.string().uri().allow(null).allow(''),
      description:Joi.string().allow(null).allow('')
  }))
  })
})

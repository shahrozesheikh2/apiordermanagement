import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import {  } from 'src/models/subBookingType';
import { ANY } from 'src/shared/common';
import { customer, order, orderErrorDetails, orderStatus, product, productBrand, productType, recomendations  } from '../../models/index';
import schema, { getProductByIdSchema } from './schema';
import * as typings from '../../shared/common';



const getProductById:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()
    console.log("productData")
    const productData :typings.ANY  = await getProductByIdSchema.validateAsync(event.body)

    // console.log("productData",event.queryStringParameters)

    

    const response : ANY = await product.findOne({
            where: {
                id: productData.id,
            },
            attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}, 
            include:[
              { 
                as: 'productType',
                model: productType,
                attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}
              },
              { 
                as: 'brands',
                model: productBrand,
                attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}
              },
              { 
                as: 'order',
                model: order,
                include:[{
                  as: 'orderStatus',
                  model: orderStatus,
                  attributes: ['id','name','key']
                },
                {
                  as: 'errorDetails',
                  model: orderErrorDetails,
                  attributes:['id','description','problem','errorCode','problem','resources']
                },
                {
                  as: 'recomendations',
                  model: recomendations,
                  attributes:['id','recomendations','resources']
                },
                {
                  as: 'customer',
                  model: customer,
                  attributes: ['id','firstName','lastName', 'notes'],
                }
              ],
                attributes:  ['id','customerId','subBookingTypeId', 'createdBy']
              }
            ]
            
        }); 
     await sequelize.connectionManager.close();
     return formatJSONResponse({
       statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

       response
     });
     
   } catch(error) {
     await sequelize.connectionManager.close();
     console.error(error);
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getProductById);
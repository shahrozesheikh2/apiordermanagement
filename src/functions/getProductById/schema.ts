import * as Joi from "joi";

export default {
  type: "object",
  // properties: {
  //   id:{ type :'number'},
  // }
} as const;


export const getProductByIdSchema =  Joi.object({
  id: Joi.number().integer().required(),
})

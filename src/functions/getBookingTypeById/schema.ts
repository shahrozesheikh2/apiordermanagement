import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;


export const getSingleBookingTypeSchema =  Joi.object({
  id: Joi.number().integer().required(),
})

import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import {  } from 'src/models/subBookingType';
import { ANY } from 'src/shared/common';
import { bookingType, subBookingType } from '../../models/index';
import schema, { getSingleBookingTypeSchema } from './schema';
import * as typings from '../../shared/common';



const getBookingTypeById:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const BookingTypeData :typings.ANY   = await getSingleBookingTypeSchema.validateAsync(event.body)

    const response : ANY = await bookingType.findOne({
            where: {
                id: BookingTypeData.id,
            },
            attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}, 
            include:
            { 
              as: 'subBookingType',
              model: subBookingType,
              attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}
            }
        }); 
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

       response
     });
     
   } catch(error) {
     await sequelize.connectionManager.close();
     console.error(error);
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getBookingTypeById);
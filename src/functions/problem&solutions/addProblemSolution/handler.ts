import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { problemSolutions, problemSolutionsI } from 'src/models';
import schema, { addProblemSolutionSchema }  from './schema';



const addProblemSolution:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    const data = await addProblemSolutionSchema.validateAsync(event.body)

    const response: problemSolutionsI = await problemSolutions.create(data)
    await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
      message: `erfolgreich hinzugefügt`,
      //  message: `Successfully added`,
       response
     });
     
   } catch(error) {
     console.error(error);
      await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(addProblemSolution);
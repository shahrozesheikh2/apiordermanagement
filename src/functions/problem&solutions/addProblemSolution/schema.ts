import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  },
} as const;

export const addProblemSolutionSchema =  Joi.object({
  orderId: Joi.number().integer().required(),
  technicianId: Joi.number().integer().required(),
  problemTitle: Joi.string().required(),
  problemDescription: Joi.string().required(),
  solutionTitle: Joi.string().required(),
  solutionDescription: Joi.string().required()
})
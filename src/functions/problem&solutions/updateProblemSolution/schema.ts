import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  },
} as const;

export const updateProblemSolutionSchema =  Joi.object({

  id: Joi.number().integer().required(),
  userId: Joi.number().integer(),
  problemTitle: Joi.string(),
  problemDescription: Joi.string(),
  solutionTitle: Joi.string(),
  solutionDescription: Joi.string()
})
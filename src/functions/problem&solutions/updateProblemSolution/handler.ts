import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { problemSolutions, problemSolutionsI } from 'src/models';
import schema, { updateProblemSolutionSchema }  from './schema';



const updateProblemSolution:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    const data = await updateProblemSolutionSchema.validateAsync(event.body)

    const {
      id,
      ...other
    } = data

    const Exists = await problemSolutions.findOne({where:{id}, attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}});

    // console.log("Exists",Exists)

    if(!Exists){
       await sequelize.connectionManager.close();

      return formatJSONResponse({
         statusCode: 403,
         message: `Id Invalid`,
       });
    } 

   await problemSolutions.update({
        ...other
        }, {
            where: {
                id:id,
            }
        });
    const response : problemSolutionsI = await problemSolutions.findOne({
          where: {
            id
          }
      });
      await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
       message: `Successfully Updated`,
       response
     });
     
   } catch(error) {
     console.error(error);
      await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(updateProblemSolution);
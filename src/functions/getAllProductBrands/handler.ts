import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import {  } from 'src/models/subBookingType';
import { ANY } from 'src/shared/common';
import { productBrand } from '../../models/index';
import schema, {  } from './schema';



const getAllProductBrands:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()
    var lang = event.headers['accept-language']

    // const BookingTypeData :typings.ANY   = await getSingleBookingTypeSchema.validateAsync(event.body)

    const brands : ANY = await productBrand.findAll({
            where:{deletedAt:null},
            attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}, 
        }); 
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,
       brands
     });
     
   } catch(error) {
     await sequelize.connectionManager.close();
     console.error(error);
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getAllProductBrands);
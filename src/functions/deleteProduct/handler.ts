import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { order, product } from '../../models/index';
import schema, { deleteProductSchema }  from './schema';

const deleteProduct:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
          sequelize.authenticate()
          var lang = event.headers['Accept-Language'];
          var message = 'Successfully deleted'
          var errormessage = 'Order Exist against This Product First Delete Order'
  
          if(lang =='de')
          {
             message = 'Erfolgreich gelöscht'
             errormessage = 'Bestellung vorhanden für dieses Produkt Erste Bestellung löschen'
          }
          const applianceData = await deleteProductSchema.validateAsync(event.body)

          const{
            id
          } = applianceData

          const productData = await product.findOne({
            where: {
               id,
               deletedAt:null
            }
          })

        if(!productData){
          throw new Error('Invalid Id')
        }

        const orderExit = await order.findOne({where:{productId:id , deletedAt:null}})

        if(orderExit){
          throw new Error(errormessage)
          // throw new Error("Order Exist against This Product First Delete Order")

        }

        const data = await product.update({ deletedAt:new Date },{where:{ id } })
          await sequelize.connectionManager.close();
          return formatJSONResponse({
            statusCode: 200,
            message: message,
            data
            });
   
   } catch(error) {
      await sequelize.connectionManager.close();
      return formatJSONResponse({
        statusCode: 403,
        message: error.message
    });
     
   }
 }
 export const main = middyfy(deleteProduct);
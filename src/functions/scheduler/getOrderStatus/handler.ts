import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { orderStatus } from '../../../models/index';
import schema from './schema';
import * as typings from '../../../shared/common';


const getOrderStatus:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async()=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    // const data : typings.ANY   = await getTechTimingsSchema.validateAsync(event.body)

    const response : typings.ANY = await orderStatus.findAll({
      where: { 
        // createdBy:userId
        deletedAt:null,
      },
      attributes:['id','name','key']
    })

  
      if(!response){
        throw Error("Data not Exist")
      }
      await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode:200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode:403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getOrderStatus);
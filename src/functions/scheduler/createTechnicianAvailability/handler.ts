import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { Op } from 'sequelize';
import { sequelize } from 'src/config/database';
import { schAvalabilityTechnicians, schAvalabilityTechniciansI, schReccurenceDateList, schRecurrenceDayList, schRecurrenceEndingCriteria, techniciansTimings, techniciansTimingsI } from 'src/models';
import { ANY } from 'src/shared/common';
import schema, { availableTechnicianSchema } from './schema';


// const createDaysAndDatesMethod = {
//   false: 'createDaysAndDatesWithoutRecurrence',
//   true: 'createDaysAndDatesWithRecurrence'
// }

const addTechnicianAvailability: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {

  try {
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    const transaction = await sequelize.transaction();

    sequelize.authenticate()

    try {
      const data = await availableTechnicianSchema.validateAsync(event.body)

      const {
        // userId,
        technician
      } = data

      let {
        description,
        technicianId,
        avlForId,
        startDate: startDateString,
        endDate: endDateString,
        timeZone,
        endDateForRecurrence,
        // days,
        title,
        recurrenceEndingCriteriaId
      } = technician

      const techStartDate: Date = new Date(startDateString);
      const techEndDate: Date = new Date(endDateString);

      if (new Date(startDateString).getTime() < new Date(new Date().setUTCHours(0, 0, 0, 0)).getTime()) {
        // throw Error(' Assignment Not created On Previous Date');
      await sequelize.connectionManager.close();
        return formatJSONResponse({
          statusCode:403,
          message: 'Assignment Not created On Previous Date'
        });
      }

      if (!recurrenceEndingCriteriaId) {

        const checkDuplicate = await schAvalabilityTechnicians.findOne({
          where: {
            technicianId,
            avlForId,
            deletedAt: null,
          },
          include: {
            as: 'dateList',
            model: schReccurenceDateList,
            where: {
              deletedAt: null,
              [Op.or]: [
                {
                  endDate: {
                    [Op.and]: [
                      {
                        [Op.gt]: techStartDate
                      },
                      {
                        [Op.lte]: techEndDate
                      }
                    ]
                  }
                },
                {
                  startDate: { [Op.between]: [techStartDate, techEndDate] }
                }
              ]
            }
          }
        });

        if (checkDuplicate && Object.keys(checkDuplicate).length) {
         await transaction.commit()
          await sequelize.connectionManager.close();
        
          return formatJSONResponse({
            statusCode:403,
            message: 'SAME ASSIGNMENT EXIST'
          });
        }
      }

      let endingCriteriaObj
      let endingCriteria

      if (recurrenceEndingCriteriaId) {

        endingCriteriaObj = await schRecurrenceEndingCriteria.findOne({ where: { id: recurrenceEndingCriteriaId } })
        const { name: endingCriteriaString } = endingCriteriaObj;
        endingCriteria = endingCriteriaString ?? '';
      }
      console.log("endingCriteria", endingCriteria)

      const checkForRecurrence: boolean = !recurrenceEndingCriteriaId ? false : true;

      console.log("checkForRecurrence", checkForRecurrence)

      let techTimings = await techniciansTimings.findAll({ where: { technicianId, deletedAt: null } })

      techTimings = JSON.parse(JSON.stringify(techTimings))

      ///check tech Timings
      // const checkedTechTimings: techniciansTimingsI[] = await checkTechniciansTimings(techTimings, startDateString, endDateString, checkForRecurrence, endingCriteria)
      // console.log("checkedTechTimings", checkedTechTimings)

      // if (!checkedTechTimings.length) {
      //   return formatJSONResponse({
      //     statusCode:403,
      //     message: 'Technicians Timing Dose Not Fall'
      //   });
      // }
      ///check tech Timings End
      // let formatedDays = [0,1,2,3,4,5,6]; // for days
      if (endingCriteria === 'daily') {
        ///day For Recurnce 
        // formatedDays = checkedTechTimings.map((d: ANY): ANY => d.dayId)
        ///day For Recurnce End
        //.filter((x: ANY): ANY => formatedDays.includes(x));
      }
      // console.log("formatedDays", formatedDays)
    
      const avlTechnicians = await schAvalabilityTechnicians.findAll(
        {
          where: {
            deletedAt: null,
            technicianId,
          },
          include: {
            as: 'dateList',
            model: schReccurenceDateList,
            where: {
              deletedAt: null,
              [Op.or]: [
                {
                  endDate: {
                    [Op.and]: [
                      {
                        [Op.gt]: techStartDate
                      },
                      {
                        [Op.lte]: techEndDate
                      }
                    ]
                  }
                },
                {
                  startDate: { [Op.between]: [techStartDate, techEndDate] }
                }
              ]
            }
          }
        })

      if (avlTechnicians.length) {
        throw Error("Technician Already Assign")
      }
      // throw new Error("stop")
      const newAvailableTechnician: schAvalabilityTechniciansI = await schAvalabilityTechnicians.create({
        description,
        technicianId,
        avlForId,
        startDate:techStartDate,
        endDate:techEndDate,
        timeZone,
        title
      }, { transaction })

      // const newAvailableTechnician = {
      //   id: 1
      // }

      const daysAndDatesMethod: boolean = recurrenceEndingCriteriaId ? true : false;

      console.log("daysAndDatesMethod", daysAndDatesMethod)
      // newAvailableTechnician.id
      // console.log("createDaysAndDatesMethod", createDaysAndDatesMethod[`${daysAndDatesMethod}`])
      //  [createDaysAndDatesMethod[`${daysAndDatesMethod}`]]({
      //   availableTechnicianId: 1,
      //   avlForId,
      //   technicianId,
      //   startDateString,
      //   endDateString,
      //   transaction
      // })

      const daysAndDateObj = {
        availableTechnicianId: newAvailableTechnician.id,
        avlForId,
        technicianId,
        startDateString,
        endDateString,
        // formatedDays,
        endingCriteria,
        // techTimings,
        endDateForRecurrence,
        transaction
      }

      if (daysAndDatesMethod == false) {
        await createDaysAndDatesWithoutRecurrence(daysAndDateObj)
      }
      else if (daysAndDatesMethod == true) {
        await createDaysAndDatesWithRecurrence(daysAndDateObj)
      }

      await transaction.commit();
      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode:200,
        message: `Erfolgreich erstellen`,
        //  message: `Successfully Create`,
        newAvailableTechnician
      });

    } catch (error) {
      if (transaction) { transaction.rollback(); }
      console.error(error);
      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode:403,
        message: error.message
      });
    }
  } catch (error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode:403,
      message: error.message
    });

  }
}

export const main = middyfy(addTechnicianAvailability);


const checkTechniciansTimings = async (techTimings, startDate: string, endDate: string, withRecurrence: boolean, endingCriteria?: string): Promise<techniciansTimingsI[]> => {

  if (!withRecurrence) {
    const filteredTimings = techTimings.filter(t => {
      const {
        dayId,
        startTime,
        endTime,
        timeZone
      } = t

      if (dayId == new Date(startDate).getDay()) {

        const techStartDate: Date = convertDateToLocal(new Date(`${startDate.slice(0, 10)}T${String(startTime)}.000Z`), timeZone);
        const techEndDate: Date = convertDateToLocal(new Date(`${endDate.slice(0, 10)}T${String(endTime)}.000Z`), timeZone);

        // console.log("techStartDate",techStartDate)
        // console.log("techEndDate",techEndDate)

        const startDateWithTimezone: Date = convertDateToLocal(new Date(startDate), timeZone);
        const endDateWithTimezone: Date = convertDateToLocal(new Date(endDate), timeZone);

        // console.log("startDateWithTimezone",startDateWithTimezone)
        // console.log("endDateWithTimezone",endDateWithTimezone)

        if (techStartDate.getTime() <= startDateWithTimezone.getTime() && startDateWithTimezone.getTime() <= techEndDate.getTime() && techStartDate.getTime() <= endDateWithTimezone.getTime() && endDateWithTimezone.getTime() <= techEndDate.getTime()) {
          console.log("done")
          return t;
        }

      }


    })
    return filteredTimings;
  }
  if (endingCriteria === 'daily') {
    return [...techTimings]
  }


}

console.log(checkTechniciansTimings);


const convertDateToLocal = (date: Date, timeZone: number) => {
  return new Date(date.setMinutes(date.getMinutes() - timeZone));
}


const createDaysAndDatesWithoutRecurrence = async (obj: ANY): Promise<ANY> => {

  const {
    availableTechnicianId,
    avlForId,
    technicianId,
    startDateString:startDate,
    endDateString:endDate,
    // techTimings,
    transaction } = obj;
  //Technicians timing checks
  // const userTimingDays: number[] = techTimings.map(t => t.dayId);
  
  // if (!userTimingDays.includes(new Date(startDate).getDay())) {
  //   throw Error('Technicians Not Available');
  // }
 //Technicians timing checks End
  const checkDuplicate = await schAvalabilityTechnicians.findOne({
    where: {
      technicianId,
      avlForId,
      deletedAt: null,
    },
    include: {
      as: 'dateList',
      model: schReccurenceDateList,
      where: {
        deletedAt: null,
        [Op.or]: [
          {
            endDate: {
              [Op.and]: [
                {
                  [Op.gt]: startDate
                },
                {
                  [Op.lte]: endDate
                }
              ]
            }
          },
          {
            startDate: { [Op.between]: [startDate, endDate] }
          }
        ]
      }
    }
  });

  if (checkDuplicate && Object.keys(checkDuplicate).length) {
    throw Error('SAME ASSIGNMENT EXIST');
  }

  const dayId = new Date(startDate).getDay();

  await schRecurrenceDayList.create({ availableTechnicianId, dayId }, transaction);

  await schReccurenceDateList.create({ availableTechnicianId, startDate: new Date(startDate), endDate: new Date(endDate) }, transaction);

}

const createDaysAndDatesWithRecurrence = async (obj: ANY): Promise<ANY> => {

  // const { endAfterOccurences } = obj;

  const {
    availableTechnicianId,
    avlForId,
    technicianId,
    startDateString: startDate,
    endDateString: endDate,
    // techTimings,
    endingCriteria,
    formatedDays,
    endDateForRecurrence,
    transaction } = obj;

  console.log("with recurrence obj",obj)


  const checkForDateCriteria: boolean = endDateForRecurrence ? true : false;

  const formatDateObj = {
    startDateString: startDate,
    endDateString: endDate,
    daysList: formatedDays,
    recurrenceEndDateString: endDateForRecurrence,
    endingCriteria
  }

  let formatedDates;

  if (checkForDateCriteria == true) {
    formatedDates = await formatDatesCriteriaWithEndDate(formatDateObj)
  }


  ////////////////////////////////////////
  if (formatedDates && formatedDates.length) {

    for (const formatedDate of formatedDates) {

      const checkDuplicate = await schAvalabilityTechnicians.findOne({
        where: {
          technicianId,
          avlForId,
          deletedAt: null,
        },
        include: {
          as: 'dateList',
          model: schReccurenceDateList,
          where: {
            deletedAt: null,
            [Op.or]: [
              {
                endDate: {
                  [Op.and]: [
                    {
                      [Op.gt]: formatedDate.startDate
                    },
                    {
                      [Op.lte]: formatedDate.endDate
                    }
                  ]
                }
              },
              {
                startDate: { [Op.between]: [formatedDate.startDate, formatedDate.endDate] }
              }
            ]
            // startDate: { [Op.between]: [formatedDate.startDate, formatedDate.endDate] },
          }
        }
      });

      if (checkDuplicate && Object.keys(checkDuplicate).length) {
        throw Error('SAME ASSIGNMENT EXIST');
      }

    }
  }
  formatedDates = formatedDates.map(c =>({
    ...c,
    availableTechnicianId
  }))
  console.log("formatedDates", formatedDates)

  if (formatedDates && formatedDates.length) {
    await schReccurenceDateList.bulkCreate([...formatedDates], transaction);
  }

  let recurrenceDayslist

  // console.log("formatedDates[0].startDate", formatedDates[0].startDate.getDay())

  if (endingCriteria === 'daily') {
    recurrenceDayslist = [...new Set(formatedDates.map((d => d.startDate.getDay())))]
  }
  // console.log("recurrenceDayslist", recurrenceDayslist)

  const formatedRecurrenceDays = recurrenceDayslist.map(d => ({
    availableTechnicianId,
    dayId: d
  }))

  // console.log("formatedRecurrenceDays", formatedRecurrenceDays)

  await schRecurrenceDayList.bulkCreate([...formatedRecurrenceDays], transaction);

}

const formatDatesCriteriaWithEndDate = async (obj: ANY) => {

  console.log("obj",obj)
  let {
    // daysList, 
    endDateString, 
    // endingCriteria, 
    recurrenceEndDateString, startDateString
  } = obj;
  const startDate: Date = new Date(startDateString);
  const endDate: Date = new Date(endDateString);
  const recurrenceEndDate: Date = new Date(recurrenceEndDateString);


  const startDateTime: number = startDate.getTime();
  const endDateTime: number = endDate.getTime();

  // console.log("startDateTime", startDateTime)
  // console.log("endDateTime", endDateTime)
  // console.log("recurrenceEndDateTime", recurrenceEndDate.getTime())



  const duration: number = endDateTime - startDateTime;

  // console.log("duration", duration)

  let date: Date[] = [];
  let formatedDates = [];

  while (startDate.getTime() <= recurrenceEndDate.getTime()) {

    console.log("startDateInLoop", startDate)
    console.log("recurrenceEndDateInLoop", recurrenceEndDate)

    // if daylist Exist start
     // const daysList = [0,1,2,3,4,5,6]
    // const newDateList = daysList.map((_j: number, index: number): Date => {
    //   if (startDate.getDay() === daysList[index]) {
    //     // console.log("startDate.getDay()", startDate.getDay())
    //     return new Date(JSON.parse(JSON.stringify(startDate)));
    //   }
    // })
    // console.log("newDateList", newDateList)

    // const filteredDate = newDateList.filter(d => d !== null && d !== undefined)
    // date = [...date, ...filteredDate];

    //  daylist Exist End

    date.push(new Date(JSON.parse(JSON.stringify(startDateString))))
    // console.log("startDate JSON=======>", new Date(JSON.parse(JSON.stringify(startDate))))
    // console.log("startDate =======>", startDate)
    startDate.setTime(startDate.getTime() + 1000 * 60 * 60 * 24);
    startDateString = startDate
    console.log("startDateString =======>", startDateString)
  }
  
  console.log("date", date)

  formatedDates = date?.map((d: Date): { endDate: Date; startDate: Date } => {
    const expectedRecurrenceEndDate: Date = new Date(d.getTime() + duration);
    // console.log("expectedRecurrenceEndDate", expectedRecurrenceEndDate)

    return {
      endDate: expectedRecurrenceEndDate,
      startDate: d
    };
  }) || [];
  console.log("formatedDates", formatedDates)
 
  return formatedDates;

}

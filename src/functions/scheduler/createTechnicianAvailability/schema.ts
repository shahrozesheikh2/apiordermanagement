import * as Joi from "joi";

export default {
  type: "object",
  properties: {
   
  },
} as const;

export const availableTechnicianSchema =  Joi.object({
  userId: Joi.number().integer().required(),
  technician:Joi.object({
    description:Joi.string().required(),
    technicianId:Joi.number().integer().required(),
    avlForId:Joi.number().integer(),//companyId
    startDate:Joi.string().required(),
    endDate:Joi.string().required(),
    recurrenceEndingCriteriaId:Joi.number().integer(),
    timeZone:Joi.number().integer().required(),
    endDateForRecurrence:Joi.string(),
    duration:Joi.string(),
    title:Joi.string(),
  }).required(),
 
})

import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { Op } from 'sequelize';
import { sequelize } from 'src/config/database';
import { orderAssignment, schAvalabilityTechnicians, schReccurenceDateList, users} from 'src/models';
// import { ANY } from 'src/shared/common'; 
import schema, { availableTechnicianSchema }  from './schema';



const getAllAvailableTechnicians:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    
  try{

    sequelize.authenticate()

    const data  = await availableTechnicianSchema.validateAsync(event.body)

    const {
      startDateTime,
      endDateTime,
      companyId,
    } = data


    const startDate: Date = new Date(startDateTime);
    const endDate: Date = new Date(endDateTime);
   

    let techniciansAvailability = await schAvalabilityTechnicians.findAll({
      where:{
        deletedAt:null,
        avlForId: companyId,
      },
      include:[ 
        {
        as: 'dateList',
        model: schReccurenceDateList,
        where: {
          deletedAt: null,
          [Op.or]: [
            {
              endDate: {
                [Op.and]: [
                  {
                    [Op.gt]: startDate
                  },
                  {
                    [Op.lte]: endDate
                  }
                ]
              }
            },
            {
              startDate: { [Op.between]: [startDate, endDate] }
            }
          ]
        }
        },
        {
          as: 'orderAssignment',
          model: orderAssignment,
          required:false,
          where:{
            deletedAt:null
          }
        }
    ]
    })

  techniciansAvailability = techniciansAvailability.filter(t =>t.orderAssignment == null)

  // techniciansAvailability = JSON.parse(JSON.stringify(techniciansAvailability))

  console.log("techniciansAvailability",techniciansAvailability);
  
  const techIds = techniciansAvailability.map(t=>t.technicianId);

  const availableTechnicains = await users.findAll(
    {
      where:{ id:{[Op.in]:techIds},
              deletedAt:null
            },
      attributes:['id','firstName','lastName']
  })

  
     return formatJSONResponse({
       statusCode: 200,
       message: `Successfully Recieved`,
       availableTechnicains
     });
     
   } catch(error) {
     console.error(error);
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }

 export const main = middyfy(getAllAvailableTechnicians);


  
import * as Joi from "joi";

export default {
  type: "object",
  properties: {
   
  },
} as const;

export const availableTechnicianSchema =  Joi.object({
  companyId: Joi.number().integer().required(),
  startDateTime:Joi.string().required(),
  endDateTime:Joi.string().required()
})

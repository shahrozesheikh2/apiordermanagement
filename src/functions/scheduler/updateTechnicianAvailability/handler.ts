import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { Op } from 'sequelize';
import { sequelize } from 'src/config/database';
import { orderAssignment, schAvalabilityTechnicians,  schReccurenceDateList } from 'src/models';
// import { checkTechniciansTimings } from 'src/shared/common/schedulerFunction';
import schema, { updateTechnicianAvailabilitySchema } from './schema';

const updateTechnicianAvailability: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {

  try {
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    const transaction = await sequelize.transaction();

    sequelize.authenticate()

    try {
      const data = await updateTechnicianAvailabilitySchema.validateAsync(event.body)

      const {
        userId,
        availableTechnician
      } = data

      let {
        id,
        description,
        technicianId,
        avlForId,
        dateListId,
        startDate: startDateString,
        endDate: endDateString,
        timeZone,
        duration,
        title
      } = availableTechnician

      const techStartDate: Date = new Date(startDateString);
      const techEndDate: Date = new Date(endDateString);

      if (new Date(startDateString).getTime() < new Date(new Date().setUTCHours(0, 0, 0, 0)).getTime()) {
      await sequelize.connectionManager.close();

        return formatJSONResponse({
          statusCode:403,
          message: 'Assignment Not updated On Previous Date'
        });
      }

      const availableExist = await schAvalabilityTechnicians.findByPk(id)

      console.log("availableExist",availableExist)


      if (!availableExist || !Object.keys(availableExist).length){
      await sequelize.connectionManager.close();

        return formatJSONResponse({
          statusCode:403,
          message: 'Invalid Avaiability Id'
        });
      }
      

      // let techTimings = await techniciansTimings.findAll({ where: { technicianId, deletedAt: null } })

      // techTimings = JSON.parse(JSON.stringify(techTimings))
      // console.log("techTimings",techTimings)

      // technicians Timings check
      
      // const checkedTechTimings: techniciansTimingsI[] = await checkTechniciansTimings(techTimings, startDateString, endDateString)
      // console.log("checkedTechTimings", checkedTechTimings)

      // if (!checkedTechTimings.length) {
      //   return formatJSONResponse({
      //     statusCode:403,
      //     message: 'Technicians Timing Dose Not Fall'
      //   });
      // }
      // technicians Timings check end
      
      const avlTechniciansDateList = await schAvalabilityTechnicians.findAll(
        {
          where: {
            deletedAt: null,
            technicianId,
          },
          include: [{
            as: 'dateList',
            model: schReccurenceDateList,
            where: {
              deletedAt: null,
              [Op.or]: [
                {
                  endDate: {
                    [Op.and]: [
                      {
                        [Op.gt]: techStartDate
                      },
                      {
                        [Op.lte]: techEndDate
                      }
                    ]
                  }
                },
                {
                  startDate: { [Op.between]: [techStartDate, techEndDate] }
                }
              ]
            },
            include:[{
              as: 'orderAssignment',
              model: orderAssignment,
              required:false,
              where:{
                deletedAt:null
              }
            }]
          }
        ]
        })

       
      

      if (avlTechniciansDateList.length>1 || (avlTechniciansDateList.length === 1 && avlTechniciansDateList[0].id !== id)) {
      await sequelize.connectionManager.close();
        
        return formatJSONResponse({
          statusCode:403,
          message: 'No Technician Available'
        });
      }

      let checkOrder = false

      for (let single of avlTechniciansDateList){
        console.log("single",JSON.parse(JSON.stringify(single.dateList)))

        if(single.dateList[0].orderAssignment != null){
          console.log("saasdsd")
          checkOrder = true
          break
        }
      }
      console.log("checkOrder",checkOrder)

      if(checkOrder){
        // await sequelize.connectionManager.close();

        return {
          statusCode: 403,
          body: JSON.stringify({
              message: 'Order Exist Against Current Availablity',
          }),
      }
    }

       await schAvalabilityTechnicians.update({
          description,
          technicianId,
          avlForId,
          startDate:techStartDate,
          endDate:techEndDate,
          timeZone,
          title,
          duration 
        },
        {where:{id:id},transaction})
        console.log("dateListId",dateListId)
        await schReccurenceDateList.update({
          startDate:techStartDate,
          endDate:techEndDate,
          updatedAt: new Date(),
          updatedBy: userId,
        },
        {where:{id:dateListId},transaction})
        await transaction.commit();
      const updatedAvailability = await schAvalabilityTechnicians.findOne(
        {
          where: {
            deletedAt: null,
            id,
          },
          include: {
            as: 'dateList',
            model: schReccurenceDateList,
            where: {
              deletedAt: null,
              id:dateListId
            }
          }
        })

    
        await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode:200,
        message: `Erfolgreich erstellen`,
        //  message: `Successfully Create`,
        updatedAvailability
      });

    } catch (error) {
      if (transaction) { transaction.rollback(); }
      console.error(error);
      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode:403,
        message: error.message
      });
    }
  } catch (error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode:403,
      message: error.message
    });

  }
}

export const main = middyfy(updateTechnicianAvailability);

import * as Joi from "joi";

export default {
  type: "object",
  properties: {
   
  },
} as const;

export const updateTechnicianAvailabilitySchema =  Joi.object({
  userId: Joi.number().integer().required(),
  availableTechnician:Joi.object({
    id:Joi.number().integer(),
    description:Joi.string(),
    dateListId:Joi.number().integer(),
    technicianId:Joi.number().integer(),
    avlForId:Joi.number(),
    startDate:Joi.string(),
    endDate:Joi.string(),
    timeZone:Joi.number().integer(),
    title:Joi.string(),
    duration:Joi.string(),
  }).required(),
 
})

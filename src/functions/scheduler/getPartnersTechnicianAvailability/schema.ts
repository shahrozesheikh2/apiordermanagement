import * as Joi from "joi";

export default {
  type: "object",
  properties: {
   
  },
} as const;

export const getPartnerAvailabilitySchema =  Joi.object({
  userId: Joi.number().integer(),
  startDateTime:Joi.string(),
  endDateTime:Joi.string(),
  partnerId:Joi.array().items(Joi.number()).required(), //companyId
})

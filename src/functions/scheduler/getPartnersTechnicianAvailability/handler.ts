import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { Op } from 'sequelize';
import { sequelize } from 'src/config/database';
import { orderAssignment, roles, schAvalabilityTechnicians, schReccurenceDateList, users } from 'src/models';
// import { ANY } from 'src/shared/common'; 
import schema, { getPartnerAvailabilitySchema }  from './schema';



const getPartnersTechnicianAvailability:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    const data  = await getPartnerAvailabilitySchema.validateAsync(event.body)

    console.log(data)

    const {
      partnerId,//companyId
      startDateTime,
      endDateTime,
      // userId,

    } = data


    const startDate: Date = new Date(startDateTime);
    const endDate: Date = new Date(endDateTime);

  //   const userData = await users.findOne({
  //     where:{id:partnerId , deletedAt:null},
  //     attributes:['companyId']
  //   })
  //  console.log("companyId",userData.companyId)

  //  await users.findAll({
  //   where:{id:userData.companyId,deletedAt:null},
  // })

  const technicianIds = (await users.findAll({
    where:{companyId:partnerId,deletedAt:null,emailVerified:true},
    include:{
      as:'roles',
      model:roles,
      where:{ key:'technician'},
      attributes:['name']
    }
  })).map((t): number => t.id);
  console.log("technicianIds",technicianIds)


    let techniciansAvailability:any = await schAvalabilityTechnicians.findAll({
      where:{
        deletedAt:null,
        technicianId: { [Op.in]: technicianIds },
      },
      order: [['startDate', 'ASC']],
      include:[ 
        {
        as: 'dateList',
        model: schReccurenceDateList,
        where: {
          deletedAt: null,
          [Op.or]: [
            {
              endDate: {
                [Op.and]: [
                  {
                    [Op.gt]: startDate
                  },
                  {
                    [Op.lte]: endDate
                  }
                ]
              }
            },
            {
              startDate: { [Op.between]: [startDate, endDate] }
            }
          ]
        }
      },
      {
        as: 'orderAssignment',
        model: orderAssignment,
        required:false,
        where:{
          deletedAt:null
        }
      }
    ]
    })
    techniciansAvailability = JSON.parse(JSON.stringify(techniciansAvailability))

    techniciansAvailability = techniciansAvailability.filter(t =>t.orderAssignment == null)
   
    if(techniciansAvailability){
      let array = [];
      let arrobj = [];
      let finalarr = [];
      let objStartDate1;
      let objEndDate1;

      for (let i = 0; i < techniciansAvailability.length; i++) {
        // console.log('data1', techniciansAvailability[i]);
        let data1 = techniciansAvailability[i].dateList;
        arrobj = [];
        for (let j = 0; j < data1.length; j++) {
          // console.log('datelist1', data1[j]);
          objStartDate1 = data1[j].startDate;
          objEndDate1 = data1[j].endDate;
      
          if (!array.includes(objStartDate1, objEndDate1)) {
            array.push(objStartDate1, objEndDate1);
            arrobj.push(data1[j]);
            console.log('obj', arrobj);
            techniciansAvailability[i].dateList = arrobj;
            finalarr.push(techniciansAvailability[i]);
      
            // techniciansAvailability[i].dateList = data1[j];
            // finalarr.push(techniciansAvailability);
          }
        }
      }
      techniciansAvailability = finalarr
    }
  // techniciansAvailability = JSON.parse(JSON.stringify(techniciansAvailability))

  await sequelize.connectionManager.close();
  
  return formatJSONResponse({
       statusCode: 200,
      //  message: `Erfolgreich erhalten`,
       message: `Successfully Recieved`,
      techniciansAvailability
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }

 export const main = middyfy(getPartnersTechnicianAvailability);


  
import * as Joi from "joi";

export default {
  type: "object",
  properties: {
   
  },
} as const;

export const updateForwardedOrderAssignmentSchema =  Joi.object({
  userId: Joi.number().integer().required(),
  orderFdPartner:Joi.object({
    id:Joi.number().required(),
    comments:Joi.string(),
    fdPartnerId:Joi.number().integer().required(),//companyId
    orderId:Joi.number().integer().required(),
    startDateTime:Joi.string(),
    endDateTime:Joi.string(),
    timeZone:Joi.number().integer(),
    timeDuration:Joi.string(),
    title:Joi.string(),
  }).required(),
})

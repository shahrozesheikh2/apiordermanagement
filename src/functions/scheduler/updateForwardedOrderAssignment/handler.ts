import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { Op } from 'sequelize';
import { sequelize } from 'src/config/database';
import { order, orderAssignment, orderAssignmentI, orderStatus, orderStatusI, schAvalabilityTechnicians, schReccurenceDateList, statusActivity, statusActivityI, statusNotifications } from 'src/models';
// import { ANY } from 'src/shared/common'; 
import schema, { updateForwardedOrderAssignmentSchema }  from './schema';



const updateForwardedOrderAssignment:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    const transaction = await sequelize.transaction();

    try{
      var lang = event.headers['Accept-Language'];
      var message = 'Successfully Created'
      var errormessage = 'Order not Assign On Previous Date'
    
      if(lang =='de')
      {
         message = 'Erfolgreich erstellen'
         errormessage = 'Order not Assign On Previous Date'
      }
      const data  = await updateForwardedOrderAssignmentSchema.validateAsync(event.body)
  
      const {
        userId,
        orderFdPartner
      } = data
  
      let {
        id,
        // comments,
        // orderId,
        // timeZone,
        orderId,
        fdPartnerId,
        startDateTime,
        endDateTime,
        title,
        timeDuration
      } = orderFdPartner

    const statusType: orderStatusI = await orderStatus.findOne(
      {
        where:{
          key:'assigned',
          deletedAt:null,
        }
    });
  
    console.log("statusType",statusType)

    const orderDetails = await order.findOne(
      {
        where:{
          id:orderId,
          deletedAt:null,
        }
    });

    if(orderDetails.statusId !=statusType.id){

      return formatJSONResponse({
        statusCode:403,
        message: 'scheduled Order Not Update'
      });

    }

  
      const startDate: Date = new Date(startDateTime);
      const endDate: Date = new Date(endDateTime);
      
     console.log(id)
  
      const orderAssignmentData: orderAssignmentI = await orderAssignment.findOne(
        {
          where:{
            id,
            deletedAt:null,
          }
      });
      console.log("orderAssignmentData",orderAssignmentData)
      if(!orderAssignmentData){
        await sequelize.connectionManager.close();
        message = 'No order Assign Against this Id'
        if(lang =='de')
        {
           message = 'Kein Auftrag gegen diese ID zuweise'
        }
        return formatJSONResponse({
          statusCode:403,
          message: message
        });
      }
  
      if (new Date(startDate).getTime() < new Date(new Date().setUTCHours(0, 0, 0, 0)).getTime()) {
        errormessage = 'Order not Assign On Previous Date'
        if(lang =='de')
        {
           errormessage = 'Der Auftrag ist keiner Uhrzeit zugeordnet'
        }
        throw Error (errormessage);
        // throw Error (' Order not Assign On Previous Date');
      }
      
      // const checkDuplicateOrderAssign: orderAssignmentI = await orderAssignment.findOne(
      //   {
      //     where:{
      //       orderId,
      //       deletedAt:null,
      //     }
      // });
  
      //  if (checkDuplicateOrderAssign && Object.keys(checkDuplicateOrderAssign).length) {
      //    throw Error ('Order Already Assign To Technicians');
      //   }
  
      let getAvailableTechnician = await schAvalabilityTechnicians.findOne({
        where:{
          deletedAt:null,
          avlForId:fdPartnerId,
          // startDate:{[Op.lte]:startDateTime},
          // endDate:{[Op.gte]:endDateTime}
        },
        include:{
          as: 'dateList',
          model:schReccurenceDateList,
          required:true,
          where:{
            startDate:{[Op.lte]:startDateTime},
            endDate:{[Op.gte]:endDateTime}
          }
          
        }
  
      })
    getAvailableTechnician = JSON.parse(JSON.stringify(getAvailableTechnician))
    console.log("getAvailableTechnician",getAvailableTechnician);
    
    if(!getAvailableTechnician || !Object.keys(getAvailableTechnician).length){
      errormessage = 'Technicians Not Available for given Time'
      if(lang =='de')
      {
         errormessage = 'Techniker für bestimmte Zeit nicht verfügbar'
      }
      throw Error(errormessage)
    }
   
    
  
   
    const techAlreadyAssign = await orderAssignment.findAll(
      {
        where:{
          id:{[Op.ne]:id},
          deletedAt:null,
          availableTechnicianId:getAvailableTechnician.id,
          [Op.or]: [
            {
              endDateTime: {
                    [Op.and]: [
                        {
                            [Op.gt]: startDate
                        },
                        {
                            [Op.lte]: endDate
                        }
                    ]
                }
            },
            {
              startDateTime: { [Op.between]: [startDate, endDate] }
            }]
        }
    });
    console.log("techAlreadyAssign",techAlreadyAssign)
    
    
    if(techAlreadyAssign.length != 0){
      errormessage = 'Technicians Already Assign to someOne'
      if(lang =='de')
      {
         errormessage = 'Techniker weisen bereits jemandem zu'
      }
      throw Error(errormessage)
    }
    // throw Error ('message')
    // const response :orderAssignmentI   = await orderAssignment.create({
    //       startDateTime,
    //       endDateTime,
    //       timeZone,
    //       availableTechnicianId:getAvailableTechnician.id,
    //       orderId,
    //       comments
    //   })
  
    // const statusType: orderStatusI = await orderStatus.findOne(
    //   {
    //     where:{
    //       key:'reschecduled',
    //       deletedAt:null,
    //     }
    // });
  
    // console.log("statusType",statusType)
  
        await orderAssignment.update({
          startDateTime:startDate,
          endDateTime:endDate,
          updatedAt: new Date(),
          updatedBy: userId,
          availableTechnicianId:getAvailableTechnician.id,
          title,
          timeDuration
        },
        {where:{id},transaction})
  
      //   await order.update({
      //     statusId:statusType.id
      // }, {
      //     where: {
      //       id:orderAssignmentData.orderId,
      //     },transaction
      // });
  
      // const statusObj = {
      //   statusId:statusType.id,
      //   orderId:orderAssignmentData.orderId,
      //   userId,
      //   activityDateTime:new Date(),
      // }
      // console.log("statusObj",statusObj)
  
      // const statusResponse: statusActivityI = await statusActivity.create(statusObj,{transaction})
      
      // console.log("statusResponse",statusResponse)
  
      // const statusActivityExist = await statusActivity.findAll({where:{orderId,deletedAt:null}})
  
      // const notiArray = statusActivityExist.map(s => ({
      //   userId:s.userId,
      //   statusActivityId:statusResponse.id
      // }));
  
      // const notificationsResponse = await statusNotifications.bulkCreate(notiArray,{transaction})

      await transaction.commit()
  
      // console.log("notificationsResponse",notificationsResponse)
  
      const updateOrderAssignment = await orderAssignment.findOne({
        where:{id,deletedAt:null},
        include:{
          as:'availableTechnicians',
          include: [{
            as: 'dateList',
            model: schReccurenceDateList,
            where: {
              deletedAt: null
            }
          }],
          model:schAvalabilityTechnicians,
          where:{
            deletedAt:null
          }
        }
      })
      await sequelize.connectionManager.close();
  
       return formatJSONResponse({
         statusCode:200,
         message: message,
         //  message: `Successfully Create`,
         updateOrderAssignment
       });
       
     } catch(error) {
      if (transaction) {transaction.rollback(); }
       await sequelize.connectionManager.close();
       return formatJSONResponse({
        statusCode:403,
        message: error.message
      });
       
     }

  }catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode:403,
      message: error.message
    });
 }
}
 export const main = middyfy(updateForwardedOrderAssignment);

import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { Op } from 'sequelize';
import { sequelize } from 'src/config/database';
import { bookingType, customer, order, orderAssignment, orderAssignmentI, orderStatus, orderStatusI, schAvalabilityTechnicians, schReccurenceDateList, statusActivity, statusActivityI, statusNotifications, subBookingType, users } from 'src/models';
// import { ANY } from 'src/shared/common'; 
import schema, { orderAssignSchema }  from './schema';

import fetch from 'node-fetch'



const assignOrderToTechnician:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    const transaction = await sequelize.transaction();

    try{
      const data  = await orderAssignSchema.validateAsync(event.body)

      var lang = event.headers['accept-language']

      const {
        userId,
        orderAssign
      } = data
  
      let {
        comments,
        technicianId,
        orderId,
        startDateTime,
        endDateTime,
        timeZone,
        timeDuration,
        partnerId

      } = orderAssign
  
      const startDate: Date = new Date(startDateTime);
      const endDate: Date = new Date(endDateTime);

      console.log("startDate",startDate)
      
      console.log("endDate",endDate)
      
      if (new Date(startDate).getTime() < new Date(new Date().setUTCHours(0, 0, 0, 0)).getTime()) {
        if(lang == 'de')
        throw Error ('Der Auftrag ist keiner Uhrzeit zugeordnet');
        throw Error (' Order not Assign On Previous Date');
      }
      
      if(!partnerId){
      const checkDuplicateOrderAssign: orderAssignmentI = await orderAssignment.findOne(
        {
          where:{
            orderId,
            deletedAt:null,
          }
      });
    
  
       if (checkDuplicateOrderAssign && Object.keys(checkDuplicateOrderAssign).length) {
        if(lang == 'de')
         throw Error ('Bestellung bereits Technikern zuweisen');
         throw Error ('Order Already Assign To Technicians');
        }
      }
  
    let getAvailableTechnician = await schAvalabilityTechnicians.findOne({
        where:{
          deletedAt:null,
          technicianId,
          // startDate:{[Op.lte]:startDateTime},
          // endDate:{[Op.gte]:endDateTime}
        },
        include:{
          as: 'dateList',
          model:schReccurenceDateList,
          attributes:['id','startDate','endDate','availableTechnicianId'],
          required:true,
          where:{
            startDate:{[Op.lte]:startDateTime},
            endDate:{[Op.gte]:endDateTime}
          }
          
        }
      })
    
    if(!getAvailableTechnician || !Object.keys(getAvailableTechnician).length){

      if(lang == 'de'){
        throw Error("Techniker für bestimmte Zeit nicht verfügbar")
      }
      throw Error("Technicians Not Available for given Time")
    }
  
    getAvailableTechnician = JSON.parse(JSON.stringify(getAvailableTechnician))
  
    console.log("getAvailableTechnician",getAvailableTechnician)

   
    const techAlreadyAssign = await orderAssignment.findAll(
      {
        where:{
          deletedAt:null,
          availableTechnicianId:getAvailableTechnician.id,
          [Op.or]: [
            {
              endDateTime: {
                    [Op.and]: [
                        {
                            [Op.gt]: startDate
                        },
                        {
                            [Op.lte]: endDate
                        }
                    ]
                }
            },
            {
              startDateTime: { [Op.between]: [startDate, endDate] }
            }]
        }
    });
    console.log("techAlreadyAssign",techAlreadyAssign)
     

    
    if(techAlreadyAssign.length != 0){
      if(lang == 'de'){
        throw Error("Techniker sind bereits jemandem zugewiese")
      }
      throw Error("Technicians Already Assign to someone")
      
    }
    // const statusType :statusType = await orderStatus.findOne({where:{name:'schecduled'}})
  
    const statusType: orderStatusI = await orderStatus.findOne(
      {
        where:{
          key:'schecduled',
          deletedAt:null,
        }
    });

    let response:orderAssignmentI;

    if(partnerId){

      await orderAssignment.update({
        availableTechnicianId:getAvailableTechnician.id,
      },{where:{orderId},transaction})
    }else{

       response = await orderAssignment.create({
        startDateTime,
        endDateTime,
        timeZone,
        availableTechnicianId:getAvailableTechnician.id,
        dateListId:getAvailableTechnician?.dateList[0]?.id,
        orderId,
        comments,
        timeDuration,
        // statusId:statusType.id
    },{ transaction })

    }
  
   

      await order.update({
        statusId:statusType.id,
        technicianId
    }, {
        where: {
            id:orderId,
        },transaction
    });

    const statusObj = {
      statusId:statusType.id,
      orderId,
      userId,
      activityDateTime:new Date(),
    }
    console.log("statusObj",statusObj)

    const statusResponse: statusActivityI = await statusActivity.create(statusObj,{transaction})
    
    console.log("statusResponse",statusResponse)

    const statusActivityExist = await statusActivity.findAll({where:{orderId,deletedAt:null}})

    const notiArray = statusActivityExist.map(s => ({
      userId:s.userId,
      statusActivityId:statusResponse.id
    }));

    const notificationsResponse = await statusNotifications.bulkCreate(notiArray,{transaction})

    console.log("notificationsResponse",notificationsResponse)

    const orderDetails = await order.findOne({where:{id:orderId},
      include:
        {
          as: 'subBookingType',
          model: subBookingType,
          attributes: ['name'],
          // include: [{
          //   as: 'bookingType',
          //   model: bookingType,
          //   attributes: ['typeName'],
          // }]
        }
      
      })

    const customerData = await customer.findOne({where:{id:orderDetails.customerId}})

    const userData = await users.findOne({where:{id:orderDetails.technicianId}})


        var params = {
          subject: 'Order Scheduled',
          body: `Your order #${orderDetails.id} was successfully scheduled with a technician. 
          scheduled on ${response.startDateTime} 
          Booking Type : ${orderDetails.subBookingType.name}`,
          recipient: customerData.email
        }


        var paramsTech = {
          subject: 'Order Scheduled',
          body: `Your order #${orderDetails.id} was successfully scheduled with you. 
          scheduled on ${response.startDateTime} 
          Booking Type : ${orderDetails.subBookingType.name}`,
          recipient: userData.email
        }

        const emailServiceResponse = await fetch(`${process.env.SEND_EMAIL_SES}/sendMail`, {
          method: 'POST',
          body: JSON.stringify(params),
          headers: { 'Content-Type': 'application/json' }
        })
        .then(res => res.json())
        .then(json => json)
        .catch(err => err);


        const emailServiceResponseTech = await fetch(`${process.env.SEND_EMAIL_SES}/sendMail`, {
          method: 'POST',
          body: JSON.stringify(paramsTech),
          headers: { 'Content-Type': 'application/json' }
        })
        .then(res => res.json())
        .then(json => json)
        .catch(err => err);




    
    
    await transaction.commit()

    response = await orderAssignment.findOne({
      where:{
        availableTechnicianId:getAvailableTechnician.id,
        orderId
      }
    })

    await sequelize.connectionManager.close();

       return formatJSONResponse({
         statusCode:200,
         message: `Erfolgreich erstellen`,
        //  message: `Successfully Create`,
         response,
         statusResponse,
         notificationsResponse
       });

    }catch(error) {
      if (transaction) {transaction.rollback(); }
       await sequelize.connectionManager.close();
        return formatJSONResponse({
        statusCode:403,
        message: error.message
        });   
    }     
   } catch(error) {
    await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode:403,
      message: error.message
    });   
   }

 }

 export const main = middyfy(assignOrderToTechnician);

import * as Joi from "joi";

export default {
  type: "object",
  properties: {
   
  },
} as const;

export const orderAssignSchema =  Joi.object({
  userId: Joi.number().integer().required(),
  orderAssign:Joi.object({
    comments:Joi.string(),
    technicianId:Joi.number().integer().required(),
    orderId:Joi.number().integer().required(),
    startDateTime:Joi.string().required(),
    endDateTime:Joi.string().required(),
    timeZone:Joi.number().integer(),
    timeDuration:Joi.string(),
    title:Joi.string(),
    partnerId:Joi.number().integer() // partner --> company only forwarded order
  }).required(),
 
})

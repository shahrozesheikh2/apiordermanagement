import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { Op } from 'sequelize';
import { sequelize } from 'src/config/database';
import { order, orderAssignment, schAvalabilityTechnicians, schAvalabilityTechniciansI, schReccurenceDateList } from 'src/models';
// import { ANY } from 'src/shared/common'; 
import schema, { getTechniciansOrderSchema }  from './schema';



const getTechnicianAssignOrders:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    const data  = await getTechniciansOrderSchema.validateAsync(event.body)

    const {
      technicianIds,
      startDateTime,  
      endDateTime,
      // userId,

    } = data


    const startDate: Date = new Date(startDateTime);
    const endDate: Date = new Date(endDateTime);
   

    let techniciansAvailability = await schAvalabilityTechnicians.findAll({
      where:{
        deletedAt:null,
        technicianId: { [Op.in]: technicianIds },
      },
      include: {
        as: 'dateList',
        model: schReccurenceDateList,
        where: {
          deletedAt: null,
          [Op.or]: [
            {
              endDate: {
                [Op.and]: [
                  {
                    [Op.gt]: startDate
                  },
                  {
                    [Op.lte]: endDate
                  }
                ]
              }
            },
            {
              startDate: { [Op.between]: [startDate, endDate] }
            }
          ]
        }
      }
    })
  
  if(!techniciansAvailability || !Object.keys(techniciansAvailability).length){
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode:403,
      message: `Technicians Not Available`,
    });
  }

  // techniciansAvailability = JSON.parse(JSON.stringify(techniciansAvailability))

  const availableTechnicianIds: number[] = techniciansAvailability.map((o: schAvalabilityTechniciansI): number => o.id);

  // console.log("availableTechnicianIds",availableTechnicianIds);

  const orderAssignments = await orderAssignment.findAll({
    where:{
      deletedAt:null,
      availableTechnicianId: { [Op.in]: availableTechnicianIds },
    },
    include:[{
      as:'availableTechnicians',
      model: schAvalabilityTechnicians,
      required: false,
      where: {
        deletedAt: null,
      }
    },{
      as:'order',
      model: order,
      required: false,
      where: {
        deletedAt: null,
      }
    }]
  })
  
  await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

       orderAssignments
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }

 export const main = middyfy(getTechnicianAssignOrders);


  
import * as Joi from "joi";

export default {
  type: "object",
  properties: {
   
  },
} as const;

export const getTechniciansOrderSchema =  Joi.object({
  userId: Joi.number().integer(),
  startDateTime:Joi.string(),
  endDateTime:Joi.string(),
  technicianIds:Joi.array().items(Joi.number()).required(), 
})

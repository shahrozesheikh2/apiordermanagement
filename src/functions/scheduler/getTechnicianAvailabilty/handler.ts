import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { Op } from 'sequelize';
import { sequelize } from 'src/config/database';
import { orderAssignment, schAvalabilityTechnicians, schReccurenceDateList } from 'src/models';
// import { ANY } from 'src/shared/common'; 
import schema, { getOrderAvailabilitySchema }  from './schema';



const getTechnicianAvailability:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    const data  = await getOrderAvailabilitySchema.validateAsync(event.body)

    console.log(data)

    const {
      technicianIds,
      startDateTime,
      endDateTime,
      // userId,

    } = data


    const startDate: Date = new Date(startDateTime);
    const endDate: Date = new Date(endDateTime);
   

    let techniciansAvailability = await schAvalabilityTechnicians.findAll({
      where:{
        deletedAt:null,
        technicianId: { [Op.in]: technicianIds },
      },
      order: [['startDate', 'ASC']],
      include:[ 
      {
        as: 'dateList',
        model: schReccurenceDateList,
        where: {
          deletedAt: null,
          [Op.or]: [
            {
              endDate: {
                [Op.and]: [
                  {
                    [Op.gt]: startDate
                  },
                  {
                    [Op.lte]: endDate
                  }
                ]
              }
            },
            {
              startDate: { [Op.between]: [startDate, endDate] }
            }
          ]
        },
        include:[{
          as: 'orderAssignment',
          model: orderAssignment,
          required:false,
          where:{
          deletedAt:null
         }
        }]
      }
      // {
      //   as: 'orderAssignment',
      //   model: orderAssignment,
      //   required:false,
      //   where:{
      //     deletedAt:null
      //   }
      // }
    ]
    })
  
  // if(!techniciansAvailability || !Object.keys(techniciansAvailability).length){
  //   throw Error("Technicians Not Available for given Time")
  //   return formatJSONResponse({
  //     message: `Technicians Not Available`,
  //   });
  // }

  // techniciansAvailability = JSON.parse(JSON.stringify(techniciansAvailability))

  await sequelize.connectionManager.close();
  
     return formatJSONResponse({
       statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

       techniciansAvailability
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }

 export const main = middyfy(getTechnicianAvailability);


  
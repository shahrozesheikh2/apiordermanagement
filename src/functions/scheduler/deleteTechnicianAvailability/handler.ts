import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { orderAssignment, schAvalabilityTechnicians, schReccurenceDateList, schRecurrenceDayList } from '../../../models/index';
import schema, { deleteTechnicianAvailabilitySchema }  from './schema';

const deleteTechnicianAvailability:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
          const transaction = await sequelize.transaction();
          sequelize.authenticate()
          try{
            const availabilityData = await deleteTechnicianAvailabilitySchema.validateAsync(event.body)

            const{
              id:availableTechnicianId,
              dateListId
              // userId
            } = availabilityData

            var lang = event.headers['accept-language']

            const avilableDateLists = await schReccurenceDateList.findAll({
              where:{
                availableTechnicianId,
                deletedAt:null
              },
              include:{
                as:'orderAssignment',
                model:orderAssignment,
                required:false,
              where:{
                deletedAt:null
              }
              }

            })


            const orderExist = await orderAssignment.findOne({
              where: {
                dateListId,
                deletedAt:null
              }
            })
  

          if(orderExist){
            if(lang =='de'){
              throw new Error('Verfügbarkeit kann nicht gelöscht werden. Erste Bestellung löschen')
            }
            throw new Error('Availability cannot Delete First Delete Assign Order')
          }


            await schReccurenceDateList.update({  deletedAt:new Date },{where:{ id:dateListId },transaction })

            if(avilableDateLists.length === 1){
              await schAvalabilityTechnicians.update({  deletedAt:new Date },{where:{ id:availableTechnicianId },transaction })

              await schRecurrenceDayList.update({  deletedAt:new Date },{where:{ availableTechnicianId },transaction })

            }

  
            // const exist = await schAvalabilityTechnicians.findOne({
            //   where: {
            //     id:availableTechnicianId,
            //      deletedAt:null
            //   },
            //   include:[ {
            //     as: 'dateList',
            //     model: schReccurenceDateList,
            //     where: {
            //       deletedAt: null,
            //       // [Op.or]: [
            //       //   {
            //       //     endDate: {
            //       //       [Op.and]: [
            //       //         {
            //       //           [Op.gt]: techStartDate
            //       //         },
            //       //         {
            //       //           [Op.lte]: techEndDate
            //       //         }
            //       //       ]
            //       //     }
            //       //   },
            //       //   {
            //       //     startDate: { [Op.between]: [techStartDate, techEndDate] }
            //       //   }
            //       // ]
            //     }
            //   }]
            // })
            // if(!exist){
            //   throw new Error('Invalid Id ')
            // }

          //   console.log("exist",JSON.parse(JSON.stringify(exist)))

          //   throw Error
  
          //   const orderExist = await orderAssignment.findOne({
          //     where: {
          //       availableTechnicianId,
          //        deletedAt:null
          //     }
          //   })
  

          // if(orderExist){
          //   if(lang =='de'){
          //     throw new Error('Verfügbarkeit kann nicht gelöscht werden. Erste Bestellung löschen')
          //   }
          //   throw new Error('Availability cannot Delete First Delete Assign Order')
          // }
  
          


          await transaction.commit()

          await sequelize.connectionManager.close();
          
          return formatJSONResponse({
              statusCode: 200,
              message: `Successfully deteled`,
              });

          }catch(error) {
            if (transaction) {transaction.rollback(); }
                await sequelize.connectionManager.close();
                return formatJSONResponse({
                    statusCode: 403,
                    message: error.message
          });
     
   }
   } catch(error) {
    await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(deleteTechnicianAvailability);
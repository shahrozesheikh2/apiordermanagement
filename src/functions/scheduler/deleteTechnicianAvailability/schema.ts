import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;


export const deleteTechnicianAvailabilitySchema =  Joi.object({
    id: Joi.number().integer().required(),
    userId:Joi.number().integer(),
    dateListId:Joi.number().integer().required()
})



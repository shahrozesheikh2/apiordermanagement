import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { orderAssignment, schAvalabilityTechnicians, schReccurenceDateList, users } from 'src/models';
import { ANY } from 'src/shared/common';
// import { ANY } from 'src/shared/common'; 
import schema, { getForwardedOrderByOrderIdSchema }  from './schema';



const getForwardedOrderByOrderId:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    var lang = event.headers['accept-language']

    var  message =`Successfully Create`

    if(lang =='de')
    {
      message =  `Erfolgreich erstellen`
    }

    const orderData  = await getForwardedOrderByOrderIdSchema.validateAsync(event.body)

    const{
      orderId
    } = orderData


    let assignOrder : ANY = await orderAssignment.findOne({
      where: {
          orderId,
          deletedAt:null
      },
      include: [{
        as: 'availableTechnicians',
        model: schAvalabilityTechnicians,
        include:[{
          as: 'dateList',
          model: schReccurenceDateList,
          where: {
            deletedAt: null,
          }
        },
        {
          as: 'users',
          model: users,
          attributes: ['id','firstName','lastName'],
          where: {
            deletedAt: null,
          }
        }],
        where: {
          deletedAt: null,
        }
      }],
      attributes: {exclude: ['updatedAt','updatedBy','deletedBy']}
  });        
   
  
  await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode: 200,
       message: message,//`Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,
      assignOrder
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }

 export const main = middyfy(getForwardedOrderByOrderId);


  
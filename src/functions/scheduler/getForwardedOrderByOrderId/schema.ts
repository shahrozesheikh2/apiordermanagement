import * as Joi from "joi";

export default {
  type: "object",
  properties: {
   
  },
} as const;

export const getForwardedOrderByOrderIdSchema =  Joi.object({
  orderId: Joi.number().integer() 
})

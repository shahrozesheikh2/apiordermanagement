import * as Joi from "joi";

export default {
  type: "object",
  properties: {
   
  },
} as const;

export const updateOrderAssignSchema =  Joi.object({
  userId: Joi.number().integer().required(),
  orderAssign:Joi.object({
    id:Joi.number().required(),
    technicianId:Joi.number().integer(),
    orderId:Joi.number().integer(),
    startDateTime:Joi.string(),
    endDateTime:Joi.string(),
    timeZone:Joi.number().integer(),
    comments:Joi.string(),
    timeDuration:Joi.string(),
    statusReason:Joi.string(),
    statusId:Joi.number().integer(),
    title:Joi.string(),
  }).required(),
 
})

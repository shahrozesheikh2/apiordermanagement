import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { orderAssignment, orderStatus, orderStatusI } from '../../../models/index';
import schema, { cancelOrderSchema }  from './schema';

const cancelOrderAssignment:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
          sequelize.authenticate()
          
          const orderData = await cancelOrderSchema.validateAsync(event.body)

          const{
            id,
            // userId
          } = orderData

          const statusType: orderStatusI = await orderStatus.findOne(
            {
              where:{
                name:'Inprogress',
                deletedAt:null,
              }
          });
        

          const exist = await orderAssignment.findOne({
            where: {
               id,
               statusId:statusType.id,
               deletedAt:null
            }
          })

        if(exist){
          throw new Error('In Progress Order cannot Delete')
        }

        const cancelStatus: orderStatusI = await orderStatus.findOne(
          {
            where:{
              key:'cancelled',
              deletedAt:null,
            }
        });
        const data = await orderAssignment.update({ statusId:cancelStatus.id ,deletedAt:new Date },{where:{ id } })
          await sequelize.connectionManager.close();
          
          return formatJSONResponse({
            statusCode: 200,
            message: `Successfully deteled`,
            data
            });
   
   } catch(error) {
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(cancelOrderAssignment);
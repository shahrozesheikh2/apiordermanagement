import * as Joi from "joi";

export default {
  type: "object",
  properties: {
   
  },
} as const;

export const orderFdToPartnerSchema =  Joi.object({
  userId: Joi.number().integer().required(),
  orderFdPartner:Joi.object({
    comments:Joi.string(),
    fdPartnerId:Joi.number().integer().required(),//companyId
    orderId:Joi.number().integer().required(),
    startDateTime:Joi.string().required(),
    endDateTime:Joi.string().required(),
    timeZone:Joi.number().integer(),
    timeDuration:Joi.string(),
    title:Joi.string(),
  }).required(),
 
})

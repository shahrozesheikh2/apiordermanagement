import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { Op } from 'sequelize';
import { sequelize } from 'src/config/database';
import { order, orderAssignment, orderAssignmentI, orderStatus, orderStatusI, schAvalabilityTechnicians, schReccurenceDateList, statusActivity, statusActivityI, statusNotifications } from 'src/models';
// import { ANY } from 'src/shared/common'; 
import schema, { orderFdToPartnerSchema }  from './schema';



const orderFdToPartner:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    const transaction = await sequelize.transaction();

    try{
      const data  = await orderFdToPartnerSchema.validateAsync(event.body)

      const {
        userId,
        orderFdPartner
      } = data
  
      let {
        comments,
        fdPartnerId,
        orderId,
        startDateTime,
        endDateTime,
        timeZone,
        timeDuration

      } = orderFdPartner

      var lang = event.headers['accept-language']
      var messagePreviousDate = 'Order not Assign On Previous Date'
      var messageAlreadyAssign = 'Order Already Assign To Technicians'
      var messageTechNotAvl = 'Technicians Not Available for given Time'
      var messageTechAlreadyAssign = 'Technicians Already Assign to someone'
      var  message =`Successfully Create`



      if(lang =='de')
      {
        messagePreviousDate = 'Der Auftrag ist keiner Uhrzeit zugeordnet'
        messageAlreadyAssign = 'Bestellung bereits Technikern zuweisen'
        messageTechNotAvl = 'Techniker für bestimmte Zeit nicht verfügbar'
        messageTechAlreadyAssign = 'Techniker sind bereits jemandem zugewiese'
        message =  `Erfolgreich erstellen`
      }
  
      const startDate: Date = new Date(startDateTime);
      const endDate: Date = new Date(endDateTime);
      
      if (new Date(startDate).getTime() < new Date(new Date().setUTCHours(0, 0, 0, 0)).getTime()) {
        // throw Error ('Der Auftrag ist keiner Uhrzeit zugeordnet');
        throw Error (messagePreviousDate);
      }
     
      const checkDuplicateOrderAssign: orderAssignmentI = await orderAssignment.findOne(
        {
          where:{
            orderId,
            deletedAt:null,
          }
      });
  
       if (checkDuplicateOrderAssign && Object.keys(checkDuplicateOrderAssign).length) {
        //  throw Error ('Bestellung bereits Technikern zuweisen');
         throw Error (messageAlreadyAssign);
        // Bestellung bereits Technikern zuweisen

        }
  
      let getAvailableTechnician = await schAvalabilityTechnicians.findOne({
        where:{
          deletedAt:null,
          avlForId:fdPartnerId
          // startDate:{[Op.lte]:startDateTime},
          // endDate:{[Op.gte]:endDateTime}
        },
        include:{
          as: 'dateList',
          model:schReccurenceDateList,
          required:true,
          where:{
            startDate:{[Op.lte]:startDateTime},
            endDate:{[Op.gte]:endDateTime}
          }
          
        }
      })
    console.log("getAvailableTechnician",getAvailableTechnician)
    if(!getAvailableTechnician || !Object.keys(getAvailableTechnician).length){
      // throw Error("")
      throw Error(messageTechNotAvl)
    }
  
    getAvailableTechnician = JSON.parse(JSON.stringify(getAvailableTechnician))
  
   
    // const techAlreadyAssign = await orderAssignment.findAll(
    //   {
    //     where:{
    //       deletedAt:null,
    //       availableTechnicianId:getAvailableTechnician.id,
    //       [Op.or]: [
    //         {
    //           endDateTime: {
    //                 [Op.and]: [
    //                     {
    //                         [Op.gt]: startDate
    //                     },
    //                     {
    //                         [Op.lte]: endDate
    //                     }
    //                 ]
    //             }
    //         },
    //         {
    //           startDateTime: { [Op.between]: [startDate, endDate] }
    //         }]
    //     }
    // });
    // console.log("getAvailableTechnician",techAlreadyAssign)
    
    
    // if(techAlreadyAssign.length != 0){
    //   throw Error(messageTechAlreadyAssign)
    //   // throw Error("Technicians Already Assign to someone")
      
    // }
    // const statusType :statusType = await orderStatus.findOne({where:{name:'schecduled'}})
  
    const statusType: orderStatusI = await orderStatus.findOne(
      {
        where:{
          key:'assigned',
          deletedAt:null,
        }
    });
  
    const response :orderAssignmentI   = await orderAssignment.create({
          startDateTime:startDate,
          endDateTime:endDate,
          timeZone,
          fdPartnerId,
          orderId,
          comments,
          timeDuration,
          // statusId:statusType.id
      },{ transaction })

    await order.update({
        statusId:statusType.id,
        fdPartnerId
    }, {
        where: {
            id:orderId,
        },transaction
    });

    const statusObj = {
      statusId:statusType.id,
      orderId,
      userId,
      activityDateTime:new Date(),
    }
    console.log("statusObj",statusObj)

    const statusResponse: statusActivityI = await statusActivity.create(statusObj,{transaction})
    
    console.log("statusResponse",statusResponse)

    const statusActivityExist = await statusActivity.findAll({where:{orderId,deletedAt:null}})

    const notiArray = statusActivityExist.map(s => ({
      userId:s.userId,
      statusActivityId:statusResponse.id
    }));

    const notificationsResponse = await statusNotifications.bulkCreate(notiArray,{transaction})

    console.log("notificationsResponse",notificationsResponse)
    
    await transaction.commit()
    await sequelize.connectionManager.close();

       return formatJSONResponse({
         statusCode:200,
         message: message,
        //  message: `Successfully Create`,
         response,
        //  statusResponse,
        //  notificationsResponse
       });

    }catch(error) {
      if (transaction) {transaction.rollback(); }
       await sequelize.connectionManager.close();
        return formatJSONResponse({
        statusCode:403,
        message: error.message
        });   
    }     
   } catch(error) {
    await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode:403,
      message: error.message
    });   
   }

 }

 export const main = middyfy(orderFdToPartner);

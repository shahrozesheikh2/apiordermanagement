import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;

export const addSignatureSchema =  Joi.object({
  technicianId: Joi.number().integer().required(),
  orderId: Joi.number().integer().required(),
    clientSignature: Joi.object({
      awsKey: Joi.string().required(),
      awsUrl: Joi.string().uri().required()
    }).max(2).required(),
    technicianSignature: Joi.object({
        awsKey: Joi.string().required(),
        awsUrl: Joi.string().uri().required()
      }).max(2).required()
})

import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { signature, signatureI } from 'src/models';
import schema, { addSignatureSchema }  from './schema';



const addSignature:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'Successfully Created'
    if(lang =='de')
    {
       message = 'erfolgreich hinzugefügt'
    }
    
    const data = await addSignatureSchema.validateAsync(event.body)

    const response: signatureI = await signature.create(data)
    await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
      message: message,
      //  message: `Successfully added`,
       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(addSignature);
import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { signature } from 'src/models';
import { ANY } from 'src/shared/common';

import schema, { getSignatureSchema } from './schema';


const getSignature:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const data : ANY   = await getSignatureSchema.validateAsync(event.body)

    const{
      orderId
    } = data

    const response : ANY = await signature.findOne({
      where: { 
        // createdBy:userId
        deletedAt:null,
        orderId
      },
      attributes: {exclude: ['updatedAt','updatedBy','deletedBy','createdBy','deletedAt']},
    })

  
      // if(!response){
      //   throw Error("Data not Exist")
      // }
      await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode:200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode:403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getSignature);
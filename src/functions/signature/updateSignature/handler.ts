import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { signature, signatureI } from 'src/models';
import schema, { updateSignatureSchema }  from './schema';



const updateSignature:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'Updated Successfully'
    if(lang =='de')
    {
       message = 'Erfolgreich geupdated'
    }
    const data = await updateSignatureSchema.validateAsync(event.body)

    const {
        technicianId,
        orderId,
      ...other
    } = data

    const Exists = await signature.findOne({where:{technicianId, orderId}, attributes: {exclude: ['updatedAt','updatedBy','deletedBy', 'createdBy']}});

    // console.log("Exists",Exists)

    if(!Exists){
      await sequelize.connectionManager.close();

      return formatJSONResponse({
         statusCode: 403,
         message: `no data against order`,
       });
    } 

   await signature.update({
        ...other
        }, {
            where: {
                technicianId:technicianId,
                orderId:orderId
            }
        });
    const response : signatureI = await signature.findOne({
          where: {
            technicianId,
            orderId
          }
      });
      await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
       message: message,
       response
     });
     
   } catch(error) {
     console.error(error);
     await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(updateSignature);
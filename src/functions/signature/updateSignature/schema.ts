import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;

export const updateSignatureSchema =  Joi.object({
  technicianId: Joi.number().integer().required(),
  orderId: Joi.number().integer().required(),
  clientSignature: Joi.object({
    awsKey: Joi.string(),
    awsUrl: Joi.string().uri()
  }).max(2),
  technicianSignature: Joi.object({
      awsKey: Joi.string(),
      awsUrl: Joi.string().uri()
    }).max(2)
})


import * as Joi from "joi";

export default {
  type: "object",
  properties: {

  },
} as const;


export const updateOrderSchema =  Joi.object({
  orderDetails:Joi.object({
    subBookingTypeId: Joi.number().integer(),
    serviceProvideId: Joi.number().integer(),
    customerId: Joi.number().integer(),
    partialFilled:Joi.boolean(),
    customTag:Joi.string(),
    statusId:Joi.number().integer(),
    archived:Joi.boolean(),
    onHold:Joi.boolean(),
    onHoldReason:Joi.when('onHold',{is: true, then: Joi.string().required()}),
    prepared:Joi.boolean()
  }).required(),
  userId:Joi.number().integer().required(),
  id:Joi.number().integer().required()


})
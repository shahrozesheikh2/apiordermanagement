import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { order, orderI, statusActivity, statusActivityI, statusNotifications } from '../../models/index';
import schema, { updateOrderSchema } from './schema';


const updateOrder:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
     
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
      sequelize.authenticate()
      var lang = event.headers['accept-language']
      var message = 'Order Updated Successfully'
      if(lang =='de')
      {
         message = 'Erfolgreich geupdated'
      }
      const transaction = await sequelize.transaction();
      try{

        const Data  = await updateOrderSchema.validateAsync(event.body)

        const {
          orderDetails,
          userId,
          id
        } = Data


        const {
          onHold
        } = orderDetails

        const Exists = await order.findOne({where:{id}});

        console.log("Exists",Exists)

        if(!Exists){
          await sequelize.connectionManager.close();

          return formatJSONResponse({
            statusCode: 403,
            message: `Id Invalid`,
          });
        } 

        const obj = {...orderDetails , updatedBy:userId}
        
        await order.update({
                ...obj
            }, 
            {
              where: {
                    id:id, 
                },transaction
            });

            if(orderDetails.statusId){

              const statusObj = {
                statusId:orderDetails.statusId,
                orderId:id,
                userId,
                activityDateTime:new Date(),
              }
      
              const statusResponse: statusActivityI = await statusActivity.create(statusObj,{transaction})
              
              const statusActivityExist = await statusActivity.findAll({where:{orderId:id,deletedAt:null}})

              const notiArray = statusActivityExist.map(s => ({
                    userId:s.userId,
                    statusActivityId:statusResponse.id
              }))?.filter(t =>{
                if(userId != t.userId ){
                  return t
                }
              });

              let newArr = notiArray.filter((value, index, self) =>
              index === self.findIndex((t) => (
                t.userId === value.userId 
              ))
            )

              await statusNotifications.bulkCreate(newArr,{transaction})
            }
        
          if(onHold == true){
            
          }
    
        await transaction.commit()
            
        const response : orderI = await order.findOne({
              where: {
                id
              }
          });
        await sequelize.connectionManager.close();

        return formatJSONResponse({
          statusCode: 200,
          message: message,
          //  message: `Successfully Updated`,
          response
        });
        
      } catch(error) {
        if (transaction) { transaction.rollback(); }
        console.error(error);
        await sequelize.connectionManager.close();

        return formatJSONResponse({
          statusCode: 403,
          message: error.message
        });
        
      }
  }catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
    
  }  
 }
 export const main = middyfy(updateOrder);
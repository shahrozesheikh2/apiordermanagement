import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;


export const getTechTimingsSchema =  Joi.object({
  userId: Joi.number().integer().required(),
  technicianId:Joi.number().integer().required()
})

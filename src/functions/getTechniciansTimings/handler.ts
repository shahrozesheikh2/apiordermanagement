import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { techniciansTimings } from '../../models/index';
import schema, { getTechTimingsSchema } from './schema';
import * as typings from '../../shared/common';


const getTechniciansTimings:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const data : typings.ANY   = await getTechTimingsSchema.validateAsync(event.body)

    const{
      technicianId
    } = data

    const response : typings.ANY = await techniciansTimings.findAll({
      where: { 
        // createdBy:userId
        deletedAt:null,
        technicianId
      }
    })

  
      if(!response){
        throw Error("Data not Exist")
      }
      await sequelize.connectionManager.close();
     return formatJSONResponse({
       statusCode:200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

       response
     });
     
   } catch(error) {
     console.error(error);
      await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode:403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getTechniciansTimings);
import { handlerPath } from '@libs/handler-resolver';
// import { orderSchema } from 'src/shared/common/validators/order.validator';
import schema from './schema';


// import { orderSchema } from 'src/shared/common/validators/order.validator';

export default {
  handler: `${handlerPath(__dirname)}/handler.main`,
  events: [
    {
      http: {
        method: 'post',
        path: 'getordersByCustomerId',
        // authorizer: 'arn:aws:lambda:eu-central-1:576223876742:function:auth-service-dev-auth',
        request: {
          schemas: {
            'application/json': schema,
          },
        },
        cors: true
      },
    },
  ],
};

import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { order,  orderStatus, subBookingType, bookingType, customer, product, productType } from '../../models/index';
// import { productType, productBrand, product, orderErrorDetails, recomendations, orderAssignment, } from '../../models/index';
import schema, { getAllOrderSchema } from './schema';
import * as typings from '../../shared/common';
import { Op } from 'sequelize';



const getordersByCustomerId:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const data :typings.ANY   = await getAllOrderSchema.validateAsync(event.body)
    const{
      customerId,
      limit,
      offset,
      filters
    } = data

    // let whereClause ;

    // if(bookingTypeIds && bookingTypeIds.length){
    //   whereClause.bookingTypeId = {[Op.in]: bookingTypeIds}
    // }
    // console.log("whereClause",whereClause)

    let  whereClause: { [key: string]: typings.ANY }  = {}

    if (filters?.bookingTypeIds && filters?.bookingTypeIds.length) {
      whereClause.bookingTypeIds = {  [Op.in]: filters.bookingTypeIds };
    }
    if ((filters?.fromDate && filters?.fromDate.length) &(filters?.toDate && filters?.toDate.length)) {
      whereClause.createdAt = {   [Op.between]:[filters.fromDate,filters.toDate] };
    }

    if (filters?.statusId) {
      whereClause.statusId =  filters.statusId;
    }
    const response : typings.ANY = await order.findAndCountAll({
      where: { 
        customerId , 
        deletedAt:null,
        ...whereClause
        },
        order: [['createdAt', 'desc']],
        attributes: {exclude: ['updatedAt','updatedBy','deletedBy','createdBy','deletedAt']},
      include:[
        {
          as: 'subBookingType',
          model: subBookingType,
          attributes: ['id', 'name'],
          include: [{
            as: 'bookingType',
            model: bookingType,
            attributes: ['id', 'typeName'],
          }]
        },
        { 
          as: 'products',
          model: product,
          include:[{
            as:'productType',
            model: productType,
            attributes: ['productType'],
          },
          // {
          //   as:'brands',
          //   model: productBrand,
          //   attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
          // }
        ],
          attributes: ['productTypeId'],
        },
        // {
        //   as: 'errorDetails',
        //   model: orderErrorDetails,
        //   attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
        // },
        // {
        //   as: 'recomendations',
        //   model: recomendations,
        //   attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
        // },
        // {
        //   as:'orderAssignment',
        //   model:orderAssignment,
        //   attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
        // }, 
        {
          as: 'orderStatus',
          model: orderStatus,
          attributes: {exclude: ['updatedAt','updatedBy','deletedBy','createdBy','deletedAt']},
        },
        {
          as: 'customer',
          model: customer,
          attributes:  ['id', 'customerAddress', 'mobileNumberExtension', 'mobileNumber', 'notes'],
        },
      ],
      limit:limit,
      offset: offset * limit
    })

  
      if(!response){
        throw Error("Empty")
      }
     await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,

       response
     });
     
   } catch(error) {
     await sequelize.connectionManager.close();
     console.error(error);
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getordersByCustomerId);
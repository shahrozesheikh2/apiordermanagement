import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;


export const getAllOrderSchema =  Joi.object({
  customerId: Joi.number().integer().required(),
  offset: Joi.number().min(0).default(0),
  limit: Joi.number().max(999).default(10),
  filters:Joi.object({
    subBookingTypeIds:Joi.array().items(Joi.number()),
    fromDate:Joi.string(),
    toDate:Joi.string(),
    statusId:Joi.number().integer()
  })
  
})

import { techniciansTimingsI } from "src/models";

const convertDateToLocal = (date: Date, timeZone: number) => {
  return new Date(date.setMinutes(date.getMinutes() - timeZone));
}


export const checkTechniciansTimings = async (techTimings, startDate: string, endDate: string, withRecurrence?: boolean, endingCriteria?: string): Promise<techniciansTimingsI[]> => {

    if (!withRecurrence) {
      const filteredTimings = techTimings.filter(t => {
        const {
          dayId,
          startTime,
          endTime,
          timeZone
        } = t
  
        if (dayId == new Date(startDate).getDay()) {
  
          const techStartDate: Date = convertDateToLocal(new Date(`${startDate.slice(0, 10)}T${String(startTime)}.000Z`), timeZone);
          const techEndDate: Date = convertDateToLocal(new Date(`${endDate.slice(0, 10)}T${String(endTime)}.000Z`), timeZone);
  
          // console.log("techStartDate",techStartDate)
          // console.log("techEndDate",techEndDate)
  
          const startDateWithTimezone: Date = convertDateToLocal(new Date(startDate), timeZone);
          const endDateWithTimezone: Date = convertDateToLocal(new Date(endDate), timeZone);
  
          // console.log("startDateWithTimezone",startDateWithTimezone)
          // console.log("endDateWithTimezone",endDateWithTimezone)
  
          if (techStartDate.getTime() <= startDateWithTimezone.getTime() && startDateWithTimezone.getTime() <= techEndDate.getTime() && techStartDate.getTime() <= endDateWithTimezone.getTime() && endDateWithTimezone.getTime() <= techEndDate.getTime()) {
            console.log("done")
            return t;
          }
  
        }
  
  
      })
      return filteredTimings;
    }
    if (endingCriteria === 'daily') {
      return [...techTimings]
    }
  
  
  }
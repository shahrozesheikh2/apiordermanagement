import { AutoIncrement, Column, DataType, HasMany, Model, PrimaryKey, Table } from "sequelize-typescript";
import { orderAssignment, orderSpareParts } from "./index";

export interface orderSparePartsStatusI {
    id: number;
    name:string;
    key:string;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
}

@Table({
    modelName: 'orderSparePartsStatus',
    tableName: 'orderSparePartsStatus',
    timestamps: true
})

export class orderSparePartsStatus extends Model<orderSparePartsStatusI>{

    @HasMany((): typeof orderSpareParts => orderSpareParts )
    public orderSpareParts: typeof orderSpareParts;

    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id : number;

    @Column(DataType.STRING)
    public name: string;

    @Column(DataType.STRING)
    public key: string;

    @Column(DataType.DATE)
    public createdAt : Date;

    @Column(DataType.INTEGER)
    public createdBy : number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.INTEGER)
    public deletedBy : number;

    @Column(DataType.DATE)
    public updatedAt : Date;

    @Column(DataType.INTEGER)
    public updatedBy : number;


}
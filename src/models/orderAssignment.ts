import { number } from "joi";
import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, Model, PrimaryKey, Table } from "sequelize-typescript";
import { company, order, orderStatus, schAvalabilityTechnicians, schReccurenceDateList, users } from ".";

export interface orderAssignmentI {
    id: number;
    startDateTime:Date;
    endDateTime:Date;
    comments: String;
    orderId:number;
    statusId:number;
    availableTechnicianId:number;
    timeZone:number;
    title:string;
    timeDuration:string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    createdBy: number;
    updatedBy: number;
    deletedBy: number;
    fdPartnerId:number;
    dateListId : number;
}

@Table({
    modelName: 'orderAssignment',
    tableName: 'orderAssignment',
    timestamps: true
})

export class orderAssignment extends Model<orderAssignmentI>{

    @BelongsTo((): typeof order => order )
    public order: typeof order;

    @BelongsTo((): typeof orderStatus => orderStatus )
    public orderStatus: typeof orderStatus;

    @BelongsTo((): typeof schAvalabilityTechnicians => schAvalabilityTechnicians )
    public availableTechnicians: typeof schAvalabilityTechnicians;

    @BelongsTo((): typeof schReccurenceDateList => schReccurenceDateList )
    public schReccurenceDateList: typeof schReccurenceDateList;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;

    @Column(DataType.STRING)
    public title: string;

    @Column(DataType.DATE)
    public startDateTime: Date;

    @Column(DataType.DATE)
    public endDateTime: Date;
 
    @ForeignKey((): typeof order  => order )
    @Column(DataType.INTEGER)
    public orderId: number;

    @ForeignKey((): typeof schAvalabilityTechnicians  => schAvalabilityTechnicians )
    @Column(DataType.INTEGER)
    public availableTechnicianId: number;

    @ForeignKey((): typeof schReccurenceDateList  => schReccurenceDateList )
    @Column(DataType.INTEGER)
    public dateListId: number;
    
    @ForeignKey((): typeof orderStatus  => orderStatus )
    @Column(DataType.INTEGER)
    public statusId: number;

    @Column(DataType.INTEGER)
    public timeZone: number;

    @Column(DataType.STRING)
    public comments: string;

    @Column(DataType.STRING)
    public timeDuration: string;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

    @ForeignKey((): typeof company  => company )
    @Column(DataType.INTEGER)
    public fdPartnerId: number;

}

import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, HasMany, HasOne, Model, PrimaryKey, Table } from "sequelize-typescript";
import { orderAssignment, schReccurenceDateList, schRecurrenceDayList, users } from ".";

export interface schAvalabilityTechniciansI {
    id: number;
    startDate:Date;
    endDate:Date;
    avlForId:number;
    technicianId:number;
    timeZone:number;
    description: string;
    title:string;
    duration:string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    createdBy: number;
    updatedBy: number;
    deletedBy: number;
}

@Table({
    modelName: 'schAvalabilityTechnicians',
    tableName: 'schAvalabilityTechnicians',
    timestamps: true
})

export class schAvalabilityTechnicians extends Model<schAvalabilityTechniciansI>{

    @HasMany((): typeof schReccurenceDateList => schReccurenceDateList)
    public dateList: typeof schReccurenceDateList;

    @HasMany((): typeof schRecurrenceDayList => schRecurrenceDayList)
    public dayList: typeof schRecurrenceDayList;

    @HasMany((): typeof orderAssignment => orderAssignment)
    public orderAssignment: typeof orderAssignment;

    // @HasMany((): typeof orderAssignment => orderAssignment)
    // public orderAssignment: typeof orderAssignment;

    @BelongsTo((): typeof users => users)
    public users: typeof users;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;

    @Column(DataType.DATE)
    public startDate: Date;

    @Column(DataType.DATE)
    public endDate: Date;
 
    @Column(DataType.INTEGER)
    public avlForId: number;
    
    @ForeignKey((): typeof users   => users )
    @Column(DataType.INTEGER)
    public technicianId: number;

    @Column(DataType.INTEGER)
    public timeZone: number;

    @Column(DataType.STRING)
    public description: string;

    @Column(DataType.STRING)
    public title: string;

    @Column(DataType.STRING)
    public duration: string;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

}

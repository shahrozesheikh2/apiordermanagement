import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, HasMany, HasOne, Model, PrimaryKey, Table } from "sequelize-typescript";
import { bookingType, product, orderErrorDetails, recomendations, orderStatus, orderSpareParts, company } from ".";
import { orderAssignment ,subBookingType } from ".";
import { customer } from "./customer";
import { invoice } from "./invoice";
import { statusActivity } from "./statusActivity";

export interface orderI {
    id : number;
    fdPartnerId : number;
    customerId : number;
    bookingTypeId?: number;
    subBookingTypeId : number;
    productId:number;
    customTag:string;
    statusId:number;
    companyId:number;
    partialFilled:boolean
    fixFirstId: string;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
    orderNum: number;
    prepared:boolean;
    archived:boolean;
    onHold:boolean;
    technicianId:number
}

@Table({
    modelName: 'order',
    tableName: 'order',
    timestamps: true,
    
})

export class order extends Model<orderI>{

    @BelongsTo((): typeof company => company , 'fdPartnerId' )
    public partnerCompany: typeof company;

    @HasOne((): typeof  invoice => invoice )
    public invoice : typeof invoice ;

    @BelongsTo((): typeof subBookingType => subBookingType )
    public subBookingType: typeof subBookingType;

    @BelongsTo((): typeof  product => product )
    public products : typeof product ;

    @BelongsTo((): typeof  bookingType => bookingType )
    public bookingType : typeof bookingType ;

    @BelongsTo((): typeof  customer => customer )
    public customer : typeof customer ;

    @HasOne((): typeof  orderErrorDetails => orderErrorDetails )
    public errorDetails : typeof orderErrorDetails ;

    @HasOne((): typeof  recomendations => recomendations )
    public recomendations : typeof recomendations ;

    @HasOne((): typeof  orderAssignment => orderAssignment )
    public orderAssignment : typeof orderAssignment ;

    @BelongsTo((): typeof  orderStatus => orderStatus )
    public orderStatus : typeof orderStatus ;

    @HasMany((): typeof  orderSpareParts => orderSpareParts )
    public orderSpareParts : typeof orderSpareParts ;

    @HasMany((): typeof  statusActivity => statusActivity )
    public statusActivity : typeof statusActivity ;

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;

    @ForeignKey((): typeof company  => company )
    @Column(DataType.INTEGER)
    public fdPartnerId: number;

    // @ForeignKey((): typeof company  => company )
    @Column(DataType.INTEGER)
    public technicianId: number;

    @ForeignKey((): typeof customer  => customer )
    @Column(DataType.INTEGER)
    public customerId: number;

    @ForeignKey((): typeof bookingType  => bookingType )
    @Column(DataType.INTEGER)
    public bookingTypeId: number;

    @ForeignKey((): typeof subBookingType  => subBookingType )
    @Column(DataType.INTEGER)
    public subBookingTypeId: number;

    @ForeignKey((): typeof  product => product )
    @Column(DataType.INTEGER)
    public productId: number;

    @Column(DataType.TINYINT)
    public partialFilled:boolean

    @Column(DataType.TEXT)
    public customTag:string

    @Column(DataType.TEXT)
    public fixFirstId: string

    @Column(DataType.INTEGER)
    public companyId : number;

    @ForeignKey((): typeof orderStatus  => orderStatus )
    @Column(DataType.INTEGER)
    public statusId: number;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.INTEGER)
    public orderNum: number;

    @Column(DataType.TINYINT)
    public prepared:boolean

    @Column(DataType.TINYINT)
    public archived:boolean

    @Column(DataType.TINYINT)
    public onHold:boolean

}
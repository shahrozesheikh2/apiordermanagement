import { AutoIncrement, Column, DataType, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface techniciansTimingsI {
    id: number;
    startTime:Date;
    endTime:Date;
    breakStartTime:Date;
    breakEndTime:Date;
    dayId:number;
    technicianId:number;
    timeZone:number;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    createdBy: number;
    updatedBy: number;
    deletedBy: number;
}

@Table({
    modelName: 'techniciansTimings',
    tableName: 'techniciansTimings',
    timestamps: true
})

export class techniciansTimings extends Model<techniciansTimingsI>{
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;

    @Column(DataType.STRING)
    public startTime: Date;

    @Column(DataType.STRING)
    public endTime: Date;

    @Column(DataType.STRING)
    public breakStartTime: Date;

    @Column(DataType.STRING)
    public breakEndTime: Date;

    
    @Column(DataType.INTEGER)
    public dayId: number;

    @Column({ type :DataType.VIRTUAL , 
    get() {return getDays(this.getDataValue('dayId'));}})
    public day : string;
    
    @Column(DataType.INTEGER)
    public technicianId: number;

    @Column(DataType.INTEGER)
    public timeZone: number;


    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;


    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

}

const getDays = (day:any) => {
    if (day === 0) return "Sunday"
    if (day === 1) return "Monday"
    if (day === 2) return "Tuesday"
    if (day === 3) return "Wednesday"
    if (day === 4) return "Thursday"
    if (day === 5) return "Friday"
    if (day === 6) return "Saturday"
    return ""
  };
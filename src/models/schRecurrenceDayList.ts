import { AutoIncrement, Column, DataType, ForeignKey, HasMany, Model, PrimaryKey, Table } from "sequelize-typescript";
import { schAvalabilityTechnicians } from ".";

export interface schRecurrenceDayListI {
    id: number;
    availableTechnicianId:number;
    dayId:number
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
}

@Table({
    modelName: 'schRecurrenceDayList',
    tableName: 'schRecurrenceDayList',
    timestamps: true
})

export class schRecurrenceDayList extends Model<schRecurrenceDayListI>{
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id : number;

    @ForeignKey((): typeof schAvalabilityTechnicians => schAvalabilityTechnicians)
    @Column(DataType.INTEGER)
    public availableTechnicianId : number;

    @Column(DataType.INTEGER)
    public dayId : number;

    @Column({ type :DataType.VIRTUAL , 
    get() {return getDays(this.getDataValue('dayId'));}})
    public day : string

    @Column(DataType.DATE)
    public createdAt : Date;

    @Column(DataType.INTEGER)
    public createdBy : number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.INTEGER)
    public deletedBy : number;


    @Column(DataType.DATE)
    public updatedAt : Date;

    @Column(DataType.INTEGER)
    public updatedBy : number;

}
const getDays = (day:any) => {
    if (day === 0) return "Sunday"
    if (day === 1) return "Monday"
    if (day === 2) return "Tuesday"
    if (day === 3) return "Wednesday"
    if (day === 4) return "Thursday"
    if (day === 5) return "Friday"
    if (day === 6) return "Saturday"
    return ""
  };
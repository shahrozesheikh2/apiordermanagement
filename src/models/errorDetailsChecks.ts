import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey ,Model, PrimaryKey, Table } from "sequelize-typescript";
import { bookingType } from "./bookingType";

export interface errorDetailsChecksI {
    id: number;
    bookingTypeId: number;
    checksName:string;
    checksKey:string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    createdBy: number;
    updatedBy: number;
    deletedBy: number;  
}

@Table({
    modelName: 'errorDetailsChecks',
    tableName: 'errorDetailsChecks',
    timestamps: true
})

export class errorDetailsChecks extends Model<errorDetailsChecksI>{

    @BelongsTo((): typeof bookingType => bookingType )
    public bookingType: typeof bookingType;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;

    @ForeignKey((): typeof bookingType  => bookingType )
    @Column(DataType.INTEGER)
    public bookingTypeId: number;

    @Column(DataType.STRING)
    public checksName: String;

    @Column(DataType.STRING)
    public checksKey: String;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;


    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

}
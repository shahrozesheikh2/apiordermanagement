import { AutoIncrement, Column, DataType, Model, PrimaryKey, Table } from "sequelize-typescript";


export interface problemSolutionsI {
    id: number;
    problemTitle:string;
    problemDescription:string;
    solutionTitle:string;
    solutionDescription:string;
    orderId: number;
    technicianId: number;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
}

@Table({
    modelName: 'problemSolutions',
    tableName: 'problemSolutions',
    timestamps: true
})

export class problemSolutions extends Model<problemSolutionsI>{

    // @HasMany((): typeof subBookingType => subBookingType )
    // public subBookingType: typeof subBookingType;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id : number;

    @Column(DataType.STRING)
    public problemTitle : string;

    @Column(DataType.STRING)
    public problemDescription : string;

    @Column(DataType.STRING)
    public solutionTitle : string;

    @Column(DataType.STRING)
    public solutionDescription : string;

    
    @Column(DataType.INTEGER)
    public orderId : number;

    @Column(DataType.INTEGER)
    public technicianId : number;

    @Column(DataType.DATE)
    public createdAt : Date;

    @Column(DataType.INTEGER)
    public createdBy : number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.INTEGER)
    public deletedBy : number;


    @Column(DataType.DATE)
    public updatedAt : Date;

    @Column(DataType.INTEGER)
    public updatedBy : number;


}
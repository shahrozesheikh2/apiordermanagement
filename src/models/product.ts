import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, HasMany, Model, PrimaryKey, Table } from "sequelize-typescript";
import { productBrand ,productType, order } from ".";

export interface productI {
    id : number;
    noOfProduct : number;
    productTypeId : number;
    brandId : number;
    modelNumber :string;
    serialNumber :string;
    indexNumber : number;
    modelYear : number;
    contract :string;
    IsWarrenty :boolean;
    warrantyContractNum :number;
    warrentyDetails :string;
    warrentyNotes :string;
    installation :string
    levelUsage :number;
    purchaseDate :Date;
    priceNew :number;
    problem :string;
    errorCode :string;
    detail :string;
    notes :string;
    recallInfo :string;
    other :string;
    resetLink:string;
    cleaningLink:string;
    resources:JSON;
    fixFirstId:string;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
}

@Table({
    modelName: 'product',
    tableName: 'product',
    timestamps: true
})

export class product extends Model<productI>{

    @HasMany((): typeof order => order )
    public order: typeof order;
    
    @BelongsTo((): typeof productType => productType )
    public productType: typeof productType;

    @BelongsTo((): typeof productBrand => productBrand )
    public brands: typeof productBrand;

    @PrimaryKey
    @AutoIncrement  
    @Column(DataType.INTEGER)
    public id: number;

    @Column(DataType.INTEGER)
    public noOfProduct: number;


    @ForeignKey((): typeof productType  => productType )
    @Column(DataType.INTEGER)
    public productTypeId: number;

    @ForeignKey((): typeof productBrand  => productBrand )
    @Column(DataType.INTEGER)
    public brandId: number;

    @Column(DataType.TEXT)
    public modelNumber: string;

    @Column(DataType.TEXT)
    public serialNumber: string;

    @Column(DataType.INTEGER)
    public indexNumber: number;

    @Column(DataType.INTEGER)
    public modelYear: number;

    @Column(DataType.TEXT)
    public contract: string;
    
    @Column(DataType.BOOLEAN)
    public IsWarrenty: boolean;

    @Column(DataType.INTEGER)
    public warrantyContractNum: number;

    @Column(DataType.TEXT)
    public warrentyDetails: string;

    @Column(DataType.TEXT)
    public warrentyNotes: string;

    @Column(DataType.TEXT)
    public installation: string;

    @Column(DataType.INTEGER)
    public levelUsage: number;

    @Column({ type :DataType.VIRTUAL , 
        get() {return getlevelUsage(this.getDataValue('levelUsage'));}})
        public levelUsageValue : string


    @Column(DataType.DATE)
    public purchaseDate: Date;

    @Column(DataType.INTEGER)
    public priceNew: number;


    @Column(DataType.TEXT)
    public problem: string;

    @Column(DataType.TEXT)
    public errorCode: string;

    @Column(DataType.TEXT)
    public detail: string;

    @Column(DataType.TEXT)
    public notes: string;

    @Column(DataType.TEXT)
    public recallInfo: string;

    @Column(DataType.TEXT)
    public other: string;

    @Column(DataType.TEXT)
    public resetLink: string;

    @Column(DataType.TEXT)
    public cleaningLink: string;

    @Column(DataType.JSON)
    public resources:JSON ;

    @Column(DataType.TEXT)
    public fixFirstId: string;


    // @ForeignKey((): typeof  order => order )
    // @Column(DataType.INTEGER)
    // public orderId: number;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;


}

const getlevelUsage = (levelUsage:any) => {
    if (levelUsage === 1) return "Gering"
    if (levelUsage === 2) return "Normal"
    if (levelUsage === 3) return "Intensiv"

    return ""
  };
import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey,  Model, PrimaryKey, Table } from "sequelize-typescript";
import { statusActivity,users} from "./index";

export interface statusNotificationsI {
    id: number;
    statusActivityId : Number;
    userId : Number;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
}

@Table({
    modelName: 'statusNotifications',
    tableName: 'statusNotifications',
    timestamps: true
})

export class statusNotifications extends Model<statusNotificationsI>{

    @BelongsTo((): typeof statusActivity => statusActivity )
    public statusActivity: typeof statusActivity;

    // @BelongsTo((): typeof order => order )
    // public order: typeof order;

    @BelongsTo((): typeof users => users )
    public users: typeof users;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id : number;

    @ForeignKey((): typeof statusActivity  => statusActivity )
    @Column(DataType.INTEGER)
    public statusActivityId : Number;

    // @ForeignKey((): typeof order  => order )
    // @Column(DataType.INTEGER)
    // public orderId : Number;

    @ForeignKey((): typeof users  => users )
    @Column(DataType.INTEGER)
    public userId : Number;

    @Column(DataType.DATE)
    public createdAt : Date;

    @Column(DataType.INTEGER)
    public createdBy : number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.DATE)
    public updatedAt : Date;
}
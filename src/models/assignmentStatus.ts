import { AutoIncrement, Column, DataType, HasMany, Model, PrimaryKey, Table } from "sequelize-typescript";
import { orderAssignment } from "./";

export interface assignmentStatusI {
    id: number;
    name:string;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
}

@Table({
    modelName: 'assignmentStatus',
    tableName: 'assignmentStatus',
    timestamps: true
})

export class assignmentStatus extends Model<assignmentStatusI>{

    // @HasMany((): typeof orderAssignment => orderAssignment )
    // public orderAssignment: typeof orderAssignment;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id : number;

    @Column(DataType.STRING)
    public name : string;

    @Column(DataType.DATE)
    public createdAt : Date;

    @Column(DataType.INTEGER)
    public createdBy : number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.INTEGER)
    public deletedBy : number;


    @Column(DataType.DATE)
    public updatedAt : Date;

    @Column(DataType.INTEGER)
    public updatedBy : number;


}
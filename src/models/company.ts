import { AutoIncrement, Column, DataType, HasMany, HasOne, Model, PrimaryKey, Table } from "sequelize-typescript";
import { order, users } from ".";

export interface companyI {
    id?: Number,
  compnayName?: String,
  industryId?: Number,
  logo?: JSON,
  companyName?: String,
  minNoOfEmp?: Number,
  maxNoOfEmp?: Number,
  address?: String, 
  addressNumber?: Number ,
  addition?: String,
  notes?: String,
  cityId?: Number,
  postalCode?: Number,
  countryId?: Number,
  isBillingAddress?: Boolean,
  ustId?: Number,
  handelsRegisterNumber?: Number,
  amtsgericht?: Number
  billingAddress?: String,
  billingAddressAdditionInfo?: String,
  billingAddressNotes?: String,
  billingAddressCityId?: Number
  billingAddressPostalCode?: Number 
  billingAddressCountryId?: Number
  billingAddressNumber?: String 
  noOfEmployees?: String,
  createdAt?: Number,
  updatedAt?: Number,
  deletedAt?: Number,
  createdBy?: Number,
  updatedBy?: Number,
  deletedBy?: Number,
  sparePartTax:number;
  serviceChargesTax:number;
  legalInformation:string;
  contact:string;
  additionalContactNumber:string;
  email:string;
  phoneNumber:string;
  additionalPhoneNumber:string;
  bankName:string;
  iban?: string;
  bic?: string;
  payTerms:string;
  payTermsDays:string;
  taxId?: number;
  vat?: number;
}

@Table({
    modelName: 'companyInfo',
    tableName: 'company',
    timestamps: true
})

export class company extends Model<companyI>{

    // @HasOne((): typeof users => users)
    // public users: typeof users;

    @HasMany((): typeof order => order)
    public orders: typeof order;

    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.TEXT)
    public companyName: string;

    @Column(DataType.JSON)
    public logo: JSON;

    // @Column(DataType.INTEGER)
    // public minNoOfEmp: number;

    // @Column(DataType.INTEGER)
    // public maxNoOfEmp: number;

    @Column(DataType.TEXT)
    public noOfEmployees: string;

    @Column(DataType.TEXT)
    public address: string;

    @Column(DataType.NUMBER)
    public addressNumber: number;

    @Column(DataType.TEXT)
    public addition: string;

    @Column(DataType.TEXT)
    public notes: string;

    // @ForeignKey((): typeof city => city)
    @Column(DataType.BIGINT)
    public cityId: number;

    @Column(DataType.BIGINT)
    public postalCode: number;

    // @ForeignKey((): typeof country => country)
    @Column(DataType.BIGINT)
    public countryId: number;

    @Column(DataType.TINYINT)
    public isBillingAddress: boolean;

    @Column(DataType.STRING)
    public billingAddress: string;
    
    @Column(DataType.STRING)
    public billingAddressNumber: string;

    @Column(DataType.STRING)
    public billingAddressAdditionInfo: string;

    @Column(DataType.STRING)
    public billingAddressNotes: string;
    
    @Column(DataType.INTEGER)
    public billingAddressCityId: number;

    @Column(DataType.INTEGER)
    public billingAddressPostalCode: number;

    @Column(DataType.INTEGER)
    public billingAddressCountryId: number;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

    @Column(DataType.INTEGER)
    public sparePartTax: number;
    
    @Column(DataType.INTEGER)
    public serviceChargesTax: number;
    
    @Column(DataType.TEXT)
    public legalInformation: string;


    @Column(DataType.TEXT)
    public contact: string;

    @Column(DataType.TEXT)
    public additionalContactNumber: string;

    @Column(DataType.TEXT)
    public email: string;

    @Column(DataType.TEXT)
    public phoneNumber: string;

    @Column(DataType.TEXT)
    public additionalPhoneNumber: string;

    @Column(DataType.TEXT)
    public bankName: string;

    @Column(DataType.TEXT)
    public iban: string;

    @Column(DataType.TEXT)
    public bic: string;

    @Column(DataType.TEXT)
    public payTerms: string;

    @Column(DataType.TEXT)
    public payTermsDays: string;

    @Column(DataType.BIGINT)
    public taxId: number;

    @Column(DataType.BIGINT)
    public vat: number;
    

}

import { AutoIncrement, Column, DataType, HasMany, Model, PrimaryKey, Table } from "sequelize-typescript";
import { subBookingType } from "./subBookingType";

export interface bookingTypeI {
    id: number;
    typeName:string;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
    lang?: string;
}

@Table({
    modelName: 'bookingType',
    tableName: 'bookingType',
    timestamps: true
})

export class bookingType extends Model<bookingTypeI>{

    @HasMany((): typeof subBookingType => subBookingType )
    public subBookingType: typeof subBookingType;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id : number;

    @Column(DataType.STRING)
    public typeName : string;

    @Column(DataType.DATE)
    public createdAt : Date;

    @Column(DataType.INTEGER)
    public createdBy : number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.INTEGER)
    public deletedBy : number;


    @Column(DataType.DATE)
    public updatedAt : Date;

    @Column(DataType.INTEGER)
    public updatedBy : number;



    @Column(DataType.STRING)
    public lang : string;
}
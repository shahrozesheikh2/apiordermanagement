import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, Model, PrimaryKey, Table } from "sequelize-typescript";
import { order } from ".";

export interface recomendationsI {
    id : number;
    orderId:number;
    recomendations: string;
    diagnosisSystemsNeeded: Boolean;
    specialToolsNeeded: Boolean;
    customerPreparation: Boolean;
    resources:  JSON;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
}

@Table({
    modelName: 'recomendations',
    tableName: 'recomendations',
    timestamps: true
})

export class recomendations extends Model<recomendationsI>{

    @BelongsTo((): typeof order => order )
    public order: typeof order;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;
    
    @Column(DataType.STRING)
    public recomendations: string;

    @Column(DataType.BOOLEAN)
    public diagnosisSystemsNeeded: Boolean;

    @Column(DataType.BOOLEAN)
    public specialToolsNeeded: Boolean;

    @Column(DataType.BOOLEAN)
    public customerPreparation: Boolean;

    @ForeignKey(():  typeof  order => order )
    @Column(DataType.INTEGER)
    public orderId: number;

    @Column(DataType.JSON)
    public resources:JSON ;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;


    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

}
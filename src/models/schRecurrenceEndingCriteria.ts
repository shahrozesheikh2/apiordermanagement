import { AutoIncrement, Column, DataType, HasMany, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface schRecurrenceEndingCriteriaI {
    id: number;
    name:string;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
}

@Table({
    modelName: 'schRecurrenceEndingCriteria',
    tableName: 'schRecurrenceEndingCriteria',
    timestamps: true
})

export class schRecurrenceEndingCriteria extends Model<schRecurrenceEndingCriteriaI>{
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id : number;

    @Column(DataType.STRING)
    public name : string;

    @Column(DataType.DATE)
    public createdAt : Date;

    @Column(DataType.INTEGER)
    public createdBy : number;

    @Column(DataType.DATE)
    public deletedAt : Date;


    @Column(DataType.DATE)
    public updatedAt : Date;

    @Column(DataType.INTEGER)
    public updatedBy : number;

}
import { AutoIncrement, Column, DataType, Model, PrimaryKey, Table } from "sequelize-typescript";


export interface proofOfWorkI {
    id: number;
    title:string;
    resources:JSON;
    orderId: number;
    technicianId: number;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
}

@Table({
    modelName: 'proofOfWork',
    tableName: 'proofOfWork',
    timestamps: true
})

export class proofOfWork extends Model<proofOfWorkI>{

    // @HasMany((): typeof subBookingType => subBookingType )
    // public subBookingType: typeof subBookingType;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id : number;

    @Column(DataType.STRING)
    public title : string;

    @Column(DataType.JSON)
    public resources : JSON;
    
    @Column(DataType.INTEGER)
    public orderId : number;

    @Column(DataType.INTEGER)
    public technicianId : number;

    @Column(DataType.DATE)
    public createdAt : Date;

    @Column(DataType.INTEGER)
    public createdBy : number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.INTEGER)
    public deletedBy : number;


    @Column(DataType.DATE)
    public updatedAt : Date;

    @Column(DataType.INTEGER)
    public updatedBy : number;


}
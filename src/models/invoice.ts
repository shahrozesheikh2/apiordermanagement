import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, Model, PrimaryKey, Table } from "sequelize-typescript";
import { order } from "./order";
// import { technicianWork } from ".";

export interface invoiceI {
    id?: number;
    technicianId?: number;
    orderSparePartTotal?: number;
    technicianWorkTotal?: number;
    grandTotal:number
    orderId?: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deletedBy?: number;
    customerId:number,
    companyId:number,
    billId:string,
    billStatusId:number,
    sparePartTax:number,
    servicesChargesTax:number;
    sparePartTotalWithTax:number;
    techWorkTotalWithTax:number;
    dueDate: Date;
    netTotal:number
    vatValue:number
    subTotal:number
    discount:number
    discountValue:number
    discountType:boolean
    billingAmount:number,
    additionalNotes:string
}

@Table({
    modelName: 'invoice',
    tableName: 'invoice',
    timestamps: true
})

export class invoice extends Model<invoiceI>{


    @BelongsTo((): typeof  order => order )
    public order : typeof order ;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.NUMBER)
    public technicianId: number;

    @Column(DataType.NUMBER)
    public orderSparePartTotal: number;

    // @ForeignKey((): typeof technicianWork => technicianWork)
    @Column(DataType.NUMBER)
    public technicianWorkTotal: number;

    @ForeignKey((): typeof order  => order )
    @Column(DataType.NUMBER)
    public orderId: number;

    @Column(DataType.NUMBER)
    public grandTotal: number;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

    @Column(DataType.DATE)
    public dueDate: Date;

    @Column(DataType.INTEGER)
    public netTotal: number;

    @Column(DataType.INTEGER)
    public vatValue: number;

    @Column(DataType.INTEGER)
    public subTotal: number;

    @Column(DataType.INTEGER)
    public discountValue: number;

    @Column(DataType.INTEGER)
    public discount: number;

    @Column(DataType.BOOLEAN)
    public discountType: boolean;

    @Column(DataType.INTEGER)
    public billingAmount: number;
}
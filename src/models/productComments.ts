import { AutoIncrement, Column, DataType, ForeignKey, Model, PrimaryKey, Table } from "sequelize-typescript";
import { users } from "./users";


export interface productCommentsI {
    id: number;
    comments:string;
    productId: number;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
}

@Table({
    modelName: 'productComments',
    tableName: 'productComments',
    timestamps: true
})

export class productComments extends Model<productCommentsI>{

    // @HasMany((): typeof subBookingType => subBookingType )
    // public subBookingType: typeof subBookingType;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id : number;

    @Column(DataType.STRING)
    public comments : string;

    @Column(DataType.INTEGER)
    public productId : number;

    @Column(DataType.DATE)
    public createdAt : Date;

    @ForeignKey((): typeof users  => users )
    @Column(DataType.INTEGER)
    public createdBy : number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.INTEGER)
    public deletedBy : number;

    @Column(DataType.DATE)
    public updatedAt : Date;
    
    @Column(DataType.INTEGER)
    public updatedBy : number;


}
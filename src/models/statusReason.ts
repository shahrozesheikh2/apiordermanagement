import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey,Model, PrimaryKey, Table } from "sequelize-typescript";
import { orderStatus } from ".";

export interface statusReasonI {
    id: number;
    statusId: number;
    reason:string;
    name:string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    createdBy: number;
    updatedBy: number;
    deletedBy: number;
}

@Table({
    modelName: 'statusReason',
    tableName: 'statusReason',
    timestamps: true
})

export class statusReason extends Model<statusReasonI>{

    @BelongsTo((): typeof orderStatus => orderStatus )
    public orderStatus: typeof orderStatus;

    // @HasMany((): typeof order => order)
    // public order: typeof order
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;

    @ForeignKey((): typeof orderStatus  => orderStatus )
    @Column(DataType.INTEGER)
    public statusId: number;

    @Column(DataType.STRING)
    public reason: String;

    @Column(DataType.STRING)
    public key: String;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;


    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

}
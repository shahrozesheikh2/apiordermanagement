import { AutoIncrement, Column, DataType, HasMany, Model, PrimaryKey, Table } from "sequelize-typescript";
import { orderAssignment } from "./index";

export interface orderStatusI {
    id: number;
    name:string;
    key:string;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
    orderStatusId:number;
    lang:string
}

@Table({
    modelName: 'orderStatus',
    tableName: 'orderStatus',
    timestamps: true
})

export class orderStatus extends Model<orderStatusI>{

    @HasMany((): typeof orderAssignment => orderAssignment )
    public orderAssignment: typeof orderAssignment;


    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id : number;

    @Column(DataType.STRING)
    public name: string;

    @Column(DataType.STRING)
    public key: string;

    @Column(DataType.DATE)
    public createdAt : Date;

    @Column(DataType.INTEGER)
    public createdBy : number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.INTEGER)
    public deletedBy : number;

    @Column(DataType.DATE)
    public updatedAt : Date;

    @Column(DataType.INTEGER)
    public updatedBy : number;

    @Column(DataType.INTEGER)
    public orderStatusId: number;

    @Column(DataType.STRING)
    public lang: string;


}
import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, Model, PrimaryKey, Table } from "sequelize-typescript";
import { order } from ".";

export interface orderErrorDetailsI {
    id : number;
    orderId:number;
    description: string;
    problem: string;
    errorCode: string;
    veryUrgent: Boolean;
    parkingHint: Boolean;
    noNoise: Boolean;
    badLightning: Boolean;
    poorlyAccessible: Boolean;
    slowInternet: Boolean;
    smartphone: Boolean;
    tablet: Boolean;
    laptop: Boolean;
    webcam: Boolean;
    fragile: Boolean;
    // attachmentId: number;
    resources:JSON;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
}

@Table({
    modelName: 'orderErrorDetails',
    tableName: 'orderErrorDetails',
    timestamps: true
})

export class orderErrorDetails extends Model<orderErrorDetailsI>{

    @BelongsTo((): typeof order => order )
    public order: typeof order;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;
    
    @Column(DataType.STRING)
    public description: string;

    @Column(DataType.STRING)
    public problem: string;

    @Column(DataType.STRING)
    public errorCode: string;

    // @Column(DataType.INTEGER)
    // public attachmentId: number;

    @ForeignKey((): typeof  order => order )
    @Column(DataType.INTEGER)
    public orderId: number;

    @Column(DataType.BOOLEAN)
    public veryUrgent: Boolean;

    @Column(DataType.BOOLEAN)
    public parkingHint: Boolean;

    @Column(DataType.BOOLEAN)
    public noNoise: Boolean;

    @Column(DataType.BOOLEAN)
    public badLightning: Boolean;

    @Column(DataType.BOOLEAN)
    public poorlyAccessible: Boolean;

    @Column(DataType.BOOLEAN)
    public slowInternet: Boolean;

    @Column(DataType.BOOLEAN)
    public smartphone: Boolean;

    @Column(DataType.BOOLEAN)
    public tablet: Boolean;

    @Column(DataType.BOOLEAN)
    public laptop: Boolean;

    @Column(DataType.BOOLEAN)
    public webcam: Boolean;

    @Column(DataType.BOOLEAN)
    public fragile: Boolean;

    @Column(DataType.JSON)
    public resources:JSON ;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;


    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

}
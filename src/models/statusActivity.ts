import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, HasMany, Model, PrimaryKey, Table } from "sequelize-typescript";
import { order,orderStatus ,users} from "./index";

export interface statusActivityI {
    id: number;
    statusId : Number;
    orderId : Number;
    userId : Number;
    activityDateTime : Date;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    statusReason:string
}

@Table({
    modelName: 'statusActivity',
    tableName: 'statusActivity',
    timestamps: true
})

export class statusActivity extends Model<statusActivityI>{

    @BelongsTo((): typeof orderStatus => orderStatus )
    public orderStatus: typeof orderStatus;

    @BelongsTo((): typeof order => order )
    public order: typeof order;

    @BelongsTo((): typeof users => users )
    public users: typeof users;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id : number;

    @ForeignKey((): typeof orderStatus  => orderStatus )
    @Column(DataType.INTEGER)
    public statusId : Number;

    @ForeignKey((): typeof order  => order )
    @Column(DataType.INTEGER)
    public orderId : Number;

    @ForeignKey((): typeof users  => users )
    @Column(DataType.INTEGER)
    public userId : Number;

    @Column(DataType.DATE)
    public activityDateTime : Date;

    @Column(DataType.DATE)
    public createdAt : Date;

    @Column(DataType.INTEGER)
    public createdBy : number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.DATE)
    public updatedAt : Date;

    @Column(DataType.TEXT)
    public statusReason:string
}
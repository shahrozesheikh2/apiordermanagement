import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, HasOne, Model, PrimaryKey, Table } from "sequelize-typescript";
import { roles } from "./roles";
import { statusActivity } from "./statusActivity";
// import {  company, roles } from ".";

export interface usersI {
    id: number;
    companyId: number;
    rolesId: number;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    // organizationName: string;
    // website: string;
    brand: string;
    mobileNumberCode: string;
    mobileNumber: string;
    mobileIsWhatsapp: boolean;
    additionalNumberCode: string;
    additionalNumber: string;
    additionalIsWhatsapp: boolean;
    taxId: string;
    emailVerified: boolean;
    adminTypeId: number;
    phoneNumberExtention?: string;
    phoneNumber?: string;
    isWhatsAppMblNo:boolean;
    additionNumberExtension:string
    additionNumber: string;
    isWhatsAppAddNo:boolean;
    organization?: string;
    address?: string;
    addressNumber?: number;
    country?: number;
    postalCode?: number;
    city?: number;
    languages?: number;
    certification?: number;
    brandsExpertise?: number;
    productsExpertise?: number;
    physicalResilience?: boolean;
    personalProfile?: string;
    userStatusId:number;
    timeZone:number;
    notes?: string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deltedBy?: number;
}

@Table({
    modelName: 'users',
    tableName: 'users',
    timestamps: true
})

export class users extends Model<usersI>{

    @HasOne((): typeof statusActivity => statusActivity)
    public statusActivity: typeof statusActivity; 

    @BelongsTo((): typeof roles => roles)
    public roles: typeof roles; 

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    // @ForeignKey((): typeof company => company)
    @Column(DataType.BIGINT)
    public companyId: number;

    @ForeignKey((): typeof roles => roles)
    @Column(DataType.BIGINT)
    public rolesId: number;

    @Column(DataType.TEXT)
    public firstName: string;

    @Column(DataType.TEXT)
    public lastName: string;

    @Column(DataType.TEXT)
    public email: string;

    @Column(DataType.TEXT)
    public password: string;

    // @Column(DataType.TEXT)
    // public organizationName: string;

    // @Column(DataType.TEXT)
    // public website: string;

    @Column(DataType.TEXT)
    public brand: string;

    @Column(DataType.TEXT)
    public mobileNumberCode: string;

    @Column(DataType.TEXT)
    public mobileNumber: string;

    @Column(DataType.TINYINT)
    public mobileIsWhatsapp: boolean;

    @Column(DataType.TEXT)
    public additionalNumberCode: string;

    @Column(DataType.TEXT)
    public additionalNumber: string;

    @Column(DataType.TINYINT)
    public additionalIsWhatsapp: boolean;

    @Column(DataType.TEXT)
    public taxId: string;

    @Column(DataType.TINYINT)
    public emailVerified: boolean;

    @Column(DataType.BIGINT)
    public adminTypeId: number;

    @Column(DataType.TEXT)
    public phoneNumberExtention: string;

    @Column(DataType.TEXT)
    public phoneNumber: string;

    @Column(DataType.TINYINT)
    public isWhatsAppMblNo: boolean;

    @Column(DataType.STRING)
    public additionNumberExtension: string;
    
    @Column(DataType.STRING)
    public additionNumber: string;

    @Column(DataType.BOOLEAN)
    public isWhatsAppAddNo: boolean;

    @Column(DataType.TEXT)
    public organization: string;

    @Column(DataType.TEXT)
    public address: string;

    @Column(DataType.TEXT)
    public addressNumber: number;

    @Column(DataType.BIGINT)
    public country: number;

    @Column(DataType.BIGINT)
    public timeZone: number;

    @Column(DataType.BIGINT)
    public postalCode: number;

    @Column(DataType.BIGINT)
    public city: number;

    @Column(DataType.BIGINT)
    public languages: number;

    @Column(DataType.BIGINT)
    public userStatusId: number;

    @Column(DataType.BIGINT)
    public certification: number;

    @Column(DataType.BIGINT)
    public brandsExpertise: number;

    @Column(DataType.BIGINT)
    public productsExpertise: number;

    @Column(DataType.TINYINT)
    public physicalResilience: boolean;

    @Column(DataType.TEXT)
    public personalProfile: string;

    @Column(DataType.TEXT)
    public notes: string;


    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;
}
import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey ,Model, PrimaryKey, Table } from "sequelize-typescript";
// import { bookingType } from "./bookingType";

export interface recomendationsChecksI {
    id: number;
    checksName:string;
    checksKey:string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    createdBy: number;
    updatedBy: number;
    deletedBy: number;
}

@Table({
    modelName: 'recomendationsChecks',
    tableName: 'recomendationsChecks',
    timestamps: true
})

export class recomendationsChecks extends Model<recomendationsChecksI>{
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;

    @Column(DataType.STRING)
    public checksName: String;

    @Column(DataType.STRING)
    public checksKey: String;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;


    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

}
import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, HasMany, Model, PrimaryKey, Table } from "sequelize-typescript";
import { bookingType } from "./bookingType";
import { order } from "./order";

export interface subBookingTypeI {
    id: number;
    bookingTypeId: number;
    name:string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    createdBy: number;
    updatedBy: number;
    deletedBy: number;
}

@Table({
    modelName: 'subBookingType',
    tableName: 'subBookingType',
    timestamps: true
})

export class subBookingType extends Model<subBookingTypeI>{

    @BelongsTo((): typeof bookingType => bookingType )
    public bookingType: typeof bookingType;

    @HasMany((): typeof order => order)
    public order: typeof order
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;

    @ForeignKey((): typeof bookingType  => bookingType )
    @Column(DataType.INTEGER)
    public bookingTypeId: number;

    @Column(DataType.STRING)
    public name: String;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;


    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

}
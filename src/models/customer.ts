import { AutoIncrement, Column, DataType, HasMany, Model, PrimaryKey, Table } from "sequelize-typescript";
import { order } from "./order";

export interface customerI {
    id: number;
    customerType:number;
    firstName: string;
    lastName: string;
    email: string;
    language: JSON;
    notes: string;
    mobileNumberExtension: string;
    mobileNumber: string;
    isWhatsAppMblNo:boolean;
    additionNumberExtension:string
    additionNumber: string;
    isWhatsAppAddNo:boolean;
    taxId: string;
    customerAddress:string;
    customerNumber:string;
    customerAddition:string;
    customerNotes:string;
    customerCountryId:number;
    customerCityId:number;
    customerPostalCode:number;
    isCustomerElevator:boolean;
    isVisitAddressSame:boolean; 
    visitAddress:string;
    visitAddressNumber:string;
    visitAddressAdditionInfo:string;
    visitAddressNotes:string;
    visitAddressCityId:number;
    visitAddressCountryId:number;
    languageId?: number;
    visitAddressPostalCode:number;
    isVisitAddressElevator:boolean;
    isBillingAddressSame:boolean;
    billingAddress:string;
    billingAddressAdditionInfo:string;
    billingAddressNotes:string;
    billingAddressCityId:number;
    billingAddressPostalCode:number;
    billingAddressCountryId:number;
    isbillingAddressElevator:boolean;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deletedBy?: number;
    companyName:string
}

@Table({
    modelName: 'customer',
    tableName: 'customer',
    timestamps: true
})

export class customer extends Model<customerI>{

    @HasMany((): typeof order => order )
    public order: typeof order;

    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;

    @Column(DataType.INTEGER)
    public customerType:number

    @Column(DataType.TEXT)
    public companyName: string

    @Column({ type :DataType.VIRTUAL , 
        get() {return getcustomerType(this.getDataValue('customerType'));}})
        public customerTypeValue : string

    @Column(DataType.STRING)
    public firstName: string;
    
    @Column(DataType.STRING)
    public lastName: string;
    
    @Column(DataType.STRING)
    public email: string;
    
    @Column(DataType.INTEGER)
    public languageId: number;

      
    @Column(DataType.STRING)
    public notes: string;
    
    @Column(DataType.STRING)
    public mobileNumberExtension: string;
    
    @Column(DataType.STRING)
    public mobileNumber: string;

    @Column(DataType.BOOLEAN)
    public isWhatsAppMblNo: boolean;

    @Column(DataType.STRING)
    public additionNumberExtension: string;
    
    @Column(DataType.STRING)
    public additionNumber: string;

    @Column(DataType.BOOLEAN)
    public isWhatsAppAddNo: boolean;
    
    @Column(DataType.STRING)
    public taxId: string;

    @Column(DataType.STRING)
    public customerAddress: string;
    
    @Column(DataType.STRING)
    public customerNumber: string;

    @Column(DataType.STRING)
    public customerAddition: string;

    @Column(DataType.STRING)
    public customerNotes: string;

    @Column(DataType.INTEGER)
    public customerCountryId: number;

    @Column(DataType.INTEGER)
    public customerCityId: number;

    @Column(DataType.INTEGER)
    public visitAddressCityId: number;

    @Column(DataType.INTEGER)
    public visitAddressCountryId: number;

    @Column(DataType.INTEGER)
    public billingAddressCityId: number;

    @Column(DataType.INTEGER)
    public billingAddressCountryId: number;

    @Column(DataType.INTEGER)
    public customerPostalCode: number;

    @Column(DataType.BOOLEAN)
    public isCustomerElevator: boolean;
    
    @Column(DataType.BOOLEAN)
    public isVisitAddressSame: boolean;

    @Column(DataType.STRING)
    public visitAddress: string;
    
    @Column(DataType.STRING)
    public visitAddressNumber: string;

    @Column(DataType.STRING)
    public visitAddressAdditionInfo: string;

    @Column(DataType.STRING)
    public visitAddressNotes: string;
    
    @Column(DataType.INTEGER)
    public visitAddressPostalCode: number;

    @Column(DataType.BOOLEAN)
    public isVisitAddressElevator: boolean;

    @Column(DataType.BOOLEAN)
    public isBillingAddressSame: boolean;

    @Column(DataType.STRING)
    public billingAddress: string;
    
    @Column(DataType.STRING)
    public billingAddressNumber: string;

    @Column(DataType.STRING)
    public billingAddressAdditionInfo: string;

    @Column(DataType.STRING)
    public billingAddressNotes: string;

    @Column(DataType.INTEGER)
    public billingAddressPostalCode: number;

    @Column(DataType.BOOLEAN)
    public isbillingAddressElevator: boolean;
  

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;


    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

    @Column(DataType.JSON)
    public language: String;

}

const getcustomerType = (type:any) => {
    if (type === 0) return "Privatperson"
    // if (type === 0) return "Private Person"
    if (type === 1) return "Business"
    return ""
  };
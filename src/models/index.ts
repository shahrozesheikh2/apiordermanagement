import { ANY } from '../shared/common';
import { 
    order , 
    subBookingType,
    bookingType ,
    orderErrorDetails,
    product,
    recomendations,
    productType,
    productBrand,
    techniciansTimings,
    schAvalabilityTechnicians,
    orderAssignment,
    assignmentStatus,
    schRecurrenceEndingCriteria,
    schReccurenceDateList,
    schRecurrenceDayList,
    orderStatus,
    orderSpareParts,
    orderSparePartsStatus,
    problemSolutions,
    spareParts,
    customer,
    signature,
    proofOfWork,
    users,
    statusActivity,
    roles,
    statusNotifications,
    invoice,
    productComments,
    recomendationsChecks,
    errorDetailsChecks,
    approvelSignature,
    company,
    statusReason,
    stateReason
} from '.';

export * from './order';
export * from './bookingType';
export * from './subBookingType';
export * from './product'
export * from './orderErrorDetails'
export * from './recomendations'
export * from './productType'
export * from './productBrand'
export * from './techniciansTimings'
export * from './schAvalabilityTechnicians'
export * from './orderAssignment'
export * from './assignmentStatus'
export * from './schRecurrenceEndingCriteria'
export * from './schReccurenceDateList'
export * from './schRecurrenceDayList'
export * from './orderStatus'
export * from './orderSpareParts'
export * from './orderSparePartsStatus'
export * from './problemSolutions'
export * from './spareParts'
export * from './customer'
export * from './signature'
export * from './proofOfWork'
export * from './users'
export * from './statusActivity'
export * from './roles'
export * from './statusNotifications'
export * from './invoice';
export * from './productComments';
export * from './recomendationsChecks';
export * from './errorDetailsChecks';
export * from './approvelSignature'
export * from './company'
export * from './statusReason'
export * from './stateReason'



 















type ModelType = ANY;

export const models: ModelType = [
    order,
    subBookingType,
    bookingType,
    orderErrorDetails,
    product,
    recomendations,
    productType,
    productBrand,
    techniciansTimings,
    schAvalabilityTechnicians,
    orderAssignment,
    assignmentStatus,
    schRecurrenceEndingCriteria,
    schReccurenceDateList,
    schRecurrenceDayList,
    orderStatus,
    orderSpareParts,
    orderSparePartsStatus,
    problemSolutions,
    spareParts,
    customer,
    signature,
    proofOfWork,
    users,
    statusActivity,
    roles,
    statusNotifications,
    invoice,
    productComments,
    recomendationsChecks,
    errorDetailsChecks,
    approvelSignature,
    company,
    statusReason,
    stateReason
]
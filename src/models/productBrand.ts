import { AutoIncrement, Column, DataType, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface productBrandI {
    id: number;
    brandName:string;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
    brandId?: number;
    lang?: string;
}

@Table({
    modelName: 'productBrand',
    tableName: 'productBrand',
    timestamps: true
})

export class productBrand extends Model<productBrandI>{
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id : number;

    @Column(DataType.STRING)
    public brandName : string;

    @Column(DataType.DATE)
    public createdAt : Date;

    @Column(DataType.INTEGER)
    public createdBy : number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.INTEGER)
    public deletedBy : number;


    @Column(DataType.DATE)
    public updatedAt : Date;

    @Column(DataType.INTEGER)
    public updatedBy : number;

    @Column(DataType.INTEGER)
    public brandId : number;

    @Column(DataType.STRING)
    public lang : string;
}
import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, HasMany, HasOne, Model, PrimaryKey, Table } from "sequelize-typescript";
import { orderAssignment, schAvalabilityTechnicians } from ".";

export interface schReccurenceDateListI {
    id: number;
    startDate:Date;
    endDate:Date;
    availableTechnicianId:number;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    createdBy: number;
    updatedBy: number;
    deletedBy: number;
}

@Table({
    modelName: 'schReccurenceDateList',
    tableName: 'schReccurenceDateList',
    timestamps: true
})

export class schReccurenceDateList extends Model<schReccurenceDateListI>{

    @BelongsTo((): typeof schAvalabilityTechnicians => schAvalabilityTechnicians )
    public schAvalabilityTechnicians: typeof schAvalabilityTechnicians;

    @HasOne((): typeof orderAssignment => orderAssignment)
    public orderAssignment: typeof orderAssignment;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;

    @Column(DataType.DATE)
    public startDate: Date;

    @Column(DataType.DATE)
    public endDate: Date;
 
    @ForeignKey((): typeof schAvalabilityTechnicians => schAvalabilityTechnicians)
    @Column(DataType.INTEGER)
    public availableTechnicianId: number;    

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

}

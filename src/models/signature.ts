import { AutoIncrement, Column, DataType, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface signatureI {
    id: number;
    orderId: number;
    technicianId: number;
    clientSignature: JSON;
    technicianSignature:JSON;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deletedBy?: number;
}

@Table({
    modelName: 'signature',
    tableName: 'signature',
    timestamps: true
})

export class signature extends Model<signatureI>{

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.NUMBER)
    public orderId: number

    @Column(DataType.NUMBER)
    public technicianId: number

    @Column(DataType.JSON)
    public clientSignature:JSON;

    @Column(DataType.JSON)
    public technicianSignature:JSON;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

}
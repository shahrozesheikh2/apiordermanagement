import { AutoIncrement, Column, DataType, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface productTypeI {
    id: number;
    productType:string;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
    ProductTypeId?: number;
    lang?: string;
}

@Table({
    modelName: 'productType',
    tableName: 'productType',
    timestamps: true
})

export class productType extends Model<productTypeI>{
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id : number;

    @Column(DataType.STRING)
    public productType : string;

    @Column(DataType.DATE)
    public createdAt : Date;

    @Column(DataType.INTEGER)
    public createdBy : number;

    @Column(DataType.DATE)
    public deletedAt : Date;

    @Column(DataType.INTEGER)
    public deletedBy : number;


    @Column(DataType.DATE)
    public updatedAt : Date;

    @Column(DataType.INTEGER)
    public updatedBy : number;

    @Column(DataType.INTEGER)
    ProductTypeId?: number;
    
    @Column(DataType.STRING)
    lang?: string;
}